# Fallout 2d20 Tabletop Compendium

Fallout 2d20 by Bethesda, published by Modiphius Entertainment. Compendium
compiled by the LavCorps-Line.

<!-- TOC -->
* [Fallout 2d20 Tabletop Compendium](#fallout-2d20-tabletop-compendium)
    * [Introduction](#introduction)
        * [Usage](#usage)
    * [Modularity & Forking](#modularity--forking)
        * [Patches & Overrides](#patches--overrides)
    * [Issues & Concerns](#issues--concerns)
<!-- TOC -->

## Introduction

This Fallout 2d20 Tabletop Compendium is not meant to replace the Fallout 2d20
Core Rulebook, instead, it is meant to compliment it.

Players and Game Masters alike are hoped to be aided with this document.

Made for fans of FO2D20, by fans of FO2D20.

### Usage

* Want to know more about an effect, quality, or other attribute? Try mousing
  over it! Most attributes have tooltips that can be viewed by mousing over
  them, implemented as a link that does not change the current page.
* Want to skip to a weapon in a long weapons list? Try clicking on the name of
  the weapon you want to skip to inside the weapon table!
    * Note: Doesn't work in index tables (yet).

## Modularity & Forking

All core rulebook types stem from the appropriately named file in `src/*.dhall`.

If you want to make a Fallout 2d20 homebrew compendium, feel free to fork! The
suggested manner to introduce non-core book data is through a new dhall file
that imports whatever dhall files it needs, calling it, for
example, `src/homebrew.dhall`. For homebrew that edits the core book, it would
be reasonable to simply edit the core files. Feel free to delete `.gitlab` when
forking, as it exists only for the FO2D20 Repository's badges.

New dhall & markdown files are handled automatically by the pipeline, no need to
mess with the CI/CD config or mdBook config! Dhall files are automatically
compiled in the same location with the same name. Think carefully about where
you put them! Additionally, make sure to keep the `SUMMARY.md` file updated, or
else your compiled Dhall files will not be included in the final site.

### Patches & Overrides

* The old Patches & Overrides system is no longer supported! Please see the
  mdBook documentation
  on [themes](https://rust-lang.github.io/mdBook/format/theme/index.html)
  or [custom css](https://rust-lang.github.io/mdBook/format/configuration/renderers.html#html-renderer-options)
  instead.

## Issues & Concerns

* Something seem off in a document?
* Do you have access to errata that we don't?
* Anything else bothering you?

**Feel free to open a GitLab issue!**

[We welcome all users to open a GitLab issue,](https://gitlab.com/LavCorps-Line/FO2D20/-/issues/new)
and only ask that you check that your issue hasn't been mentioned in another
post.