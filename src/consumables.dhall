-- src/refreshments.dhall

-- Imports --

let Prelude = ./prelude.dhall
let DEQ = ./genericDEQ.dhall
let Utils = ./utils.dhall

-- Type Defs --

let Consumable
    : Type
    = { cost : Natural
      , description : Text
      , effects : List DEQ.GenericDEQ
      , name : Text
      , rarity : Natural
      , weight : Natural
      }

let ProcessedConsumable
    : Type
    = { cost : Text
      , description : Text
      , effects : Text
      , entryGeneric : Text
      , entryLinked : Text
      , name : Text
      , rarity : Text
      , slug : Text
      , weight : Text
      }

-- Type Factories --

let ConsumableFactory
    : forall (con : Consumable) -> ProcessedConsumable
    = \(con : Consumable) ->
        let cost = Utils.Tooltipify (Utils.ParseValue con.cost) "in caps"
        let effects = DEQ.ProcessDEQList con.effects
        let name = con.name
        let rarity = Utils.Mono (Utils.ParseValue con.rarity)
        let slug = Utils.Sluginator name
        let weight = Utils.Tooltipify (Utils.ConvertWeight con.weight) "in pounds (lbs)"

        let entryGeneric = Prelude.Text.concat
            [ "| "
            , "${name} | "
            , "${effects} | "
            , "${weight} | "
            , "${cost} | "
            , "${rarity} |"
            ]

        let entryLinked = Prelude.Text.concat
            [ "| "
            , "[${name}](#${slug}) | "
            , "${effects} | "
            , "${weight} | "
            , "${cost} | "
            , "${rarity} |"
            ]

        let attributes = Prelude.Text.concat
            [ if Prelude.Natural.equal (List/length DEQ.GenericDEQ con.effects) 0 then "" else "* Effects: ${effects}\n"
            , "* Weight: ${weight}\n"
            , "* Cost: ${cost}\n"
            , "* Rarity: ${rarity}\n"
            ]

        let description = Prelude.Text.concat
            [ "## ${name}\n\n"
            , "${con.description}\n"
            , "${attributes}\n"
            ]

        in  { cost
            , description
            , effects
            , entryGeneric
            , entryLinked
            , name
            , rarity
            , slug
            , weight
            }

-- Consumables --

-- Misc. Consumables

let robot-repair-kit
    : Consumable
    = { cost = 48
      , description =
          ''
          A device which can help to repair and reactivate damaged robots or Power Armor.
          Most robots have internal self-diagnostic and repair protocols, which are
          activated when the robot performs repairs or is repaired by someone else. A robot
          repair kit helps jump-start and accelerate these protocols.
          ''
      , effects = [ DEQ.HealMechanicalFactory 4 ] : List DEQ.GenericDEQ
      , name = "Robot Repair Kit"
      , rarity = 2
      , weight = 0
      }

let stealth-boy
    : Consumable
    = { cost = 5
      , description =
          ''
          One of the most interesting technologies developed before the War, the Stealth
          Boy 3001 is a compact device which generates a modulating refraction field
          which transmits light from one side of the field to the other. The result is
          a near-perfect active camouflage, ranging from a sort of distorted transparency
          when moving to almost total invisibility when stationary.
          
          The Stealth Boy was reverse engineered from captured Chinese Stealth Armor
          during the War, and while the technology was never perfected—even the most
          advanced Stealth Boy consumes its battery in around 30 seconds—it was still
          seen to be reliable enough for service.
          
          Long-term use of Stealth Boy technology has been observed to cause permanent
          neurological changes, resulting in paranoid delusions, hallucinations, and
          other mental changes.
          ''
      , effects =
            [ DEQ.GenericDEQFactory
                "Stealth"
                "Activated using the Interact minor action. Once active, the device functions for three full turns: your current turn, your next turn, and your turn after that, after which, it ceases to function and is junk. When activated, you and the equipment you are carrying is rendered nearly invisible. Enemies add +2 to the difficulty of all tests to spot you; they may still hear you or notice your presence in other ways if they're looking hard enough. In addition, even if you have been detected, the invisibility increases your Defense by +2."
            ]
          : List DEQ.GenericDEQ
      , name = "Stealth Boy"
      , rarity = 1
      , weight = 0
      }

-- Output --

in  { Consumable
    , ProcessedConsumable
    , ConsumableFactory
    , ConsumablesMisc = { robot-repair-kit, stealth-boy }
    }
