-- src/genericDEQ.dhall

-- Imports --

let Prelude = ./prelude.dhall
let Utils = ./utils.dhall

-- Type Defs --

let GenericDEQ
    : Type
    = { name : Text
      , tooltip : Text
      }

-- Functions --

let ProcessDEQ
    : GenericDEQ -> Text
    = \(input : GenericDEQ) ->
        "[${Utils.Mono input.name}](# \"${input.tooltip}\")"

let ProcessDEQList
    : List GenericDEQ -> Text
    = \(input : List GenericDEQ) ->
        let a = Utils.Map GenericDEQ Text ProcessDEQ input
        let b = Prelude.Text.concatSep ", " a
        let c = Text/replace "``" "" b
        in  c

let ToggleDEQ
    : GenericDEQ -> Bool -> GenericDEQ
    = \(eq : GenericDEQ) ->
      \(toggle : Bool) ->
        let name = "${if toggle then "Gain" else "Remove"} ${eq.name}"
        let tooltip =
              "Does nothing if ${if toggle then "already present" else "not already present"}. ${eq.tooltip}"
        in  { name, tooltip }

-- Generic Factory --

let GenericDEQFactory
    : Text -> Text -> GenericDEQ
    = \(name : Text) -> \(tooltip : Text) -> { name, tooltip }

-- Factories --

let AmmoChangeFactory
    : Text -> GenericDEQ
    = \(atype : Text) ->
        let name = "Ammo Type Changes to ${atype}"
        let tooltip = "The ammo consumed by this weapon is now ${atype} type ammo."
        in  { name, tooltip }

let ChemCheapActionFactory
    : Natural -> GenericDEQ
    = \(change : Natural) ->
        let name = "Cheap Actions"
        let tooltip = "Extra actions cost ${Natural/show change} less AP."
        in  { name, tooltip }

let ChemDamageBuffFactory
    : Natural -> Text -> GenericDEQ
    = \(change : Natural) ->
      \(type : Text) ->
        let name = "+${Natural/show change} CD to ${type} Attacks"
        let tooltip = "${type} attacks deal an extra ${Natural/show change} CD for the duration of the chem, or until the end of the next scene if food granted this buff."
        in  { name, tooltip }

let ChemDefenseBuffFactory
    : Natural -> GenericDEQ -> GenericDEQ
    = \(change : Natural) ->
      \(type : GenericDEQ) ->
        let name = "+${Natural/show change} ${type.name} Damage Resistance"
        let tooltip = "${type.name} damage resistance is increased by ${Natural/show change} for the duration of the chem, or until the end of the next scene if food granted this buff."
        in  { name, tooltip }

let ChemHealFactory
    : Natural -> GenericDEQ
    = \(change : Natural) ->
        let name = "Heals ${Natural/show change} HP"
        let tooltip = "Healing Chems can be applied using the Take Chem minor action, healing ${Natural/show change} HP immediately, or it can be applied as part of a First Aid action, adding ${Natural/show change} to the number of HP healed overall."
        in  { name, tooltip }

let ChemHealRadFactory
    : Natural -> GenericDEQ
    = \(change : Natural) ->
        let name = "Heals ${Natural/show change} Radiation Damage"
        let tooltip = "Radiation Healing Chems can be applied using the Take Chem minor action, healing ${Natural/show change} Radiation damage immediately, or it can be applied as part of a First Aid action, healing ${Natural/show change} Radiation damage before any other healing done."
        in  { name, tooltip }

let ChemMaxHPChangeFactory
    : Integer -> GenericDEQ
    = \(change : Integer) ->
        let name = "${Integer/show change} Max HP"
        let tooltip = "Max HP is temporarily modified by ${Integer/show change} for the duration of the chem, or until the end of the next scene if food granted this buff."
        in  { name, tooltip }

let ChemSkillChallengeFactory
    : Bool -> Natural -> List Text -> GenericDEQ
    = \(easier : Bool) ->
      \(change : Natural) ->
      \(types : List Text) ->
        let name = "${if easier then "Easier" else "Harder"} ${Prelude.Text.concatSep "/" types} Tests"
        let tooltip = "${if easier then "Reduce" else "Increase"} the difficulty of all ${Prelude.Text.concatSep "/" types} tests by ${Natural/show change}${if easier then ", to a minimum of 0" else ""}, for the duration of the chem, or until the end of the next scene if food granted this buff."
        in  { name, tooltip }

let ChemSkillRerollFactory
    : Natural -> List Text -> GenericDEQ
    = \(change : Natural) ->
      \(types : List Text) ->
        let name = "Re-roll ${Natural/show change}d20 on ${Prelude.Text.concatSep "/" types} Tests"
        let tooltip = "You may re-roll ${Natural/show change}d20 on all ${Prelude.Text.concatSep "/" types} tests for the duration of the chem, or until the end of the next scene if food granted this buff."
        in  { name, tooltip }

let DamageBaseChangeFactory
    : Natural -> GenericDEQ
    = \(change : Natural) ->
        let name = "Change Damage to ${Natural/show change}"
        let tooltip = "Change the base damage rating of this weapon to be equal to ${Natural/show change}"
        in  { name, tooltip }

let DamageChangeFactory
    : Integer -> GenericDEQ
    = \(change : Integer) ->
        let name = "${Integer/show change} Damage"
        let tooltip = "Change the amount of on-hit damage die rolled by ${Integer/show change}"
        in  { name, tooltip }

let DamageTypeChangeFactory
    : GenericDEQ -> GenericDEQ
    = \(dtype : GenericDEQ) ->
        let name = "Change Damage Type to ${dtype.name}"
        let tooltip = "Change the base Damage Type of this weapon to ${dtype.name}."
        in  { name, tooltip }

let HealMechanicalFactory
    : Natural -> GenericDEQ
    = \(heals : Natural) ->
        let name = "Heals ${Natural/show heals} Mechanical HP"
        let tooltip = "Heals ${Natural/show heals}HP to a Robot or Power Armor. Can be applied using the Take Chem minor action, healing ${Natural/show heals} HP or treating an Injury immediately, or it can be applied as part of a First Aid action, healing ${Natural/show heals} HP or treating an Injury in addition to any other healing done. This is done using the Repair skill."
        in  { name, tooltip }

let FireRateChangeFactory
    : Integer -> GenericDEQ
    = \(change : Integer) ->
        let name = "${Integer/show change} Fire Rate"
        let tooltip = "Change the amount of ammunition that can be additionally expended to increase damage by ${Integer/show change}, to a minimum of 0 and a maximum if 6"
        in  { name, tooltip }

let RangeChangeFactory
    : Integer -> GenericDEQ
    = \(change : Integer) ->
        let name = "${Integer/show change} Range"
        let tooltip = "Change the range of the weapon by ${Integer/show change} ${if Prelude.Natural.equal ( Integer/clamp change ) 1 then "zone" else "zones"}, to a minimum of 0 (C) and a maximum of 3 (X)"
        in  { name, tooltip }

let MusketCrankFactory
    : Natural -> GenericDEQ
    = \(cost : Natural) ->
        let name = "Consume ${Natural/show cost} Ammunition per Attack"
        let tooltip = "Change the base ammunition consumption of this weapon to ${Natural/show cost}, as per any special mechanics."
        in  { name, tooltip }

-- Predefined DEQs --

let DamageTypes =
      { physical =
          GenericDEQFactory "Physical" "Targets the enemy's damage resistance."
      , energy =
          GenericDEQFactory "Energy" "Targets the enemy's energy resistance."
      , radiation =
          GenericDEQFactory
            "Radiation"
            "Targets the enemy's radiation resistance, inflicting radiation damage equal to the number rolled."
      , poison =
          GenericDEQFactory
            "Poison"
            "Inflicts the poison status with a value equal to the number rolled."
      }

let Effects =
      { alcoholic =
          GenericDEQFactory
            "Alcoholic"
            "Until the end of the scene, you may re-roll 1d20 on **STR** and **CHA** tests, but increase the difficulty of **INT** tests by +1. In addition, alcoholic drinks are addictive: After drinking the beverage, roll CD equal to the number of alcoholic drinks you've consumed during this session. If two or more Effects are rolled, you are addicted, and add +1 to the difficulty of **CHA** and **AGI** tests while not under the effects of an alcoholic drink. The Party Boy/Party Girl perk renders you immune to alcohol addictions."
      , breaking =
          GenericDEQFactory
            "Breaking"
            "For each Effect rolled, reduce the number of Cover Dice a piece of cover provides by 1, permanently. If the target is not in cover, instead reduce the damage resistance on the location struck by 1, according to the damage type of the weapon (i.e., physical damage reduces Physical damage resistance, energy damage reduces Energy damage resistance)."
      , burst =
          GenericDEQFactory
            "Burst"
            "The attack may hit one additional target within Close range of the primary target for each Effect rolled. Each additional target costs 1 additional shot from the weapon."
      , persistent =
          \(dtype : GenericDEQ) ->
              { name = Prelude.Text.concat [ "Persistent", "(${dtype.name})" ]
              , tooltip =
                  "If one or more Effects are rolled, the target suffers the weapon's damage again at the end of their next and subsequent turns, for a number of rounds equal to the number of Effects rolled. A character may spend a major action to make a test to stop Persistent damage early; the difficulty is equal to the number of Effects rolled, and the attribute + skill used are chosen by the GM."
              }
            : GenericDEQ
      , piercing =
          \(stacks : Natural) ->
            GenericDEQFactory
              "Piercing ${Natural/show stacks}"
              "Ignore ${Natural/show
                          stacks} points of the target's damage reduction for each Effect rolled."
      , poison =
          GenericDEQFactory
            "Poison"
            "Poison damage comes from toxic substances and animal venoms."
      , radiation =
          GenericDEQFactory
            "Radiation"
            "Radiation damage comes from exposure to radiation, such as from nuclear or irradiated weaponry."
      , radioactive =
          GenericDEQFactory
            "Radioactive"
            "For every Effect rolled, the target also suffers 1 point of Radiation damage. This Radiation damage is totaled and applied separately, after a character has suffered the normal damage from the attack."
      , spread =
          GenericDEQFactory
            "Spread"
            "For each Effect rolled, your attack inflicts one additional hit on the target. Each additional hit inflicts half the rolled damage (round down) and hits a random location even if a specific location was targeted for the initial attack."
      , stun =
          GenericDEQFactory
            "Stun"
            "If one or more Effects are rolled, the target loses their normal actions in their next turn. A stunned character or creature may still spend AP to take additional actions as normal."
      , vicious =
          GenericDEQFactory
            "Vicious"
            "The attack inflicts +1 damage for each Effect rolled."
      }

let Qualities =
      { accurate =
          GenericDEQFactory
            "Accurate"
            "If you take the Aim minor action before attacking with an Accurate weapon, you may spend up to 3 AP to add +1CD per AP spent to the attack's damage. If you gain damage in this way, you may not spend ammunition for extra damage. A weapon cannot be both Accurate and Inaccurate."
      , blast =
          GenericDEQFactory
            "Blast"
            "When you make an attack with a Blast weapon, you do not target a single opponent. Instead, select a single zone you can see, and make the appropriate skill test to attack, with a basic difficulty of 2 (adjusted for range as normal). If you succeed, every creature (and other damageable target) in that zone suffers the weapon's damage. If you fail, your misplaced attack is less effective: roll only half the weapon's damage to determine the damage inflicted to creatures in the target zone and ignore the weapon's normal damage effects"
      , close-quarters =
          GenericDEQFactory
            "Close Quarters"
            "A Close Quarters weapon is easy to use up-close, and suffers no difficulty increase for being used when within Reach of an enemy."
      , concealed =
          GenericDEQFactory
            "Concealed"
            "A Concealed weapon is small, or otherwise easy to hide on your person. Enemies do not spot a Concealed weapon unless you're wielding it, or if they make a thorough search and succeed at a PER + Survival test with a difficulty of 2."
      , debilitating =
          GenericDEQFactory
            "Debilitating"
            "The difficulty of any skill test to treat injuries inflicted by a Debilitating weapon increase by +1."
      , gatling =
          GenericDEQFactory
            "Gatling"
            "Ammunition is spent at ten times the normal rate by Gatling weapons: whenever you would spend one shot of ammunition, a Gatling weapon instead spends a burst of 10 shots. Whenever you spend ammunition to increase this weapon's damage, add +2 damage per ten-shot burst (to a maximum number of bursts equal to the weapon's Fire Rate), rather than +1 damage per shot."
      , inaccurate =
          GenericDEQFactory
            "Inaccurate"
            "When making an attack with an Inaccurate weapon, you gain no benefit from the Aim minor action. A weapon may not be both Accurate and Inaccurate."
      , mine =
          GenericDEQFactory
            "Mine"
            "When a Mine is placed onto a surface and primed, it becomes a dangerous object, inflicting its damage upon anyone who comes within Reach of it (and upon additional characters, if it has the Blast quality)."
      , night-vision =
          GenericDEQFactory
            "Night Vision"
            "The sights of a weapon with Night Vision have been made to allow you to see more clearly in the dark. When you take the Aim minor action with a Night Vision weapon, you ignore any increase in the difficulty of an attack due to darkness."
      , parry =
          GenericDEQFactory
            "Parry"
            "When an enemy attempts a melee attack against you, and you are wielding a Parry weapon, you may spend 1 AP to add +1 to your Defense against that attack"
      , recon =
          GenericDEQFactory
            "Recon"
            "When you Aim with a Recon weapon, you may mark the target you aimed at. The next ally to attack that target may re-roll one d20 on their attack."
      , reliable =
          GenericDEQFactory
            "Reliable"
            "During each combat encounter, a Reliable weapon ignores the first complication you roll on a test to use that weapon. A weapon may not be both Reliable and Unreliable."
      , suppressed =
          GenericDEQFactory
            "Suppressed"
            "If an enemy is not aware of you when you attack with a Suppressed weapon, they do not notice the attack unless they are the target or they pass a PER + Survival test with a difficulty of 2."
      , thrown =
          \(range : Text) ->
            GenericDEQFactory
              "Thrown (${range})"
              "A Thrown (C) weapon can be thrown, as a ranged attack with an ideal range of Close. A Thrown (M) weapon can be thrown, as a ranged attack with an ideal range of Medium. You make an AGI + Throwing test to attack with the weapon, depending on the type of weapon."
      , two-handed =
          GenericDEQFactory
            "Two-Handed"
            "A Two-Handed weapon must be held in two hands to be used effectively; attempting to attack with a Two-Handed weapon in one hand increases the difficulty by +2."
      , unreliable =
          GenericDEQFactory
            "Unreliable"
            "When you make an attack with an Unreliable weapon, increase the complication range of the attack by 1. A weapon may not be both Reliable and Unreliable."
      }

-- Output --

in  { GenericDEQ
    , ProcessDEQ
    , ProcessDEQList
    , ToggleDEQ
    , GenericDEQFactory
    , AmmoChangeFactory
    , ChemCheapActionFactory
    , ChemDamageBuffFactory
    , ChemDefenseBuffFactory
    , ChemHealFactory
    , ChemHealRadFactory
    , ChemMaxHPChangeFactory
    , ChemSkillChallengeFactory
    , ChemSkillRerollFactory
    , DamageBaseChangeFactory
    , DamageChangeFactory
    , DamageTypeChangeFactory
    , HealMechanicalFactory
    , FireRateChangeFactory
    , RangeChangeFactory
    , MusketCrankFactory
    , DamageTypes
    , Effects
    , Qualities
    }
