-- src/perks.dhall

-- Imports --

let Prelude = ./prelude.dhall
let Utils = ./utils.dhall

-- Type Defs --

let Perk : Type =
    { description : Text
    , name : Text
    , rank-current : Natural
    , rank-max : Natural
    , requirements : List Text
    }

let ProcessedPerk : Type =
    { description : Text
    , entryGeneric : Text
    , entryLinked : Text
    , name : Text
    , rank-max : Text
    , requirements : Text
    }

-- Functions --

let PerkLevelIncrease = \(input : Natural) ->
    "Every time you take this perk, the level requirement increases by ${Natural/show input}."

let ProcessRequirement = \(input : Perk) ->
    Utils.Mono (Prelude.Text.concatSep " " [input.name, Natural/show input.rank-current])

let ProcessRequirementList = \(input : List Perk) ->
    Text/replace "``" "" (Prelude.Text.concatSep ", " (Utils.Map Perk Text ProcessRequirement input))

-- Type Factories --

let PerkFactory : forall(perk : Perk) -> ProcessedPerk = \(perk : Perk) ->
    let name = perk.name
    let rank-max = Utils.Mono (Natural/show perk.rank-max)
    let requirements = Utils.ProcessList perk.requirements

    let entryGeneric = "| ${Utils.Mono name} | ${rank-max} | ${requirements} |"

    let entryLinked = "| [${Utils.Mono name}](#${Utils.Sluginator name}) | ${rank-max} | ${requirements} |"

    let description = Prelude.Text.concat
    [ "## ${name}\n\n"
    , perk.description ++ "\n"
    , "* Requirements: ${requirements}\n"
    , "* Ranks: ${rank-max}\n"
    ]

    in
    { description = description
    , entryGeneric = entryGeneric
    , entryLinked = entryLinked
    , name = name
    , rank-max = rank-max
    , requirements = requirements
    }

-- Perk Defs --

let action-figure : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you spend AP to take an additional major action, you do not suffer the
        increased skill test difficulty during your second action.
        ''
    , name = "Action Boy/Action Girl"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "None" ]
    }

let adamantium-skeleton : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you suffer damage, the amount of damage needed to inflict a critical
        hit on you increases by your rank in this perk. For example, if you have
        one rank in this perk, you suffer a critical hit from 6 or more damage,
        rather than 5 or more.

        ${PerkLevelIncrease 3}
        ''
    , name = "Adamantium Skeleton"
    , rank-current = rank
    , rank-max = 3
    , requirements = [ "END 7", "Level 1+" ]
    }

let adrenaline-rush : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When your health is below its maximum value, you count your STR score as 10
        for all purposes when attempting a STR-based skill test or melee attack.
        ''
    , name = "Adrenaline Rush"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "STR 7" ]
    }

let animal-friend : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        * Rank 1: **Whenever a creature NPC with the Mammal, Lizard, or Insect
        keyword would attack you, roll 1 CD:** on any result other than an Effect,
        the creature chooses not to attack you, although it may still attack another
        character it can target.
        * Rank 2: You can attempt a **CHA + Survival** test with a difficulty of 2
        as a major action. If you succeed, the animal treats you as friendly and
        will attack anyone who attacks you. Mighty and Legendary animals are
        unaffected by this perk.

        ${PerkLevelIncrease 5}
        ''
    , name = "Animal Friend"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "CHA 6", "Level 1+" ]
    }

let aquabody : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Water is your ally.

        * Rank 1: You no longer take radiation damage from swimming in irradiated
        water, and you can hold your breath for twice as long as normal.
        * Rank 2: enemies add +2 to the difficulty to tests to detect you while you
        are submerged underwater.

        ${PerkLevelIncrease 3}
        ''
    , name = "Aquaboy/Aquagirl"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "END 5", "Level 1+" ]
    }

let armorer : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        You can modify armor with armor mods. Each rank in this perk unlocks an
        additional rank of mods: rank 1 unlocks rank 1 mods, rank 2 unlocks rank 2
        mods, etc.
        ''
    , name = "Armorer"
    , rank-current = rank
    , rank-max = 4
    , requirements = [ "STR 5", "INT 6" ]
    }

let awareness : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you take the Aim minor action at a target within Close range, you spot
        their weaknesses and can attack more efficiently. The next attack you make
        against that target gains the Piercing 1 damage effect, or improves the
        rating of any existing Piercing X damage effect by 1.
        ''
    , name = "Awareness"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "PER 7" ]
    }

let barbarian : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Your physical Damage Resistance increases on all hit locations based on your
        STR. You do not gain this benefit while wearing Power Armor.

        * STR 7-8: +1 physical DR
        * STR 9-10: +2 physical DR
        * STR 11+: +3 physical DR
        ''
    , name = "Barbarian"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "STR 7", "Level 4+", "not a robot" ]
    }

let basher : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make a melee attack by bashing with your gun (see p.111 of the Core
        Rulebook), your attack gains the Vicious damage effect.
        ''
    , name = "Basher"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "STR 6" ]
    }

let better-criticals : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you inflict one or more points of damage to an enemy, you may spend 1
        Luck Point to automatically inflict a critical hit, causing an injury.
        ''
    , name = "Better Criticals"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "LCK 9" ]
    }

let big-leagues : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make a melee attack with a two-handed melee weapon, the weapon gains
        the Vicious damage effect.
        ''
    , name = "Big Leagues"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "STR 8" ]
    }

let black-widow : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        The Black Widow perk affects men and masculine characters, while the Lady
        Killer perk affects women and feminine characters--they are otherwise
        identical. When you attempt a **CHA** based skill test to influence a
        character of a chosen gender, you may re-roll 1d20. In addition, your attacks
        inflict +1 CD additional damage against characters of the chosen gender.
        ''
    , name = "Black Widow/Lady Killer"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "CHA 6" ]
    }

let blacksmith : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        You can modify melee weapons with weapon mods. Each rank in this perk unlocks
        an additional rank of melee weapon mods: rank 1 unlocks rank 1 mods, rank 2
        unlocks rank 2 mods, and rank 3 unlocks rank 3 mods.

        ${PerkLevelIncrease 4}
        ''
    , name = "Blacksmith"
    , rank-current = rank
    , rank-max = 3
    , requirements = [ "STR 6", "Level 2+" ]
    }

let blitz : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you move into reach of an opponent and make a melee attack against them
        in one turn, you may re-roll 1d20 on your attack. At rank 2, you also inflict
        +1 CD damage with that attack.

        ${PerkLevelIncrease 3}
        ''
    , name = "Blitz"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "AGI 9", "Level 1+" ]
    }

let bloody-mess : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you inflict a critical hit, roll 1 CD; if you roll an Effect, you
        inflict one additional injury to a random location.
        ''
    , name = "Bloody Mess"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "LCK 6" ]
    }

let can-do : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you are scavenging a location that contains food, you gain 1 additional
        random food item, without spending AP.
        ''
    , name = "Can Do!"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "LCK 5" ]
    }

let cap-collector : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you buy or sell items, you may increase or decrease the price of the
        goods being traded by 10%.
        ''
    , name = "Cap Collector"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "CHA 5" ]
    }

let cautious-nature : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Whenever you attempt a skill test, and you buy one or more d20s by spending
        Action Points, you may re-roll 1d20 on that test.
        ''
    , name = "Cautious Nature"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "PER 7", "no Daring Nature" ]
    }

let center-mass : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make a ranged attack, you may choose to strike your target's Torso
        location (or equivalent, for creatures that use a different location table)
        without increasing the difficulty of the attack. In addition, you may re-roll
        1d20 when making the test for your attack.
        ''
    , name = "Center Mass"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "AGI 7" ]
    }

let chem-resistant : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        * At rank 1, roll one fewer CD when determining if you become addicted to
        chems, to a minimum of 0.
        * At rank 2, you cannot become addicted to chems.

        ${PerkLevelIncrease 4}
        ''
    , name = "Chem Resistant"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "END 7", "Level 1+" ]
    }

let chemist : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Chems you create last twice as long as normal. In addition, you unlock chems
        recipes that have this perk as a requirement.
        ''
    , name = "Chemist"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "INT 7" ]
    }

let commando : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make a ranged attack with any weapon with a Fire Rate of 3 or higher
        (except heavy weapons), you add +1 CD per rank to the weapon's damage.

        ${PerkLevelIncrease 3}
        ''
    , name = "Commando"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "AGI 8", "Level 2+" ]
    }

let comprehension : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        After you use the bonus gained from reading a magazine, roll 1 CD. If you
        roll an Effect, you may use that bonus one additional time.
        ''
    , name = "Comprehension"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "INT 6" ]
    }

let concentrated-fire : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make a ranged attack and spend ammunition to increase the damage,
        you may re-roll up to 3 CD for your damage roll.
        ''
    , name = "Concentrated Fire"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "PER 8", "AGI 6" ]
    }

let daring-nature : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Whenever you attempt a skill test, and you buy one or more d20s by giving the
        gamemaster Action Points, you may re-roll 1d20 on that test.
        ''
    , name = "Daring Nature"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "LCK 7", "no Cautious Nature" ]
    }

let demolition-expert : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make an attack using a weapon with the Blast quality, the attack
        gains the Vicious damage effect. In addition, you unlock explosives recipes
        which have this perk as a requirement.
        ''
    , name = "Demolition Expert"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "PER 6", "LCK 6" ]
    }

let dodger : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        * At rank 1, when you take the Defend major action, you reduce the difficulty
        of the skill test by 1.
        * At rank 2, the AP cost to further increase your Defense is reduced to 1 AP.

        ${PerkLevelIncrease 6}
        ''
    , name = "Dodger"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "AGI 6", "Level 4+" ]
    }

let dogmeat : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        You aren't alone in the wilderness.

        You have a pet dog that serves as a friend and ally in dangerous times. The
        dog's profile can be seen in the Bestiary and is treated as an allied NPC
        creature under your command. If you need to forage for food and water, your
        dog looks after themselves. If your dog is slain, then you either find a new
        dog before the next adventure, or you may trade this perk for a different one
        after this adventure.
        ''
    , name = "Dogmeat"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "CHA 5" ]
    }

let entomologist : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make an attack against an NPC with the Insect keyword, your attack
        gains the Piercing 1 damage effect, or improves the rating of any Piercing X
        effect the weapon has by +1.
        ''
    , name = "Entomologist"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "INT 7" ]
    }

let fast-metabolism : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you regain one or more HP from any source other than rest, increase the
        total HP regained by +1 per rank in this perk.

        ${PerkLevelIncrease 3}
        ''
    , name = "Fast Metabolism"
    , rank-current = rank
    , rank-max = 3
    , requirements = [ "END 6", "Level 1+", "not a robot" ]
    }

let faster-healing : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make an **END + Survival** test to heal your own injuries, the first
        additional d20 you buy is free. The normal maximum of 5d20 still applies.
        ''
    , name = "Faster Healing"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "END 6", "not a robot" ]
    }

let finesse : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Once per combat encounter, you may re-roll all the CD on a single damage roll
        without spending any Luck points.
        ''
    , name = "Finesse"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "AGI 9" ]
    }

let fortune-finder : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Whenever you roll to determine how much money you find, you find more.

        * At rank 1, you find +3 CD additional caps.
        * At rank 2, you find +6 CD additional caps.
        * At rank 3, you find +10 CD additional caps.

        ${PerkLevelIncrease 4}
        ''
    , name = "Fortune Finder"
    , rank-current = rank
    , rank-max = 3
    , requirements = [ "LCK 5", "Level 2+" ]
    }

let ghost : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Whenever you attempt an **AGI + Sneak** test in shadows or darkness, the
        first additional d20 you buy is free. The normal maximum of d20 still
        applies.
        ''
    , name = "Ghost"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "PER 5", "AGI 6" ]
    }

let grim-reapers-sprint : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make an attack which kills one or more enemies, roll 1 CD. If you
        roll an Effect, add 2 AP to the group's pool.
        ''
    , name = "Grim Reaper's Sprint"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "LCK 8" ]
    }

let gun-fu : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you succeed at a ranged attack, you may spend 1 AP and 1 ammo to hit a
        second target within Close range of your initial target. The second target
        takes the same damage as the initial target. At rank 2, you may spend 2 AP
        and 2 ammo to hit two additional targets. At rank 3, you may spend 3 AP and 3
        ammo to hit three additional targets.

        ${PerkLevelIncrease 5}
        ''
    , name = "Gun Fu"
    , rank-current = rank
    , rank-max = 3
    , requirements = [ "AGI 10", "Level 1+" ]
    }

let gun-nut : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        You can modify small guns and heavy weapons with weapon mods. Each rank in
        this perk unlocks an additional rank of weapon mods: rank 1 unlocks rank 1
        mods, rank 2 unlocks rank 2 mods, etc.

        ${PerkLevelIncrease 4}
        ''
    , name = "Gun Nut"
    , rank-current = rank
    , rank-max = 3
    , requirements = [ "INT 6, Level 2+" ]
    }

let gunslinger : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make an attack with a one-handed ranged weapon with a Fire Rate of
        2 or lower, you increase the weapon's damage by +1 CD per rank. In addition,
        you may re-roll the hit location die.

        ${PerkLevelIncrease 4}
        ''
    , name = "Gunslinger"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "AGI 7", "Level 2+" ]
    }

let hacker : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        The difficulty of skill tests to hack computers is decreased by 1, to a
        minimum of 0.
        ''
    , name = "Hacker"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "INT 8" ]
    }

let healer : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you heal a patient's HP using the First Aid action, increase the amount
        of HP healed by +1 per rank in this perk.

        ${PerkLevelIncrease 5}
        ''
    , name = "Healer"
    , rank-current = rank
    , rank-max = 3
    , requirements = [ "INT 7", "Level 1+" ]
    }

let heave-ho : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make a thrown weapon attack, you may spend 1 AP to increase the
        Range of the weapon by one step.
        ''
    , name = "Heave Ho!"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "STR 8" ]
    }

let hunter : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make an attack against an NPC with one of the Mammal, Lizard, or
        Insect keywords and the Mutated keyword, your attack gains the Vicious
        damage effect, if it did not already have that effect.
        ''
    , name = "Hunter"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "END 6" ]
    }

let infiltrator : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you attempt a Lockpick skill test to unlock a door or container, you may
        re-roll 1d20.
        ''
    , name = "Infiltrator"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "PER 8" ]
    }

let inspirational : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Because you lead by example, the maximum number of AP the group may save is
        increased by 1.
        ''
    , name = "Inspirational"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "CHA 8" ]
    }

let intense-training : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Increase any one S.P.E.C.I.A.L attribute by 1 rank. As usual, your
        S.P.E.C.I.A.L attributes cannot be increased beyond 10 using this method.

        ${PerkLevelIncrease 2}
        ''
    , name = "Intense Training"
    , rank-current = rank
    , rank-max = 10
    , requirements = [ "Level 2+" ]
    }

let iron-fist : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        * Rank 1: Your unarmed attacks inflict +1 CD damage.
        * Rank 2: Your unarmed attacks also gain the Vicious damage effect.

        ${PerkLevelIncrease 5}
        ''
    , name = "Iron Fist"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "STR 6", "Level 1+" ]
    }

let junktown-jerky-vendor : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        The difficulty of any **CHA + Barter** test you attempt to buy or sell goods
        is reduced by 1, to a minimum of 0.
        ''
    , name = "Junktown Jerky Vendor"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "CHA 8" ]
    }

let jury-rigging : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        You can repair an item without needing to expend any components. However,
        the repair is temporary, and the item will break again on the next
        complication you suffer while using it. The complication range of all skill
        tests to use the item increases by 1, to the roll of a 19-20.
        ''
    , name = "Jury Rigging"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "None" ]
    }

let laser-commander : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make an attack with a ranged energy weapon, the damage is increased
        by +1 CD per rank in this perk.

        ${PerkLevelIncrease 4}
        ''
    , name = "Laser Commander"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "PER 8", "Level 2+" ]
    }

let lead-belly : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        * Rank 1: You may re-roll the CD to determine if you suffer radiation damage
        from irradiated food or drink.
        * Rank 2: You are immune to radiation damage from consuming irradiated food
        or drink.

        ${PerkLevelIncrease 4}
        ''
    , name = "Lead Belly"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "END 6", "Level 1+" ]
    }

let life-giver : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Increase your maximum health points by your Endurance rank.

        ${PerkLevelIncrease 5}
        ''
    , name = "Life Giver"
    , rank-current = rank
    , rank-max = 5
    , requirements = [ "Level 5+" ]
    }

let light-step : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you roll to generate any complications on an Agility-based skill test,
        you may ignore one complication for every 1 AP spent. In addition, you may
        re-roll 1d20 on any **AGI + Athletics** test to avoid traps triggered by
        pressure plates or similar mechanisms.
        ''
    , name = "Light Step"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "None" ]
    }

let master-thief : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you are attempting to pick a lock or pickpocket somebody, the difficulty
        of the opponent's skill test to detect you increases by +1.
        ''
    , name = "Master Thief"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "PER 8", "AGI 9" ]
    }

let medic : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        WHen you use the First Aid action to try to treat an injury, you can re-roll
        1d20.
        ''
    , name = "Medic"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "INT 8" ]
    }

let meltdown : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you kill an enemy with an energy weapon, they explode. Roll a number of
        CD equal to half the weapon's damage rating (round down). For each Effect
        rolled, one creature within Close range of the exploding enemy (starting with
        the closest) suffers energy damage equal to the total rolled on the CD.
        ''
    , name = "Meltdown"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "PER 10" ]
    }

let mister-sandman : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make a sneak attack with a silenced or suppressed weapon, the damage
        is increased by +2 CD. You cannot gain this benefit while in Power Armor.
        ''
    , name = "Mister Sandman"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "AGI 9" ]
    }

let moving-target : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you take the Sprint action, your Defense increases by +1 until the start
        of your next turn.
        ''
    , name = "Moving Target"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "AGI 6" ]
    }

let mysterious-stranger : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        From time to time, a Mysterious Stranger comes to your aid, with lethal
        results. At the start of a combat encounter, you may spend 1 Luck point. If
        you do so, then at any point during the scene, the GM may have the Mysterious
        Stranger appear, make a single ranged attack against an enemy you attacked,
        or who just attacked you, and then vanish. If you spend a Luck point and the
        Mysterious Stranger does not appear, the GM must refund the Luck point you
        spent.

        The stranger has an AGI of 10, a Small Guns skill of 6, and counts Small Guns
        as a Tag skill. They always roll 3d20 for attacks, rather than 2d20, and
        their attack--using a custom .44 Magnum revolver--inflicts 8 CD, Piercing 1,
        Vicious physical damage. They always appear within their weapon's ideal
        range. Any attempt to find where the Stranger went after their attack fails.
        ''
    , name = "Mysterious Stranger"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "LCK 7" ]
    }

let nerd-rage : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        While your health is reduced to less than 1/4 of your maximum, you add +1 to
        your physical DR, +1 to your energy DR, and +1 CD to the damage of all your
        attacks.

        * Rank 2: This increases to +2 DR, and +2 CD damage.
        * Rank 3: This increases to +3 DR and +3 CD damage.

        ${PerkLevelIncrease 5}
        ''
    , name = "Nerd Rage"
    , rank-current = rank
    , rank-max = 3
    , requirements = [ "INT 8", "Level 2+" ]
    }

let night-person : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        You reduce any difficulty increases due to darkness by 1.
        ''
    , name = "Night Person"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "PER 7" ]
    }

let ninja : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make a sneak attack with a melee weapon or an unarmed attack, the
        damage is increased by +2 CD. You cannot gain this benefit while in Power
        Armor.
        ''
    , name = "Ninja"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "AGI 8" ]
    }

let nuclear-physicist : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Whenever you use a weapon that inflicts radiation damage, or has the
        Radioactive damage effect, each Effect you roll inflicts one additional
        point of radiation damage.

        In addition, fusion cores you use have 3 additional charges.
        ''
    , name = "Nuclear Physicist"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "INT 9" ]
    }

let pain-train : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        * Rank 1: You may Charge as a major action if you are wearing Power Armor or are a
        super mutant. This is a movement action, and you may not *Move* or *Sprint*
        in the same turn. When you take this action, you move into reach of an enemy
        within Medium range (1 zone) and make a **STR + Athletics** test with a
        difficult of 2. If you succeed, the enemy suffers damage equal to your normal
        unarmed damage, and is knocked prone.
        * Rank 2: You add +1 CD and the Stun damage effect to the damage inflicted.
        At the GM's discretion, especially large or sturdy creatures cannot be
        knocked prone by this action.

        ${PerkLevelIncrease 5}
        ''
    , name = "Pain Train"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "STR 9", "END 7", "Level 1+" ]
    }

let paralyzing-palm : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make an unarmed attack, and choose to strike a specific location,
        your attack gains the Stun damage effect.
        ''
    , name = "Paralyzing Palm"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "STR 8" ]
    }

let party-animal : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        You cannot become addicted to alcoholic drinks, and whenever you drink an
        alcoholic drink, you heal +2 HP.
        ''
    , name = "Party Boy/Party Girl"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "END 6", "CHA 7" ]
    }

let pathfinder : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When travelling over long distances through the wilderness, a successful
        **PER + Survival** test (with a difficulty determined by the GM, based on
        the terrain) reduces the travel time by half.
        ''
    , name = "Pathfinder"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "PER 6", "END 6" ]
    }

let pharma-farma : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you are scavenging a location that contains medicine or chems, you find
        one additional item, randomly determined, without spending AP.
        ''
    , name = "Pharma Farma"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "LCK 6" ]
    }

let pickpocket : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        * Rank 1: You can ignore the first complication you roll when you make an
        **AGI + Sneak** test to steal an object from someone else's person or to
        plant something on them.
        * Rank 2: You can re-roll 1d20 in your dice pool when attempting to pick
        someone's pocket.
        * Rank 3: You reduce the difficulty of attempts to pick someone's pocket by
        1.

        ${PerkLevelIncrease 3}
        ''
    , name = "Pickpocket"
    , rank-current = rank
    , rank-max = 3
    , requirements = [ "PER 8", "AGI 8", "Level 1+" ]
    }

let piercing-strike : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Your attacks using unarmed attacks and bladed melee weapons gain the Piercing
        1 damage effect, or add +1 to the rating of any Piercing X damage effects
        they already had.
        ''
    , name = "Piercing Strike"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "STR 7" ]
    }

let pyromaniac : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        The damage you deal using fire-based weapons increases by +1 CD per rank.

        ${PerkLevelIncrease 4}
        ''
    , name = "Pyromaniac"
    , rank-current = rank
    , rank-max = 3
    , requirements = [ "END 6", "Level 2+" ]
    }

let quick-draw : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Each turn, you may draw a single weapon or item carried on your person
        without using a minor action.
        ''
    , name = "Quick Draw"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "AGI 6" ]
    }

let quick-hands : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        You can reload firearms faster.

        When you make a ranged attack, you may spend 2 AP to double the Fire Rate of
        your gun for that attack.
        ''
    , name = "Quick Hands"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "AGI 8" ]
    }

let rad-resistance : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Your radiation Damage Resistance, to all hit locations, increases by +1 per
        rank in this perk.

        ${PerkLevelIncrease 4}
        ''
    , name = "Rad Resistance"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "END 8", "Level 1+" ]
    }

let refractor : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Your energy Damage Resistance to all hit locations increases by +1 per rank
        in this perk.

        ${PerkLevelIncrease 4}
        ''
    , name = "Refractor"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "PER 6", "LCK 7", "Level 1+" ]
    }

let ricochet : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        If an enemy makes a ranged attack against you and rolls a complication, you
        may spend one Luck point to have their ricochet hit them. Resolve the damage
        roll against the attacking enemy instead.
        ''
    , name = "Ricochet"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "LCK 10", "Level 5+" ]
    }

let rifleman : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        * Rank 1: When you make a ranged attack with any two-handed weapon with a
        Fire Rate of 2 or lower (except heavy weapons), you add +1 CD to the weapon's
        damage per rank.
        * Rank 2: You also add the Piercing 1 damage effect, or add +1 to the rating
        of any Piercing X damage effect the weapon already had.

        ${PerkLevelIncrease 4}
        ''
    , name = "Rifleman"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "AGI 7, Level 2+" ]
    }

let robotics-expert : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        * Rank 1: You can modify robot armor, weapon mounts, and modules with rank 1
        mods.
        * Rank 2: You gain access to rank 2 mods, and the difficulty of tests to
        repair robots is permanently reduced by 1. At rank 3 you gain access to
        rank 3 mods, and you can reprogram robots to fulfill a different function
        or alter their behavior at the discretion of the GM.

        ${PerkLevelIncrease 4}
        ''
    , name = "Robotics Expert"
    , rank-current = rank
    , rank-max = 3
    , requirements = [ "INT 8", "Level 2+" ]
    }

let science : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        You can modify energy weapons with weapon mods, and you can also craft
        certain advanced armor mods. Each rank in this perk unlocks an additional
        rank of mods: rank 1 unlocks rank 1 mods, rank 2 unlocks rank 2 mods, etc.

        ${PerkLevelIncrease 4}
        ''
    , name = "Science!"
    , rank-current = rank
    , rank-max = 3
    , requirements = [ "INT 6", "Level 2+" ]
    }

let scoundrel : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make a **CHA + Speech** test to convince someone of a lie, you may
        ignore the first complication you roll.
        ''
    , name = "Scoundrel"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "CHA 7" ]
    }

let scrapper : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        * Rank 1: When you scrap an item, you can salvage uncommon component
        materials as well as common ones.
        * Rank 2: You can also salvage rare materials.

        ${PerkLevelIncrease 5}
        ''
    , name = "Scrapper"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "Level 3+" ]
    }

let scrounger : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Whenever you roll to determine how much ammunition you find, you find more.

        * Rank 1: You find +3 CD additional shots.
        * Rank 2: You find +6 CD additional shots.
        * Rank 3: You find +10 CD additional shots.

        The additional ammo you find is the same as initially found, for example, if
        you find 10mm ammunition, this perk increases how much 10mm ammo you find.
        If you find multiple types of ammunition, Scrounger applies to the ammunition
        with the lowest rarity (GM's choice if there is a tie).

        ${PerkLevelIncrease 5}
        ''
    , name = "Scrounger"
    , rank-current = rank
    , rank-max = 3
    , requirements = [ "LCK 6", "Level 1+" ]
    }

let shotgun-surgeon : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Your ranged attacks using shotguns gain the Piercing 1 damage effect, or add
        +1 to any Piercing X damage effect the weapon already had.
        ''
    , name = "Shotgun Surgeon"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "STR 5", "AGI 7" ]
    }

let size-matters : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make a ranged attack with any heavy weapon, you add +1 CD to the
        weapon's damage, per rank.

        ${PerkLevelIncrease 4}
        ''
    , name = "Size Matters"
    , rank-current = rank
    , rank-max = 3
    , requirements = [ "END 7", "AGI 6" ]
    }

let skilled : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Add +1 rank to two skills or add +2 ranks to one skill. No skil may have more
        than 6 ranks.

        ${PerkLevelIncrease 3}
        ''
    , name = "Skilled"
    , rank-current = rank
    , rank-max = 10
    , requirements = [ "Level 3+" ]
    }

let slayer : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you inflict any damage wit han unarmed attack or melee weapon, you may
        spend 1 Luck point to immediately inflict a critical hit--and therefore an
        injury--the location hit.
        ''
    , name = "Slayer"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "STR 8" ]
    }

let smooth-talker : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you make a Barter or Speech test as part of an opposed test, you may
        re-roll 1d20.
        ''
    , name = "Smooth Talker"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "CHA 6" ]
    }

let snakeater : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Your poison Damage Resistance increases by +2.
        ''
    , name = "Snakeater"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "END 7" ]
    }

let sniper : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you take the Aim minor action, and then make a ranged attack with a
        two-handed weapon with the Accurate quality, you can specify a hit location
        to target without increasing the difficulty of the attack.
        ''
    , name = "Sniper"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "PER 8", "AGI 6" ]
    }

let solar-powered : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        For every hour you spend in direct sunlight, you heal 1 radiation damage.
        ''
    , name = "Solar Powered"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "END 7" ]
    }

let steady-aim : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        When you take the Aim minor action, you may either re-roll 2d20 on the first
        attack you make this turn, or re-roll 1d20 on all attacks you make this turn.
        ''
    , name = "Steady Aim"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "STR 8, AGI 7" ]
    }

let strong-back : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Your maximum carry weight is increased by +25 lbs., per rank.

        ${PerkLevelIncrease 2}
        ''
    , name = "Strong Back"
    , rank-current = rank
    , rank-max = 3
    , requirements = [ "STR 5", "Level 1+" ]
    }

let tag : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        You may select one additional Tag skill. Increase the skill's rank by 2, to a
        maximum of 6, and mark it as a Tag skill, allowing you to roll a critical
        success with a d20 result equal or under the skill's rank.
        ''
    , name = "Tag!"
    , rank-current = rank
    , rank-max = 1
    , requirements = [ "Level 5+" ]
    }

let terrifying-presence : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        * Rank 1: When you make a Speech test to threaten or scare someone, you may
        re-roll 1d20.
        * Rank 2: You may use a major action in combat to threaten an enemy within
        Medium range, using a **STR + Speech** test with a difficulty of 2. If you
        succeed, that enemy must move away from you during their next turn (though
        they can take any other actions they wish).

        ${PerkLevelIncrease 5}
        ''
    , name = "Terrifying Presence"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "STR 6", "CHA 8", "Level 3+" ]
    }

let toughness : Natural -> Perk = \(rank : Natural) ->
    { description =
        ''
        Your physical Damage Resistance to all hit locations increases by +1 per rank
        in this perk.

        ${PerkLevelIncrease 4}
        ''
    , name = "Toughness"
    , rank-current = rank
    , rank-max = 2
    , requirements = [ "END 6", "LCK 6", "Level 1+" ]
    }

-- Output --

in  { Perk
    , ProcessedPerk
    , PerkLevelIncrease
    , ProcessRequirement
    , ProcessRequirementList
    , PerkFactory
    , Perks = 
        { action-figure
        , adamantium-skeleton
        , adrenaline-rush
        , animal-friend
        , aquabody
        , armorer
        , awareness
        , barbarian
        , basher
        , better-criticals
        , big-leagues
        , black-widow
        , blacksmith
        , blitz
        , bloody-mess
        , can-do
        , cap-collector
        , cautious-nature
        , center-mass
        , chem-resistant
        , chemist
        , commando
        , comprehension
        , concentrated-fire
        , daring-nature
        , demolition-expert
        , dodger
        , dogmeat
        , entomologist
        , fast-metabolism
        , faster-healing
        , finesse
        , fortune-finder
        , ghost
        , grim-reapers-sprint
        , gun-fu
        , gun-nut
        , gunslinger
        , hacker
        , healer
        , heave-ho
        , hunter
        , infiltrator
        , inspirational
        , intense-training
        , iron-fist
        , junktown-jerky-vendor
        , jury-rigging
        , laser-commander
        , lead-belly
        , life-giver
        , light-step
        , master-thief
        , medic
        , meltdown
        , mister-sandman
        , moving-target
        , mysterious-stranger
        , nerd-rage
        , night-person
        , ninja
        , nuclear-physicist
        , pain-train
        , paralyzing-palm
        , party-animal
        , pathfinder
        , pharma-farma
        , pickpocket
        , piercing-strike
        , pyromaniac
        , quick-draw
        , quick-hands
        , rad-resistance
        , refractor
        , ricochet
        , rifleman
        , robotics-expert
        , science
        , scoundrel
        , scrapper
        , scrounger
        , shotgun-surgeon
        , size-matters
        , skilled
        , slayer
        , smooth-talker
        , snakeater
        , sniper
        , solar-powered
        , steady-aim
        , strong-back
        , tag
        , terrifying-presence
        , toughness
        }
    }
