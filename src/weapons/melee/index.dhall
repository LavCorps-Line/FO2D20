-- src/weapons/melee/index.dhall

let data = ./src/weapons.dhall

let Document =
''
| Melee Weapon | Weapon Type | Damage Rating | Damage Effects | Damage Type | Qualities | Weight | Cost | Rarity |
|--------------|-------------|---------------|----------------|-------------|-----------|--------|------|--------|
${(data.WeaponFactory data.Weapons.baseball-bat).entryLinked}
${(data.WeaponFactory data.Weapons.baseball-bat-aluminum).entryLinked}
${(data.WeaponFactory data.Weapons.baton).entryLinked}
${(data.WeaponFactory data.Weapons.board).entryLinked}
${(data.WeaponFactory data.Weapons.combat-knife).entryLinked}
${(data.WeaponFactory data.Weapons.lead-pipe).entryLinked}
${(data.WeaponFactory data.Weapons.machete).entryLinked}
${(data.WeaponFactory data.Weapons.pipe-wrench).entryLinked}
${(data.WeaponFactory data.Weapons.pool-cue).entryLinked}
${(data.WeaponFactory data.Weapons.ripper).entryLinked}
${(data.WeaponFactory data.Weapons.rolling-pin).entryLinked}
${(data.WeaponFactory data.Weapons.shishkebab).entryLinked}
${(data.WeaponFactory data.Weapons.sledgehammer).entryLinked}
${(data.WeaponFactory data.Weapons.super-sledge).entryLinked}
${(data.WeaponFactory data.Weapons.switchblade).entryLinked}
${(data.WeaponFactory data.Weapons.sword).entryLinked}
${(data.WeaponFactory data.Weapons.tire-iron).entryLinked}
${(data.WeaponFactory data.Weapons.walking-cane).entryLinked}
${(data.WeaponFactory data.Weapons.boxing-glove).entryLinked}
${(data.WeaponFactory data.Weapons.deathclaw-gauntlet).entryLinked}
${(data.WeaponFactory data.Weapons.knuckles).entryLinked}
${(data.WeaponFactory data.Weapons.power-fist).entryLinked}
${(data.WeaponFactory data.Weapons.gun-bash-1h).entryLinked}
${(data.WeaponFactory data.Weapons.gun-bash-2h).entryLinked}
${(data.WeaponFactory data.Weapons.rock).entryLinked}
${(data.WeaponFactory data.Weapons.unarmed).entryLinked}

---
${(data.WeaponFactory data.Weapons.baseball-bat).description}
---
${(data.WeaponFactory data.Weapons.baseball-bat-aluminum).description}
---
${(data.WeaponFactory data.Weapons.baton).description}
---
${(data.WeaponFactory data.Weapons.board).description}
---
${(data.WeaponFactory data.Weapons.combat-knife).description}
---
${(data.WeaponFactory data.Weapons.lead-pipe).description}
---
${(data.WeaponFactory data.Weapons.machete).description}
---
${(data.WeaponFactory data.Weapons.pipe-wrench).description}
---
${(data.WeaponFactory data.Weapons.pool-cue).description}
---
${(data.WeaponFactory data.Weapons.ripper).description}
---
${(data.WeaponFactory data.Weapons.rolling-pin).description}
---
${(data.WeaponFactory data.Weapons.shishkebab).description}
---
${(data.WeaponFactory data.Weapons.sledgehammer).description}
---
${(data.WeaponFactory data.Weapons.super-sledge).description}
---
${(data.WeaponFactory data.Weapons.switchblade).description}
---
${(data.WeaponFactory data.Weapons.sword).description}
---
${(data.WeaponFactory data.Weapons.tire-iron).description}
---
${(data.WeaponFactory data.Weapons.walking-cane).description}
---
${(data.WeaponFactory data.Weapons.boxing-glove).description}
---
${(data.WeaponFactory data.Weapons.deathclaw-gauntlet).description}
---
${(data.WeaponFactory data.Weapons.knuckles).description}
---
${(data.WeaponFactory data.Weapons.power-fist).description}
---
${(data.WeaponFactory data.Weapons.gun-bash-1h).description}
---
${(data.WeaponFactory data.Weapons.gun-bash-2h).description}
---
${(data.WeaponFactory data.Weapons.rock).description}
---
${(data.WeaponFactory data.Weapons.unarmed).description}
''

in Document