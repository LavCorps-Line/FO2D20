-- src/weapons/explosives/index.dhall

let data = ./src/weapons.dhall

let Document =
''
| Explosive | Weapon Type | Damage Rating | Damage Effects | Damage Type | Qualities | Weight | Cost | Rarity |
|-----------|-------------|---------------|----------------|-------------|-----------|--------|------|--------|
${(data.WeaponFactory data.Weapons.baseball-grenade).entryLinked}
${(data.WeaponFactory data.Weapons.bottlecap-mine).entryLinked}
${(data.WeaponFactory data.Weapons.frag-grenade).entryLinked}
${(data.WeaponFactory data.Weapons.frag-mine).entryLinked}
${(data.WeaponFactory data.Weapons.molotov-cocktail).entryLinked}
${(data.WeaponFactory data.Weapons.nuka-grenade).entryLinked}
${(data.WeaponFactory data.Weapons.nuke-mine).entryLinked}
${(data.WeaponFactory data.Weapons.plasma-grenade).entryLinked}
${(data.WeaponFactory data.Weapons.plasma-mine).entryLinked}
${(data.WeaponFactory data.Weapons.pulse-grenade).entryLinked}
${(data.WeaponFactory data.Weapons.pulse-mine).entryLinked}

---
${(data.WeaponFactory data.Weapons.baseball-grenade).description}
---
${(data.WeaponFactory data.Weapons.bottlecap-mine).description}
---
${(data.WeaponFactory data.Weapons.frag-grenade).description}
---
${(data.WeaponFactory data.Weapons.frag-mine).description}
---
${(data.WeaponFactory data.Weapons.molotov-cocktail).description}
---
${(data.WeaponFactory data.Weapons.nuka-grenade).description}
---
${(data.WeaponFactory data.Weapons.nuke-mine).description}
---
${(data.WeaponFactory data.Weapons.plasma-grenade).description}
---
${(data.WeaponFactory data.Weapons.plasma-mine).description}
---
${(data.WeaponFactory data.Weapons.pulse-grenade).description}
---
${(data.WeaponFactory data.Weapons.pulse-mine).description}
''

in Document