-- src/weapons/small/index.dhall

let data = ./src/weapons.dhall

let Document =
''
| Small Gun | Weapon Type | Damage Rating | Damage Effects | Damage Type | Fire Rate | Range | Qualities | Weight | Cost | Rarity |
|-----------|-------------|---------------|----------------|-------------|-----------|-------|-----------|--------|------|--------|
${(data.WeaponFactory data.Weapons.pipe-bolt-action).entryLinked}
${(data.WeaponFactory data.Weapons.pipe-gun).entryLinked}
${(data.WeaponFactory data.Weapons.pipe-revolver).entryLinked}
${(data.WeaponFactory data.Weapons.pistol-44cal).entryLinked}
${(data.WeaponFactory data.Weapons.pistol-10mm).entryLinked}
${(data.WeaponFactory data.Weapons.pistol-flare).entryLinked}
${(data.WeaponFactory data.Weapons.rifle-assault).entryLinked}
${(data.WeaponFactory data.Weapons.rifle-combat).entryLinked}
${(data.WeaponFactory data.Weapons.rifle-gauss).entryLinked}
${(data.WeaponFactory data.Weapons.rifle-hunting).entryLinked}
${(data.WeaponFactory data.Weapons.rifle-railway).entryLinked}
${(data.WeaponFactory data.Weapons.shotgun-combat).entryLinked}
${(data.WeaponFactory data.Weapons.shotgun-double-barrel).entryLinked}
${(data.WeaponFactory data.Weapons.submachine-gun).entryLinked}
${(data.WeaponFactory data.Weapons.syringer).entryLinked}

---
${(data.WeaponFactory data.Weapons.pipe-bolt-action).description}
---
${(data.WeaponFactory data.Weapons.pipe-gun).description}
---
${(data.WeaponFactory data.Weapons.pipe-revolver).description}
---
${(data.WeaponFactory data.Weapons.pistol-44cal).description}
---
${(data.WeaponFactory data.Weapons.pistol-10mm).description}
---
${(data.WeaponFactory data.Weapons.pistol-flare).description}
---
${(data.WeaponFactory data.Weapons.rifle-assault).description}
---
${(data.WeaponFactory data.Weapons.rifle-combat).description}
---
${(data.WeaponFactory data.Weapons.rifle-gauss).description}
---
${(data.WeaponFactory data.Weapons.rifle-hunting).description}
---
${(data.WeaponFactory data.Weapons.rifle-railway).description}
---
${(data.WeaponFactory data.Weapons.shotgun-combat).description}
---
${(data.WeaponFactory data.Weapons.shotgun-double-barrel).description}
---
${(data.WeaponFactory data.Weapons.submachine-gun).description}
---
${(data.WeaponFactory data.Weapons.syringer).description}
''

in Document