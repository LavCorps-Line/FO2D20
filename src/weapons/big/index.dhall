-- .src/weapons/big/index.dhall

let data = ./src/weapons.dhall

let Document =
''
| Big Gun | Weapon Type | Damage Rating | Damage Effects | Damage Type | Fire Rate | Range | Qualities | Weight | Cost | Rarity |
|---------|-------------|---------------|----------------|-------------|-----------|-------|-----------|--------|------|--------|
${(data.WeaponFactory data.Weapons.fat-man).entryLinked}
${(data.WeaponFactory data.Weapons.flamer).entryLinked}
${(data.WeaponFactory data.Weapons.gatling-laser).entryLinked}
${(data.WeaponFactory data.Weapons.heavy-incinerator).entryLinked}
${(data.WeaponFactory data.Weapons.junk-jet).entryLinked}
${(data.WeaponFactory data.Weapons.minigun).entryLinked}
${(data.WeaponFactory data.Weapons.missile-launcher).entryLinked}

---
${(data.WeaponFactory data.Weapons.fat-man).description}
---
${(data.WeaponFactory data.Weapons.flamer).description}
---
${(data.WeaponFactory data.Weapons.gatling-laser).description}
---
${(data.WeaponFactory data.Weapons.heavy-incinerator).description}
---
${(data.WeaponFactory data.Weapons.junk-jet).description}
---
${(data.WeaponFactory data.Weapons.minigun).description}
---
${(data.WeaponFactory data.Weapons.missile-launcher).description}
''

in Document