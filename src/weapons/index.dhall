-- src/weapons/index.dhall

let data = ./src/weapons.dhall
let utils = ./src/utils.dhall
let prelude = ./src/prelude.dhall

let mappedWeaponRecord : Type =
    { mapKey : Text
    , mapValue : data.Weapon
    }

let processWeapon = \(input : mappedWeaponRecord) ->
    (data.WeaponFactory input.mapValue).entryGeneric

let processWeaponsList = \(input : List mappedWeaponRecord) ->
    let out1 = utils.Map mappedWeaponRecord Text processWeapon input
    let output = prelude.Text.concatSep "\n" out1
    in output

let Document =
''
| Weapon | Weapon Type | Damage Rating | Damage Effects | Damage Type | Qualities | Weight | Cost | Rarity |
|--------|-------------|---------------|----------------|-------------|-----------|--------|------|--------|
${processWeaponsList (toMap data.Weapons)}
''

in Document