-- src/weapons/throwable/index.dhall

let data = ./src/weapons.dhall

let Document =
''
| Throwing Weapon | Weapon Type | Damage Rating | Damage Effects | Damage Type | Qualities | Weight | Cost | Rarity |
|-----------------|-------------|---------------|----------------|-------------|-----------|--------|------|--------|
${(data.WeaponFactory data.Weapons.javelin).entryLinked}
${(data.WeaponFactory data.Weapons.throwing-knives).entryLinked}
${(data.WeaponFactory data.Weapons.tomahawk).entryLinked}

---
${(data.WeaponFactory data.Weapons.javelin).description}
---
${(data.WeaponFactory data.Weapons.throwing-knives).description}
---
${(data.WeaponFactory data.Weapons.tomahawk).description}
''

in Document