-- src/weapons/energy/index.dhall

let data = ./src/weapons.dhall

let Document =
''
| Energy Weapon | Weapon Type | Damage Rating | Damage Effects | Damage Type | Fire Rate | Range | Qualities | Weight | Cost | Rarity |
|---------------|-------------|---------------|----------------|-------------|-----------|-------|-----------|--------|------|--------|
${(data.WeaponFactory data.Weapons.gamma-gun).entryLinked}
${(data.WeaponFactory data.Weapons.institute-laser).entryLinked}
${(data.WeaponFactory data.Weapons.laser-gun).entryLinked}
${(data.WeaponFactory data.Weapons.laser-musket).entryLinked}
${(data.WeaponFactory data.Weapons.plasma-gun).entryLinked}

---
${(data.WeaponFactory data.Weapons.gamma-gun).description}
---
${(data.WeaponFactory data.Weapons.institute-laser).description}
---
${(data.WeaponFactory data.Weapons.laser-gun).description}
---
${(data.WeaponFactory data.Weapons.laser-musket).description}
---
${(data.WeaponFactory data.Weapons.plasma-gun).description}
''

in Document