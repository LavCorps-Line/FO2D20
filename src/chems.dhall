-- src/chems.dhall

-- Imports --

let Prelude = ./prelude.dhall
let DEQ = ./genericDEQ.dhall
let Utils = ./utils.dhall

-- Type Defs --

let ChemDuration = < instant | brief | lasting >

let Chem : Type =
    { addictive : Natural
    , addictionType : Optional Text
    , cost : Natural
    , description : Text
    , duration : ChemDuration
    , effects : List DEQ.GenericDEQ
    , name : Text
    , rarity : Natural
    , weight : Natural
    }

let ProcessedChem : Type =
    { addictive : Text
    , cost : Text
    , description : Text
    , duration : Text
    , effects : Text
    , entryGeneric : Text
    , entryLinked : Text
    , name : Text
    , rarity : Text
    , slug : Text
    , weight : Text
    }

-- Variables --

let AddictionTypes =
    { buffout =
        ''
        * **Buffout Addiction**: A failed addiction roll renders you addicted to Buffout. You
        increase the difficulty of all **STR** and **END** tests by +1 whenever you are
        not under the effects of a type of Buffout (Buffout, Buffjet, or Bufftats).
        ''
    , calmex =
        ''
        * **Calmex Addiction**: A failed addiction roll renders you addicted to Calmex. You
        suffer complications on **AGI** tests on all rolls of 18 or higher whenever you
        are not under the effects of Calmex.
        ''
    , daddy-o =
        ''
        * **Daddy-O Addiction**: A failed addiction roll renders you addicted to Daddy-O. You
        increase difficulty of all **PER** and **INT** tests by +1 whenever you are not
        under the effects of Daddy-O
        ''
    , day-tripper =
        ''
        * **Day Tripper Addiction**: A failed addiction roll renders you addicted to Day
        Tripper. You increase the difficulty of all **CHA** and **LCK** tests by +1
        whenever you are not under the effects of Day Tripper.
        ''
    , fury =
        ''
        * **Fury Addiction**: A failed addiction renders you addicted to Fury. You
        increase the difficulty of all **STR** and **PER** tests by +1 whenever you are
        not under the effects of Fury.
        ''
    , jet =
        ''
        * **Jet Addiction**: A failed addiction roll renders you addicted to Jet. You
        increase the difficulty of all **AGI** tests by +1 whenever you are not under
        the effects of a type of Jet (Jet, or Jet Fuel; Ultra Jet has a different
        addiction effect).
        ''
    , med-x =
        ''
        * **Med-X Addiction**: A failed addiction roll renders you addicted to Med-X. You
        increase the difficulty of all **AGI** tests by +1, and suffer +1 additional
        damage from all physical attacks whenever you are not under the effects of Med-X.
        ''
    , mentat =
        ''
        * **Mentat Addiction**: A failed addiction roll renders you addicted to Mentats.
        You increase the difficulty of all **CHA** tests by +1 whenever you are not under
        the effects of a type of Mentat (ordinary Mentats, or the Berry, Grape, or Orange
        versions).
        ''
    , overdrive =
        ''
        * **Overdrive Addiction**: A failed addiction roll renders you addicted to
        Overdrive. You increase the difficulty of all **STR** and **AGI** tests by +1
        whenever you are not under the effects of Overdrive.
        ''
    , psycho =
        ''
        * **Psycho Addiction**: A failed addiction roll renders you addicted to Psycho.
        You increase the difficulty of all **STR** tests by +1 and suffer +1 damage from
        all physical attacks whenever you are not under the effects of a type of Psycho
        (ordinary Psycho, Psycho Jet, Psychobuff, or Psychotats).
        ''
    , ultra-jet =
        ''
        * **Ultra Jet Addiction**: A failed addiction roll renders you addicted to Ultra
        Jet. You increase the difficulty of all **AGI** tests by +1, and you generate one
        fewer AP when you succeed at a skill test (minimum 0) whenever you are not under
        the effects of Ultra Jet. This addiction si *permanent* and cannot be cured by
        any known means.
        ''
    , x-cell =
        ''
        * **X-Cell Addiction**: A failed addiction roll renders you addicted to X-Cell.
        You increase the difficulty of all tests by +1 whenever you are not under the
        effects of X-Cell.
        ''
    }

let ChemDurationHandlers =
    { instant = Utils.Tooltipify "Instant" "Takes effect as soon as taken."
    , brief = Utils.Tooltipify "Brief" "Lasts until the end of the recipient's turn."
    , lasting = Utils.Tooltipify "Lasting" "Lasts until the end of the current scene."
    }

-- Type Factories --

let ChemFactory : forall(chem : Chem) -> ProcessedChem = \(chem : Chem) ->
    let addictive = if (Prelude.Natural.equal chem.addictive 0) then (Utils.Mono "No") else (Utils.Mono "Yes ${Natural/show chem.addictive}")
    let cost = Utils.Tooltipify (Utils.ParseValue chem.cost) "in caps"
    let duration = merge ChemDurationHandlers chem.duration
    let effects = DEQ.ProcessDEQList chem.effects
    let name = chem.name
    let rarity = Utils.Mono (Utils.ParseValue chem.rarity)
    let slug = Utils.Sluginator name
    let weight = Utils.Tooltipify (Utils.ConvertWeight chem.weight) "in pounds (lbs)"

    let entryGeneric = Prelude.Text.concat
    [ "| "
    , "`${name}` | "
    , "${duration} | "
    , "${addictive} | "
    , "${weight} | "
    , "${cost} | "
    , "${rarity} |"
    ]

    let entryLinked = Prelude.Text.concat
    [ "| "
    , "[`${name}`](#${slug}) | "
    , "${duration} | "
    , "${addictive} | "
    , "${weight} | "
    , "${cost} | "
    , "${rarity} |"
    ]


    let attributes = Prelude.Text.concat
    [ if Prelude.Natural.equal (List/length DEQ.GenericDEQ chem.effects) 0 then "" else "* Effects: ${effects}\n"
    , "* Addictive: ${addictive}\n"
    , Prelude.Optional.default Text "" chem.addictionType
    , "* Duration: ${duration}\n"
    , "* Weight: ${weight}\n"
    , "* Cost: ${cost}\n"
    , "* Rarity: ${rarity}\n"
    ]

    let description = Prelude.Text.concat
    [ "## ${name}\n\n"
    , "${chem.description}\n"
    , "${attributes}\n"
    ]

    in
    { addictive = addictive
    , cost = cost
    , description = description
    , duration = duration
    , effects = effects
    , entryGeneric = entryGeneric
    , entryLinked = entryLinked
    , name = name
    , rarity = rarity
    , slug = slug
    , weight = weight
    }

-- Item Defs --

-- Chems

let addictol : Chem =
    { addictive = 0
    , addictionType = None Text
    , cost = 125
    , description =
        ''
        A powerful and effective pre-War medicine which cleanses the body of the effects
        of addiction and the withdrawal symptoms that accompany it. It doesn't, however,
        prevent subsequent addictions, especially in the cases of long-term of habitual
        drug abuse. It can take some time to work, especially in severe cases, and tends
        to have a few side-effects, such as nausea and dizziness, while it is taking
        effect.
        ''
    , duration = ChemDuration.instant
    , effects =
        [ DEQ.GenericDEQFactory "Removes all addictions" "All addictions afflicting the user are removed."
        ]
    , name = "Addictol"
    , rarity = 3
    , weight = 0
    }

let antibiotics : Chem =
    { addictive = 0
    , addictionType = None Text
    , cost = 75
    , description =
        ''
        A dose of potent, broad-spectrum antibiotics that'll help clear out pretty much
        any infection or disease. Taking too many can be damaging to overall health, but
        these are rare enough that taking too many is rarely a problem.
        ''
    , duration = ChemDuration.instant
    , effects =
        [ DEQ.GenericDEQFactory "Cures all illnesses" "All illnesses afflicting the user are cured."
        ]
    , name = "Antibiotics"
    , rarity = 3
    , weight = 0
    }

let buffjet : Chem =
    { addictive = 1
    , addictionType = Some AddictionTypes.buffout
    , cost = 75
    , description =
        ''
        A potent, if highly addictive mixture of Buffout and Jet, providing a boost to
        physical prowess, heightened reflexes, and a burst of adrenaline.
        ''
    , duration = ChemDuration.brief
    , effects =
        [ DEQ.ChemSkillChallengeFactory True 1 [ "STR", "END" ]
        , DEQ.GenericDEQFactory "Gain 3 AP immediately" "3 AP is added to your party's AP pool. If unspent, it will be lost."
        , DEQ.ChemCheapActionFactory 1
        , DEQ.ChemMaxHPChangeFactory +4
        ]
    , name = "Buffjet"
    , rarity = 4
    , weight = 0
    }

let buffout : Chem =
    { addictive = 2
    , addictionType = Some AddictionTypes.buffout
    , cost = 45
    , description =
        ''
        A powerful and quick-acting steroid which gained popularity with athletes prior
        to the Great War. For a few minutes, it makes one stronger and tougher.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemSkillRerollFactory 1 [ "STR", "END" ]
        , DEQ.ChemMaxHPChangeFactory +3
        ]
    , name = "Buffout"
    , rarity = 2
    , weight = 0
    }

let bufftats : Chem =
    { addictive = 1
    , addictionType = Some AddictionTypes.buffout
    , cost = 75
    , description =
        ''
        An addictive cocktail of Buffout steroids and Mentats, heightening both physical
        prowess and awareness.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemSkillChallengeFactory True 1 [ "STR", "PER", "END" ]
        , DEQ.ChemMaxHPChangeFactory +4
        ]
    , name = "Bufftats"
    , rarity = 4
    , weight = 0
    }

let calmex : Chem =
    { addictive = 1
    , addictionType = Some AddictionTypes.calmex
    , cost = 100
    , description =
        ''
        A light tranquilizer used to calm the nerves. It isn't potent enough to function
        as a painkiller, but a dose can quiet anxieties and fears, keeping panic from
        disturbing delicate activities.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemSkillRerollFactory 1 [ "PER", "AGI" ]
        , DEQ.GenericDEQFactory "+2 CD to sneak attack damage" "Increase sneak attack damage by +2 CD."
        ]
    , name = "Calmex"
    , rarity = 4
    , weight = 0
    }

let daddy-o : Chem =
    { addictive = 1
    , addictionType = Some AddictionTypes.daddy-o
    , cost = 50
    , description =
        ''
        Popular with beatniks and intellectuals before the Great War, Daddy-O heightens
        the user's cognitive faculties, making them more alert and more able to process
        information, but users tend to hyper-focus on the task in front of them, making
        it awkward to interact with them.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemSkillChallengeFactory True 1 [ "PER", "INT" ]
        , DEQ.ChemSkillChallengeFactory False 1 [ "CHA" ]
        ]
    , name = "Daddy-O"
    , rarity = 2
    , weight = 0
    }

let day-tripper : Chem =
    { addictive = 1
    , addictionType = Some AddictionTypes.day-tripper
    , cost = 40
    , description =
        ''
        A mild pre-War relaxant and hallucinogen, Day Tripper was favored by Americans
        seeking a brief escape from reality. The resulting high made users more laid-back
        and relaxed, while also making them less inclined to exert themselves physically.
        It was popular amongst many social groups during the stresses of the Great War,
        food riots, and general societal problems in the years before the bombs fell.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemSkillChallengeFactory True 1 [ "CHA", "LCK" ]
        , DEQ.ChemSkillChallengeFactory False 1 [ "STR" ]
        ]
    , name = "Day Tripper"
    , rarity = 3
    , weight = 0
    }

let fury : Chem =
    { addictive = 1
    , addictionType = Some AddictionTypes.fury
    , cost = 30
    , description =
        ''
        An extremely powerful combat stimulant, Fury grants users a sense of
        invincibility, rendering them nearly immune to pain and recklessly dangerous in
        melee combat, while also rendering them largely oblivious to everything else as
        well.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemDefenseBuffFactory 3 DEQ.DamageTypes.physical
        , DEQ.ChemDamageBuffFactory 3 "Melee"
        , DEQ.ChemSkillChallengeFactory False 2 [ "PER" ]
        ]
    , name = "Fury"
    , rarity = 4
    , weight = 0
    }

let healing-salve : Chem =
    { addictive = 0
    , addictionType = None Text
    , cost = 20
    , description =
        ''
        A salve or ointment which can be applied to reduce pain and speed recovery from
        injury, typically made from several natural herbal ingredients.
        ''
    , duration = ChemDuration.instant
    , effects =
        [ DEQ.ChemHealFactory 2
        ]
    , name = "Healing Salve"
    , rarity = 1
    , weight = 0
    }

let jet : Chem =
    { addictive = 2
    , addictionType = Some AddictionTypes.jet
    , cost = 50
    , description =
        ''
        Jet is an inhaled stimulant which creates an altered state of consciousness where
        time appears to slow, heightening reflexes and allowing the user to act more
        quickly during a moment of crisis.
        ''
    , duration = ChemDuration.brief
    , effects =
        [ DEQ.ChemCheapActionFactory 1
        ]
    , name = "Jet"
    , rarity = 2
    , weight = 0
    }

let jet-fuel : Chem =
    { addictive = 1
    , addictionType = Some AddictionTypes.jet
    , cost = 60
    , description =
        ''
        A volatile variant of Jet, Jet Fuel provides a massive burst of energy in the
        user, allowing them to act more swiftly and decisively.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.GenericDEQFactory "Regenerating AP" "Gain 1 free AP at the start of each turn"
        ]
    , name = "Jet Fuel"
    , rarity = 3
    , weight = 0
    }

let med-x : Chem =
    { addictive = 2
    , addictionType = Some AddictionTypes.med-x
    , cost = 50
    , description =
        ''
        Med-X is a potent opiate analgesic which significantly reduces both the
        perception of pain and the natural emotional response to pain. In short, it's a
        powerful painkiller.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemDefenseBuffFactory 3 DEQ.DamageTypes.physical
        ]
    , name = "Med-X"
    , rarity = 20
    , weight = 0
    }

let mentats : Chem =
    { addictive = 3
    , addictionType = Some AddictionTypes.mentat
    , cost = 50
    , description =
        ''
        Mentats were a popular recreational and performance-enhancing drug before the
        Great War, which enhance memory and speed mental processes. It was popular
        amongst students studying for exams, and amongst armchair philosophers and those
        in jobs requiring intensive thought. The effect lasts only a short time, leaving
        the user feeling tired and unfocused afterwards, and they are addictive by
        design, with withdrawal causing migraines and irritability.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemSkillRerollFactory 1 [ "PER", "INT" ]
        ]
    , name = "Mentats"
    , rarity = 2
    , weight = 0
    }

let mentats-berry : Chem =
    { addictive = 2
    , addictionType = Some AddictionTypes.mentat
    , cost = 60
    , description =
        ''
        A portion of Mentats reformulated to promote brain activity and memory.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemSkillChallengeFactory True 2 [ "INT" ]
        ]
    , name = "Berry Mentats"
    , rarity = 3
    , weight = 0
    }

let mentats-grape : Chem =
    { addictive = 2
    , addictionType = Some AddictionTypes.mentat
    , cost = 60
    , description =
        ''
        A tin of Mentats, their formula altered to reduce social anxiety, boost
        confidence, and make users more aware of body language and other social cues.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemSkillChallengeFactory True 2 [ "CHA" ]
        , DEQ.ChemSkillRerollFactory 1 [ "Barter" ]
        ]
    , name = "Grape Mentats"
    , rarity = 3
    , weight = 0
    }

let mentats-orange : Chem =
    { addictive = 2
    , addictionType = Some AddictionTypes.mentat
    , cost = 60
    , description =
        ''
        A variant of Mentats which heighten awareness and sensory acuity.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemSkillChallengeFactory True 2 [ "PER" ]
        , DEQ.GenericDEQFactory "Better Aim" "Aim minor action lets you re-roll one additional d20."
        ]
    , name = "Orange Mentats"
    , rarity = 3
    , weight = 0
    }

let overdrive : Chem =
    { addictive = 1
    , addictionType = Some AddictionTypes.overdrive
    , cost = 55
    , description =
        ''
        An enhanced form of Psycho which massively stimulates aggression, making the user
        more dangerous in combat.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemDamageBuffFactory 3 "All"
        , DEQ.GenericDEQFactory "Damage Re-Roll" "You may re-roll up to 3 CD per damage roll."
        ]
    , name = "Overdrive"
    , rarity = 3
    , weight = 0
    }

let psycho : Chem =
    { addictive = 2
    , addictionType = Some AddictionTypes.psycho
    , cost = 50
    , description =
        ''
        A potent combat stimulant created by the U.S. Army to enhance the effectiveness
        of their soldiers. It's still common in the wasteland, as the post-War plant
        Hubflower has proven to replicate several of the ingredients of the pre-War
        version. Psycho stimulates aggression and dulls pain receptors, allowing the user
        to fight harder with less care for their own safety.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemDamageBuffFactory 2 "All"
        , DEQ.ChemDefenseBuffFactory 3 DEQ.DamageTypes.physical
        ]
    , name = "Psycho"
    , rarity = 2
    , weight = 0
    }

let psycho-jet : Chem =
    { addictive = 1
    , addictionType = Some AddictionTypes.psycho
    , cost = 70
    , description =
        ''
        A powerful, addictive cocktail of Psycho and Jet, which enhances aggression,
        dulls pain, and augments a target's reflexes and energy levels. The effect lasts
        only for a short while--under a minute--but the effects are always an explosive
        burst of extreme violence.
        ''
    , duration = ChemDuration.brief
    , effects =
        [ DEQ.ChemDamageBuffFactory 2 "All"
        , DEQ.ChemDefenseBuffFactory 4 DEQ.DamageTypes.physical
        , DEQ.GenericDEQFactory "Gain 4 AP immediately" "4 AP is added to your party's AP pool. If unspent, it will be lost."
        ]
    , name = "Psycho Jet"
    , rarity = 4
    , weight = 0
    }

let psychobuff : Chem =
    { addictive = 1
    , addictionType = Some AddictionTypes.psycho
    , cost = 70
    , description =
        ''
        A highly addictive cocktail of Psycho and Buffout steroids, combining the effects
        of the two drugs to produce a period of extreme strength, durability, and
        aggression.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemDamageBuffFactory 2 "All"
        , DEQ.ChemMaxHPChangeFactory +4
        , DEQ.ChemSkillChallengeFactory True 1 [ "STR", "END" ]
        ]
    , name = "Psychobuff"
    , rarity = 4
    , weight = 0
    }

let psychotats : Chem =
    { addictive = 1
    , addictionType = Some AddictionTypes.psycho
    , cost = 70
    , description =
        ''
        A cocktail of Psycho and Mentats. The heightened awareness by the Mentats and the
        aggression and pain insensitivity somewhat cancel one another out, but the
        resultant effects are a short period of high aggression, enhanced sensory acuity,
        and boosted pain tolerance.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemDamageBuffFactory 2 "All"
        , DEQ.ChemDefenseBuffFactory 2 DEQ.DamageTypes.physical
        , DEQ.ChemSkillChallengeFactory True 1 [ "PER" ]
        ]
    , name = "Psychotats"
    , rarity = 4
    , weight = 0
    }

let rad-x : Chem =
    { addictive = 0
    , addictionType = None Text
    , cost = 40
    , description =
        ''
        A preventative medicine for those intending to enter areas with heightened risk
        of radiation exposure. A dose can help you resist the effects of radiation,
        especially when combined with other protective measures.

        Due to scarcity, it is often found diluted, reducing the effectiveness of a dose
        but allowing limited supplies to last longer. Diluted Rad-X is about half as
        effective, but easier to find.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemDefenseBuffFactory 6 DEQ.DamageTypes.radiation
        ]
    , name = "Rad-X"
    , rarity = 2
    , weight = 0
    }

let rad-x-diluted : Chem =
    { addictive = 0
    , addictionType = None Text
    , cost = 25
    , description =
        rad-x.description
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemDefenseBuffFactory 3 DEQ.DamageTypes.radiation
        ]
    , name = "Rad-X (Diluted)"
    , rarity = 1
    , weight = 0
    }

let radaway : Chem =
    { addictive = 0
    , addictionType = None Text
    , cost = 80
    , description =
        ''
        An intravenous drug which purges radiation from the user's bloodstream. It's also
        a potent diuretic, expelling the radiation with the user's urine, so it's
        advisable to drink a lot of (clean) water shortly after taking a dose.

        Sometimes encountered in a diluted form, reducing its effectiveness as the user
        is only taking a partial dose. Diluted RadAway is only about half as effective,
        but it's easier to find.
        ''
    , duration = ChemDuration.instant
    , effects =
        [ DEQ.ChemHealRadFactory 4
        ]
    , name = "RadAway"
    , rarity = 2
    , weight = 0
    }

let radaway-diluted : Chem =
    { addictive = 0
    , addictionType = None Text
    , cost = 50
    , description =
        radaway.description
    , duration = ChemDuration.instant
    , effects =
        [ DEQ.ChemHealRadFactory 2
        ]
    , name = "RadAway (Diluted)"
    , rarity = 1
    , weight = 0
    }

let skeeto-spit : Chem =
    { addictive = 0
    , addictionType = None Text
    , cost = 40
    , description =
        ''
        Derived from the secretions of a Bloodbug, and the curative properties of some
        wasteland plants, Skeeto Spit is a wasteland remedy which dulls the user's
        ability to feel pain. It's effective, lasts a reasonable amount of time, and is
        non-addictive, but it normally has to be home-made as it isn't generally produced
        in large quantities.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.ChemMaxHPChangeFactory +2
        ]
    , name = "Skeeto Spit"
    , rarity = 2
    , weight = 0
    }

let stimpak : Chem =
    { addictive = 0
    , addictionType = None Text
    , cost = 50
    , description =
        ''
        A stimulation delivery package, or Stimpak, is a form of fast-acting medication
        used to bolster the body's own healing and recuperative ability, and countless
        millions were produced for household and workplace first aid kits, hospitals,
        clinics, and other places. A wonder of pre-War science, a Stimpak is a single-use
        syringe filled with a variety of potent healing agents, stimulants, and
        painkillers, and can be applied in an intravenous or intramuscular fashion:
        literally stick it anywhere and it'll work. Even so, a Stimpak is most effective
        when applied by an Auto-Doc or someone with medical training alongside other
        treatment.

        Sometimes encountered in a diluted form, reducing its effectiveness as the user
        is only taking a partial dose. Diluted Stimpaks are only about half as effective,
        but they're easier to find.
        ''
    , duration = ChemDuration.instant
    , effects =
        [ DEQ.ChemHealFactory 4
        ]
    , name = "Stimpak"
    , rarity = 2
    , weight = 0
    }

let stimpak-diffuser : Chem =
    { addictive = 0
    , addictionType = None Text
    , cost = 200
    , description =
        ''
        This delivery mechanism allows the contents of a super Stimpak to be dispersed
        into an aerosol cloud, providing a burst of medicinal vapor over a small area.
        ''
    , duration = ChemDuration.instant
    , effects =
        [ DEQ.GenericDEQFactory "Heals 4 HP to all within Close range" "All biological creatures within Close range of you are healed by 4 HP."
        ]
    , name = "Stimpak Diffuser"
    , rarity = 5
    , weight = 0
    }

let stimpak-diluted : Chem =
    { addictive = 0
    , addictionType = None Text
    , cost = 30
    , description =
        stimpak.description
    , duration = ChemDuration.instant
    , effects =
        [ DEQ.ChemHealFactory 2
        ]
    , name = "Stimpak (Diluted)"
    , rarity = 1
    , weight = 0
    }

let stimpak-super : Chem =
    { addictive = 0
    , addictionType = None Text
    , cost = 90
    , description =
        ''
        A super Stimpak is an enhanced version of the standard Stimpak, with an
        additional vial of medicine and a leather strap to secure the needle to the
        patient's arm during treatment. A more potent cocktail of medication is used in a
        super Stimpak, allowing it to heal much more effectively.
        ''
    , duration = ChemDuration.instant
    , effects =
        [ DEQ.ChemHealFactory 8
        ]
    , name = "Super Stimpak"
    , rarity = 4
    , weight = 0
    }

let ultra-jet : Chem =
    { addictive = 3
    , addictionType = Some AddictionTypes.ultra-jet
    , cost = 67
    , description =
        ''
        An extremely concentrated form of Jet, it is significantly more potent, and
        affects Ghouls as easily as it does humans due to its potency.
        ''
    , duration = ChemDuration.brief
    , effects =
        [ DEQ.GenericDEQFactory "Gain 6 AP immediately" "6 AP is added to your party's AP pool. If unspent, it will be lost."
        ]
    , name = "Ultra Jet"
    , rarity = 2
    , weight = 0
    }

let x-cell : Chem =
    { addictive = 1
    , addictionType = Some AddictionTypes.x-cell
    , cost = 60
    , description =
        ''
        X-cell was a general-purpose performance enhancer still being developed before
        the Great War. Development was never finished, but prototype versions were
        distributed through the black market and by scavengers after the War. It enhances
        all aspects of a user's abilities for a few minutes, but it is extremely
        addictive, and the withdrawal symptoms are as far-reaching as the effects.
        ''
    , duration = ChemDuration.lasting
    , effects =
        [ DEQ.GenericDEQFactory "Free d20" "The first extra d20 bought on all tests is free."
        ]
    , name = "X-Cell"
    , rarity = 4
    , weight = 0
    }

-- Output --

in  { ChemDuration
    , Chem
    , ProcessedChem
    , AddictionTypes
    , ChemDurationHandlers
    , ChemFactory
    , Chems =
        { addictol
        , antibiotics
        , buffjet
        , buffout
        , bufftats
        , calmex
        , daddy-o
        , day-tripper
        , fury
        , healing-salve
        , jet
        , jet-fuel
        , med-x
        , mentats
        , mentats-berry
        , mentats-grape
        , mentats-orange
        , overdrive
        , psycho
        , psycho-jet
        , psychobuff
        , psychotats
        , rad-x
        , rad-x-diluted
        , radaway
        , radaway-diluted
        , skeeto-spit
        , stimpak
        , stimpak-diffuser
        , stimpak-diluted
        , stimpak-super
        , ultra-jet
        , x-cell
        }
    }
