-- src/utils.dhall

-- Imports --

let Prelude = ./prelude.dhall

-- Variables --

let Values =
    { NotApplicable = 85762695512320 -- "N/A" in UTF-16 -> Hexadecimal -> Decimal
    , Inherited = 80265657468928 -- "INH" in UTF-16 -> Hexadecimal -> Decimal
    , ToDo = 92360084046848 -- "TBD" in UTF-16 -> Hexadecimal -> Decimal
    }

-- Functions --

let ParseValue = \(input : Natural) ->
    if (Prelude.Natural.equal input Values.NotApplicable) then "N/A" else
    if (Prelude.Natural.equal input Values.Inherited) then "Inherited" else
    if (Prelude.Natural.equal input Values.ToDo) then "TODO" else (Natural/show input)

let Exists = \(type : Type) -> \(smth : Optional type) ->
    merge
        { Some = \(_ : type) -> True
        , None = False
        } smth

let ConvertRange = \(input : Natural) ->
    if (Prelude.Natural.lessThanEqual input 0) then "C" else
        if (Prelude.Natural.equal input 1) then "M" else
        if (Prelude.Natural.equal input 2) then "L" else "X"

let ConvertWeight = \(input : Natural) ->
    if (Natural/isZero input) then "<1" else (ParseValue input)

let Map
    : forall(a : Type) -> forall(b : Type) -> (a -> b) -> List a -> List b
    = \(a : Type) ->
      \(b : Type) ->
      \(f : a -> b) ->
      \(xs : List a) ->
        List/build
          b
          ( \(list : Type) ->
            \(cons : b -> list -> list) ->
              List/fold a xs list (\(x : a) -> cons (f x))
          )

let Mono = \(input : Text) ->
    Text/replace "``" "" ("`${input}`")

let ProcessList = \(input: List Text) ->
    Text/replace "``" "" (Prelude.Text.concatSep ", " (Map Text Text Mono input))

let Sluginator
    : Text -> Text
    = List/fold
        (Text -> Text)
        [ Text/replace "A" "a"
        , Text/replace "B" "b"
        , Text/replace "C" "c"
        , Text/replace "D" "d"
        , Text/replace "E" "e"
        , Text/replace "F" "f"
        , Text/replace "G" "g"
        , Text/replace "H" "h"
        , Text/replace "I" "i"
        , Text/replace "J" "j"
        , Text/replace "K" "k"
        , Text/replace "L" "l"
        , Text/replace "M" "m"
        , Text/replace "N" "n"
        , Text/replace "O" "o"
        , Text/replace "P" "p"
        , Text/replace "Q" "q"
        , Text/replace "R" "r"
        , Text/replace "S" "s"
        , Text/replace "T" "t"
        , Text/replace "U" "u"
        , Text/replace "V" "v"
        , Text/replace "W" "w"
        , Text/replace "X" "x"
        , Text/replace "Y" "y"
        , Text/replace "Z" "z"
        , Text/replace " " "-"
        , Text/replace "'" ""
        , Text/replace "." ""
        , Text/replace "(" ""
        , Text/replace ")" ""
        , Text/replace "/" ""
        , Text/replace "!" ""
        ]
        Text
        (\(replacement : Text -> Text) -> replacement)

let Tooltipify = \(name : Text) -> \(tooltip : Text) ->
    let out = "[${Mono name}](# \"${tooltip}\")"
    in out

let Zip = \(t : Type) -> \(l : List t) ->
    let ZipTool: t -> Optional t = \(i : t) ->
        Some i
    in Map t (Optional t) ZipTool l

in  { Values
    , ParseValue
    , Exists
    , ConvertRange
    , ConvertWeight
    , Map
    , Mono
    , ProcessList
    , Sluginator
    , Tooltipify
    , Zip
    }
