-- src/mods.dhall

-- Imports --

let Prelude = ./prelude.dhall
let DEQ = ./genericDEQ.dhall
let Perks = ./perks.dhall
let Utils = ./utils.dhall

-- Type Defs --

let Mod : Type =
    { complexity : Natural
    , cost : Integer
    , effects : List DEQ.GenericDEQ
    , mtype : Text
    , name : Text
    , prefix : Text
    , requirements : List Perks.Perk
    , weight : Integer
    }

let ProcessedMod : Type =
    { complexity : Text
    , cost : Text
    , effects : Text
    , entryGeneric : Text
    , mtype : Text
    , name : Text
    , prefix : Text
    , requirements : Text
    , weight : Text
    }

-- Functions --

let ProcessMod = \(input: Mod) ->
    Prelude.Text.concat
        [ "| "
        , Utils.Mono input.name
        , " | "
        , Utils.Mono input.mtype
        , " | "
        , Utils.Mono input.prefix
        , " | "
        , DEQ.ProcessDEQList input.effects
        , " | "
        , Utils.Mono (Natural/show input.complexity)
        , " | "
        , Perks.ProcessRequirementList input.requirements
        , " | "
        , Utils.Mono (Integer/show input.weight)
        , " | "
        , Utils.Mono (Integer/show input.cost)
        , " |\n"
        ]

let ProcessMods = \(input: List Mod) ->
    let a =
    [ "| Name | Type | Prefix | Effect | Complexity | Requirements | Weight | Cost |\n"
    , "|------|------|--------|--------|------------|--------------|--------|------|\n"
    ]
    let b = Utils.Map Mod Text ProcessMod input
    let c = a # b
    let d = if (Prelude.Natural.equal (List/length Text b) 0) then ("") else (Prelude.Text.concat c)

    in d

-- Type Factories --

let ModFactory = \(mod : Mod) ->
    let complexity = Utils.Mono (Natural/show mod.complexity)
    let cost = Utils.Mono (Integer/show mod.cost)
    let effects = DEQ.ProcessDEQList mod.effects
    let mtype = Utils.Mono mod.mtype
    let name = Utils.Mono mod.name
    let prefix = Utils.Mono mod.prefix
    let requirements = Perks.ProcessRequirementList mod.requirements
    let weight = Utils.Mono (Integer/show mod.weight)

    let entryGeneric = "| `${name}` | ${mtype} | ${prefix} | ${effects} | ${complexity} | ${requirements} | ${weight} | ${cost} |"

    in
    { complexity = complexity
    , cost = cost
    , effects = effects
    , entryGeneric = entryGeneric
    , mtype = mtype
    , name = name
    , prefix = prefix
    , requirements = requirements
    , weight = weight
    }

-- Mod Defs --

-- Big Guns

let flamer-long-barrel : Mod =
    { complexity = 3
    , cost = +28
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Barrel"
    , name = "Long Barrel"
    , prefix = "Long"
    , requirements = [] : List Perks.Perk
    , weight = +2
    }

let flamer-napalm : Mod =
    { complexity = 3
    , cost = +59
    , effects =
        [ DEQ.DamageChangeFactory +1
        ]
    , mtype = "Fuel"
    , name = "Napalm"
    , prefix = "Napalmer"
    , requirements = [] : List Perks.Perk
    , weight = +7
    }

let flamer-compression-nozzle : Mod =
    { complexity = 3
    , cost = +22
    , effects =
        [ DEQ.DamageChangeFactory +1
        ]
    , mtype = "Nozzle"
    , name = "Compression Nozzle"
    , prefix = "Compressed"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let flamer-vaporization-nozzle : Mod =
    { complexity = 4
    , cost = +47
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.Effects.vicious
        ]
    , mtype = "Nozzle"
    , name = "Vaporization Nozzle"
    , prefix = "Vaporizing"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let flamer-large-tank : Mod =
    { complexity = 3
    , cost = +28
    , effects =
        [ DEQ.FireRateChangeFactory +1
        ]
    , mtype = "Propellant Tank"
    , name = "Large Tank"
    , prefix = "High Capacity"
    , requirements = [] : List Perks.Perk
    , weight = +3
    }

let flamer-huge-tank : Mod =
    { complexity = 4
    , cost = +34
    , effects =
        [ DEQ.FireRateChangeFactory +2
        ]
    , mtype = "Propellant Tank"
    , name = "Huge Tank"
    , prefix = "Max. Capacity"
    , requirements = [] : List Perks.Perk
    , weight = +6
    }

let gatling-laser-charging-barrels : Mod =
    { complexity = 7
    , cost = +357
    , effects =
        [ DEQ.DamageChangeFactory +4
        , DEQ.FireRateChangeFactory -3
        , DEQ.RangeChangeFactory +1
        ]
    , mtype = "Barrel"
    , name = "Charging Barrels"
    , prefix = "Charging"
    , requirements = [ (Perks.Perks.science 4) ]
    , weight = +10
    }

let gatling-laser-beta-wave-tuner : Mod =
    { complexity = 4
    , cost = +57
    , effects =
        [ DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.energy ) True
        ]
    , mtype = "Capacitor"
    , name = "Beta Wave Tuner"
    , prefix = "Incendiary"
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let gatling-laser-boosted-capacitor : Mod =
    { complexity = 4
    , cost = +94
    , effects =
        [ DEQ.DamageChangeFactory +1
        ]
    , mtype = "Capacitor"
    , name = "Boosted Capacitor"
    , prefix = "Boosted"
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let gatling-laser-photon-agitator : Mod =
    { complexity = 6
    , cost = +132
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Effects.vicious True
        ]
    , mtype = "Capacitor"
    , name = "Photon Agitator"
    , prefix = "Agitated"
    , requirements = [ (Perks.Perks.science 3) ]
    , weight = +3
    }

let gatling-laser-photon-exciter : Mod =
    { complexity = 6
    , cost = +19
    , effects =
        [ DEQ.ToggleDEQ DEQ.Effects.vicious True
        ]
    , mtype = "Capacitor"
    , name = "Photon Exciter"
    , prefix = "Excited"
    , requirements = [ (Perks.Perks.science 3) ]
    , weight = +1
    }

let gatling-laser-beam-focuser : Mod =
    { complexity = 4
    , cost = +22
    , effects =
        [ DEQ.RangeChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Nozzle"
    , name = "Beam Focuser"
    , prefix = "Focused"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let gatling-laser-reflex-sight : Mod =
    { complexity = 7
    , cost = +169
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Sight"
    , name = "Reflex Sight"
    , prefix = "Tactical"
    , requirements = [ (Perks.Perks.science 4) ]
    , weight = +1
    }

let junk-jet-long-barrel : Mod =
    { complexity = 3
    , cost = +20
    , effects =
        [ DEQ.RangeChangeFactory +1
        ]
    , mtype = "Barrel"
    , name = "Long Barrel"
    , prefix = "Long"
    , requirements = [ (Perks.Perks.gun-nut 1) ]
    , weight = +2
    }

let junk-jet-electrification-module : Mod =
    { complexity = 6
    , cost = +70
    , effects =
        [ DEQ.ToggleDEQ DEQ.Effects.vicious True
        , DEQ.DamageTypeChangeFactory DEQ.DamageTypes.energy
        ]
    , mtype = "Muzzle"
    , name = "Electrification Module"
    , prefix = "Electrified"
    , requirements = [ (Perks.Perks.gun-nut 2), (Perks.Perks.science 1) ]
    , weight = +1
    }

let junk-jet-ignition-module : Mod =
    { complexity = 7
    , cost = +130
    , effects =
        [ DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.energy ) True
        ]
    , mtype = "Muzzle"
    , name = "Ignition Module"
    , prefix = "Flaming"
    , requirements = [ (Perks.Perks.gun-nut 3), (Perks.Perks.science 1) ]
    , weight = +1
    }

let junk-jet-gunner-sight : Mod =
    { complexity = 2
    , cost = +5
    , effects =
        [ DEQ.GenericDEQFactory "May re-roll hit location die" "When rolling the d20 to determine hit location, you may re-roll it once."
        ]
    , mtype = "Sight"
    , name = "Gunner Sight"
    , prefix = "Tactical"
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let junk-jet-recoil-compensating-stock : Mod =
    { complexity = 2
    , cost = +40
    , effects =
        [ DEQ.RangeChangeFactory +1
        ]
    , mtype = "Stock"
    , name = "Recoil Compensating Stock"
    , prefix = "Recoil Compensated"
    , requirements = [] : List Perks.Perk
    , weight = +2
    }

let minigun-accelerated-barrel : Mod =
    { complexity = 5
    , cost = +45
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.FireRateChangeFactory +1
        , DEQ.RangeChangeFactory -1
        ]
    , mtype = "Barrel"
    , name = "Accelerated Barrel"
    , prefix = "High-Speed"
    , requirements = [ (Perks.Perks.gun-nut 3) ]
    , weight = +5
    }

let minigun-tri-barrel : Mod =
    { complexity = 6
    , cost = +75
    , effects =
        [ DEQ.DamageChangeFactory +2
        , DEQ.FireRateChangeFactory -2
        ]
    , mtype = "Barrel"
    , name = "Tri-Barrel"
    , prefix = "High-Powered"
    , requirements = [ (Perks.Perks.gun-nut 4) ]
    , weight = +5
    }

let minigun-gunner-sight : Mod =
    { complexity = 2
    , cost = +68
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Sight"
    , name = "Gunner Sight"
    , prefix = "Tactical"
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let minigun-shredder : Mod =
    { complexity = 4
    , cost = +5
    , effects =
        [ DEQ.GenericDEQFactory "Melee weapon" "Your Gun Bash attack is modified according to any additional mod effects."
        , DEQ.GenericDEQFactory "Deals Physical damage equal to Fire Rate" "Your Gun Bash attack is modified to deal damage equal to the Fire Rate of the weapon."
        ]
    , mtype = "Muzzle"
    , name = "Shredder"
    , prefix = "Shredding"
    , requirements = [ (Perks.Perks.gun-nut 2) ]
    , weight = +5
    }

let missile-launcher-triple-barrel : Mod =
    { complexity = 4
    , cost = +143
    , effects =
        [ DEQ.FireRateChangeFactory +1
        ]
    , mtype = "Barrel"
    , name = "Triple Barrel"
    , prefix = "Triple Barrel"
    , requirements = [ (Perks.Perks.gun-nut 2) ]
    , weight = +16
    }

let missile-launcher-quad-barrel : Mod =
    { complexity = 5
    , cost = +218
    , effects =
        [ DEQ.FireRateChangeFactory +2
        ]
    , mtype = "Barrel"
    , name = "Quad Barrel"
    , prefix = "Quad Barrel"
    , requirements = [ (Perks.Perks.gun-nut 3) ]
    , weight = +20
    }

let missile-launcher-bayonet : Mod =
    { complexity = 2
    , cost = +30
    , effects =
        [ DEQ.GenericDEQFactory "Melee weapon" "Your Gun Bash attack is modified according to any additional mod effects."
        , DEQ.GenericDEQFactory "Deals 4 CD Piercing 1 physical damage" "Your Gun Bash attack is modified to deal 4 combat dice of damage, inflicting Piercing 1 as an Effect."
        ]
    , mtype = "Muzzle"
    , name = "Bayonet"
    , prefix = "Bayoneted"
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let missile-launcher-stabilizer : Mod =
    { complexity = 4
    , cost = +60
    , effects =
        [ DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Muzzle"
    , name = "Stabilizer"
    , prefix = "Muzzled"
    , requirements = [ (Perks.Perks.gun-nut 2) ]
    , weight = +2
    }

let missile-launcher-night-vision-scope : Mod =
    { complexity = 6
    , cost = +248
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.accurate True
        , DEQ.ToggleDEQ DEQ.Qualities.night-vision True
        ]
    , mtype = "Sight"
    , name = "Night Vision Scope"
    , prefix = "Night-Vision"
    , requirements = [ (Perks.Perks.gun-nut 4), (Perks.Perks.science 1) ]
    , weight = +6
    }

let missile-launcher-scope : Mod =
    { complexity = 4
    , cost = +143
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.accurate True
        ]
    , mtype = "Sight"
    , name = "Scope"
    , prefix = "Scoped"
    , requirements = [ (Perks.Perks.gun-nut 2) ]
    , weight = +6
    }

let missile-launcher-targeting-computer : Mod =
    { complexity = 6
    , cost = +293
    , effects =
        [ DEQ.GenericDEQFactory "Ignore Target Cover" "When you Aim at a target, the target does not benefit from being in cover, and the bonus for aiming applies to the next attack on any subsequent turn during the scene"
        ]
    , mtype = "Sight"
    , name = "Targeting Computer"
    , prefix = "Targeting"
    , requirements = [ (Perks.Perks.gun-nut 2), (Perks.Perks.science 2) ]
    , weight = +7
    }

-- Energy Weapons

let e-automatic-barrel : Mod =
    { complexity = 4
    , cost = +24
    , effects =
        [ DEQ.DamageChangeFactory -1
        , DEQ.FireRateChangeFactory +1
        , DEQ.RangeChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Qualities.close-quarters False
        ]
    , mtype = "Barrel"
    , name = "Automatic Barrel"
    , prefix = "Automatic"
    , requirements = [ (Perks.Perks.science 1) ]
    , weight = +1
    }

let e-beam-focuser : Mod =
    { complexity = 4
    , cost = +20
    , effects =
        [ DEQ.RangeChangeFactory +1
        ]
    , mtype = "Muzzle"
    , name = "Beam Focuser"
    , prefix = "Focused"
    , requirements = [ (Perks.Perks.science 1) ]
    , weight = +1
    }

let e-beam-splitter : Mod =
    { complexity = 4
    , cost = +15
    , effects =
        [ DEQ.DamageChangeFactory -1
        , DEQ.FireRateChangeFactory -1
        , DEQ.RangeChangeFactory -1
        , DEQ.ToggleDEQ DEQ.Effects.spread True
        , DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Muzzle"
    , name = "Beam Splitter"
    , prefix = "Scattered"
    , requirements = [ (Perks.Perks.science 1) ]
    , weight = +1
    }

let e-beta-wave-tuner : Mod =
    { complexity = 2
    , cost = +30
    , effects =
        [ DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.energy ) True
        ]
    , mtype = "Capacitor"
    , name = "Beta Wave Tuner"
    , prefix = "Incendiary"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let e-boosted-capacitor : Mod =
    { complexity = 2
    , cost = +35
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.FireRateChangeFactory -1
        ]
    , mtype = "Capacitor"
    , name = "Boosted Capacitor"
    , prefix = "Boosted"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let e-bracketed-long-barrel : Mod =
    { complexity = 4
    , cost = +25
    , effects =
        [ DEQ.RangeChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Qualities.close-quarters False
        , DEQ.GenericDEQFactory "Enable Muzzle Mods" "This mod enables the usage and installation of muzzle mods on this weapon."
        ]
    , mtype = "Barrel"
    , name = "Bracketed Long Barrel"
    , prefix = ""
    , requirements = [ (Perks.Perks.science 1) ]
    , weight = +2
    }

let e-bracketed-short-barrel : Mod =
    { complexity = 3
    , cost = +6
    , effects =
        [ DEQ.GenericDEQFactory "Enable Muzzle Mods" "This mod enables the usage and installation of muzzle mods on this weapon."
        ]
    , mtype = "Barrel"
    , name = "Bracketed Short Barrel"
    , prefix = ""
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let gamma-gun-deep-dish : Mod =
    { complexity = 6
    , cost = +72
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.RangeChangeFactory +1
        ]
    , mtype = "Dish"
    , name = "Deep Dish"
    , prefix = "Long"
    , requirements = [ (Perks.Perks.science 4) ]
    , weight = +2
    }

let gamma-gun-electric-signal-carrier-antennae : Mod =
    { complexity = 5
    , cost = +30
    , effects =
        [ DEQ.DamageBaseChangeFactory 7
        , DEQ.DamageTypeChangeFactory DEQ.DamageTypes.energy
        , DEQ.ToggleDEQ DEQ.Effects.radioactive True
        ]
    , mtype = "Muzzle"
    , name = "Electric Signal Carrier Antennae"
    , prefix = "Electrified"
    , requirements = [ (Perks.Perks.science 3) ]
    , weight = +0
    }

let laser-musket-five-crank-capacitor : Mod =
    { complexity = 4
    , cost = +12
    , effects =
        [ DEQ.DamageChangeFactory +3
        , DEQ.MusketCrankFactory 5
        ]
    , mtype = "Capacitor"
    , name = "Five Crank Capacitor"
    , prefix = "Five-Crank"
    , requirements = [ (Perks.Perks.science 2) ]
    , weight = +1
    }

let e-flamer-barrel : Mod =
    { complexity = 5
    , cost = +35
    , effects =
        [ DEQ.DamageChangeFactory -2
        , DEQ.FireRateChangeFactory +2
        , DEQ.RangeChangeFactory -1
        , DEQ.ToggleDEQ DEQ.Effects.burst True
        , DEQ.ToggleDEQ DEQ.Effects.spread True
        , DEQ.ToggleDEQ DEQ.Qualities.inaccurate True
        ]
    , mtype = "Barrel"
    , name = "Flamer Barrel"
    , prefix = "Thrower"
    , requirements = [ (Perks.Perks.science 2) ]
    , weight = +1
    }

let laser-musket-four-crank-capacitor : Mod =
    { complexity = 3
    , cost = +8
    , effects =
        [ DEQ.DamageChangeFactory +2
        , DEQ.MusketCrankFactory 4
        ]
    , mtype = "Capacitor"
    , name = "Four Crank Capacitor"
    , prefix = "Four-Crank"
    , requirements = [ (Perks.Perks.science 1) ]
    , weight = +1
    }

let e-full-stock : Mod =
    { complexity = 2
    , cost = +15
    , effects =
        [ DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        , DEQ.ToggleDEQ DEQ.Qualities.close-quarters False
        ]
    , mtype = "Stock"
    , name = "Full Stock"
    , prefix = ""
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let e-gyro-compensating-lens : Mod =
    { complexity = 4
    , cost = +25
    , effects =
        [ DEQ.FireRateChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Muzzle"
    , name = "Gyro Compensating Lens"
    , prefix = "Targeting"
    , requirements = [ (Perks.Perks.science 1) ]
    , weight = +1
    }

let e-improved-barrel : Mod =
    { complexity = 4
    , cost = +26
    , effects =
        [ DEQ.DamageChangeFactory +1
        ]
    , mtype = "Barrel"
    , name = "Improved Barrel"
    , prefix = "Improved"
    , requirements = [ (Perks.Perks.science 1) ]
    , weight = +1
    }

let e-long-barrel : Mod =
    { complexity = 3
    , cost = +20
    , effects =
        [ DEQ.RangeChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Qualities.close-quarters False
        ]
    , mtype = "Barrel"
    , name = "Long Barrel"
    , prefix = "Long"
    , requirements = [] : List Perks.Perk
    , weight = +2
    }

let e-long-night-vision-scope : Mod =
    { complexity = 5
    , cost = +50
    , effects =
        [ DEQ.RangeChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Qualities.accurate True
        , DEQ.ToggleDEQ DEQ.Qualities.night-vision True
        ]
    , mtype = "Sight"
    , name = "Long Night Vision Scope"
    , prefix = "Night Vision"
    , requirements = [ (Perks.Perks.science 3) ]
    , weight = +1
    }

let e-long-scope : Mod =
    { complexity = 4
    , cost = +29
    , effects =
        [ DEQ.RangeChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Qualities.accurate True
        ]
    , mtype = "Scope"
    , name = "Long Scope"
    , prefix = "Scoped"
    , requirements = [ (Perks.Perks.science 2) ]
    , weight = +1
    }

let e-marksmans-stock : Mod =
    { complexity = 4
    , cost = +20
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.accurate True
        , DEQ.ToggleDEQ DEQ.Qualities.two-handed True
        , DEQ.ToggleDEQ DEQ.Qualities.close-quarters False
        , DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Stock"
    , name = "Marksman's Stock"
    , prefix = "Marksman's"
    , requirements = [ (Perks.Perks.gun-nut 2) ]
    , weight = +2
    }

let e-photon-agitator : Mod =
    { complexity = 4
    , cost = +35
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Effects.vicious True
        ]
    , mtype = "Capacitor"
    , name = "Photon Agitator"
    , prefix = "Agitated"
    , requirements = [ (Perks.Perks.science 2) ]
    , weight = +1
    }

let e-photon-exciter : Mod =
    { complexity = 3
    , cost = +30
    , effects =
        [ DEQ.ToggleDEQ DEQ.Effects.vicious True
        ]
    , mtype = "Capacitor"
    , name = "Photon Exciter"
    , prefix = "Excited"
    , requirements = [ (Perks.Perks.science 1) ]
    , weight = +0
    }

let e-recoil-compensating-stock : Mod =
    { complexity = 5
    , cost = +3 -- TODO: Check for errata, this doesn't seem right
    , effects =
        [ DEQ.FireRateChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Qualities.two-handed True
        , DEQ.ToggleDEQ DEQ.Qualities.close-quarters False
        , DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Stock"
    , name = "Recoil Compensating Stock"
    , prefix = "Recoil Compensated"
    , requirements = [ (Perks.Perks.gun-nut 3) ]
    , weight = +2
    }

let e-recon-scope : Mod =
    { complexity = 5
    , cost = +59
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.accurate True
        , DEQ.ToggleDEQ DEQ.Qualities.recon True
        ]
    , mtype = "Sight"
    , name = "Recon Scope"
    , prefix = "Recon"
    , requirements = [ (Perks.Perks.science 3) ]
    , weight = +1
    }

let e-reflex-sight : Mod =
    { complexity = 2
    , cost = +14
    , effects =
        [ DEQ.GenericDEQFactory "May re-roll hit location die" "When rolling the d20 to determine hit location, you may re-roll it once."
        ]
    , mtype = "Sight"
    , name = "Reflex Sight"
    , prefix = "Tactical"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let e-sharpshooters-grip : Mod =
    { complexity = 3
    , cost = +10
    , effects =
        [ DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        , DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Grip"
    , name = "Sharpshooter's Grip"
    , prefix = "Sharpshooter's"
    , requirements = [ (Perks.Perks.gun-nut 1) ]
    , weight = +0
    }

let e-short-night-vision-scope : Mod =
    { complexity = 4
    , cost = +38
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.accurate True
        , DEQ.ToggleDEQ DEQ.Qualities.night-vision True
        ]
    , mtype = "Sight"
    , name = "Short Night Vision Scope"
    , prefix = "Night Vision"
    , requirements = [ (Perks.Perks.science 2) ]
    , weight = +1
    }

let e-short-scope : Mod =
    { complexity = 2
    , cost = +11
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.accurate True
        ]
    , mtype = "Sight"
    , name = "Short Scope"
    , prefix = "Scoped"
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let gamma-gun-signal-repeater : Mod =
    { complexity = 6
    , cost = +60
    , effects =
        [ DEQ.FireRateChangeFactory +2
        , DEQ.ToggleDEQ DEQ.Effects.burst True
        , DEQ.ToggleDEQ DEQ.Qualities.blast False
        ]
    , mtype = "Muzzle"
    , name = "Signal Repeater"
    , prefix = "Automatic"
    , requirements = [ (Perks.Perks.science 4) ]
    , weight = +0
    }

let laser-musket-six-crank-capacitor : Mod =
    { complexity = 5
    , cost = +16
    , effects =
        [ DEQ.DamageChangeFactory +4
        , DEQ.MusketCrankFactory 6
        ]
    , mtype = "Capacitor"
    , name = "Six Crank Capacitor"
    , prefix = "Six-Crank"
    , requirements = [ (Perks.Perks.science 3) ]
    , weight = +2
    }

let e-sniper-barrel : Mod =
    { complexity = 4
    , cost = +30
    , effects =
        [ DEQ.DamageChangeFactory +2
        , DEQ.FireRateChangeFactory -1
        , DEQ.RangeChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Qualities.close-quarters False
        ]
    , mtype = "Barrel"
    , name = "Sniper Barrel"
    , prefix = "Sniper"
    , requirements = [ (Perks.Perks.science 1) ]
    , weight = +2
    }

let e-splitter : Mod =
    { complexity = 3
    , cost = +31
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Effects.spread True
        , DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Barrel"
    , name = "Splitter"
    , prefix = "Scattergun"
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let e-standard-stock : Mod =
    { complexity = 2
    , cost = +10
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.two-handed True
        , DEQ.ToggleDEQ DEQ.Qualities.close-quarters False
        , DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Stock"
    , name = "Standard Stock"
    , prefix = ""
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let laser-musket-three-crank-capacitor : Mod =
    { complexity = 2
    , cost = +4
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.MusketCrankFactory 3
        ]
    , mtype = "Capacitor"
    , name = "Three Crank Capacitor"
    , prefix = "Three-Crank"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

-- Melee Weapons

let baseball-bat-barbed : Mod =
    { complexity = 1
    , cost = +5
    , effects =
        [ DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Baseball Bat"
    , name = "Barbed"
    , prefix = "Barbed"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let baseball-bat-bladed : Mod =
    { complexity = 4
    , cost = +12
    , effects =
        [ DEQ.DamageChangeFactory +2
        , DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.physical ) True
        ]
    , mtype = "Baseball Bat"
    , name = "Bladed"
    , prefix = "Bladed"
    , requirements = [ (Perks.Perks.blacksmith 2) ]
    , weight = +2
    }

let baseball-bat-chain-wrapped : Mod =
    { complexity = 3
    , cost = +10
    , effects =
        [ DEQ.DamageChangeFactory +2
        ]
    , mtype = "Baseball Bat"
    , name = "Chain-Wrapped"
    , prefix = "Chain-Wrapped"
    , requirements = [ (Perks.Perks.blacksmith 1) ]
    , weight = +1
    }

let baseball-bat-sharp : Mod =
    { complexity = 2
    , cost = +7
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.physical ) True
        ]
    , mtype = "Baseball Bat"
    , name = "Sharp"
    , prefix = "Sharp"
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let baseball-bat-spiked : Mod =
    { complexity = 2
    , cost = +7
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Baseball Bat"
    , name = "Spiked"
    , prefix = "Spiked"
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let baton-electrified : Mod =
    { complexity = 4
    , cost = +15
    , effects =
        [ DEQ.DamageChangeFactory +2
        , DEQ.DamageTypeChangeFactory DEQ.DamageTypes.energy
        ]
    , mtype = "Baton"
    , name = "Electrified"
    , prefix = "Shock"
    , requirements = [ (Perks.Perks.blacksmith 2), (Perks.Perks.science 1) ]
    , weight = +0
    }

let baton-stun-pack : Mod =
    { complexity = 4
    , cost = +30
    , effects =
        [ DEQ.DamageChangeFactory +3
        , DEQ.DamageTypeChangeFactory DEQ.DamageTypes.energy
        ]
    , mtype = "Baton"
    , name = "Stun Pack"
    , prefix = "Stun"
    , requirements = [ (Perks.Perks.blacksmith 2), (Perks.Perks.science 1) ]
    , weight = +0
    }

let board-bladed : Mod =
    { complexity = 2
    , cost = +10
    , effects =
        [ DEQ.DamageChangeFactory +2
        , DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.physical ) True
        ]
    , mtype = "Board"
    , name = "Bladed"
    , prefix = "Bladed"
    , requirements = [ (Perks.Perks.blacksmith 1) ]
    , weight = +2
    }

let board-puncturing : Mod =
    { complexity = 2
    , cost = +9
    , effects =
        [ DEQ.DamageChangeFactory +2
        ]
    , mtype = "Board"
    , name = "Puncturing"
    , prefix = "Puncturing"
    , requirements = [ (Perks.Perks.blacksmith 1) ]
    , weight = +1
    }

let board-spiked : Mod =
    { complexity = 1
    , cost = +6
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Board"
    , name = "Spiked"
    , prefix = "Spiked"
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let combat-knife-serrated-blade : Mod =
    { complexity = 3
    , cost = +12
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.physical ) True
        ]
    , mtype = "Combat Knife"
    , name = "Serrated Blade"
    , prefix = "Serrated"
    , requirements = [ (Perks.Perks.blacksmith 1) ]
    , weight = +0
    }

let combat-knife-stealth-blade : Mod =
    { complexity = 4
    , cost = +18
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.physical ) True
        , DEQ.GenericDEQFactory "+2 Damage on Sneak Attacks" "Add +2 Damage Die on Sneak Attacks."
        ]
    , mtype = "Combat knife"
    , name = "Stealth Blade"
    , prefix = "Stealth"
    , requirements = [ (Perks.Perks.blacksmith 2) ]
    , weight = +0
    }

let lead-pipe-heavy : Mod =
    { complexity = 3
    , cost = +11
    , effects =
        [ DEQ.DamageChangeFactory +2
        ]
    , mtype = "Lead Pipe"
    , name = "Heavy"
    , prefix = "Heavy"
    , requirements = [ (Perks.Perks.blacksmith 2) ]
    , weight = +2
    }

let lead-pipe-spiked : Mod =
    { complexity = 1
    , cost = +4
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Lead Pipe"
    , name = "Spiked"
    , prefix = "Spiked"
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let machete-serrated-blade : Mod =
    { complexity = 3
    , cost = +12
    , effects =
        [ DEQ.DamageChangeFactory +2
        ]
    , mtype = "Machete"
    , name = "Serrated Blade"
    , prefix = "Serrated"
    , requirements = [ (Perks.Perks.blacksmith 2) ]
    , weight = +0
    }

let pipe-wrench-extra-heavy : Mod =
    { complexity = 3
    , cost = +22
    , effects =
        [ DEQ.DamageChangeFactory +3
        ]
    , mtype = "Pipe Wrench"
    , name = "Extra Heavy"
    , prefix = "Heavy"
    , requirements = [ (Perks.Perks.blacksmith 2) ]
    , weight = +2 -- TODO: Check for errata???
    }

let pipe-wrench-heavy : Mod =
    { complexity = 2
    , cost = +12
    , effects =
        [ DEQ.DamageChangeFactory +2
        ]
    , mtype = "Pipe Wrench"
    , name = "Heavy"
    , prefix = "Weighted"
    , requirements = [ (Perks.Perks.blacksmith 1) ]
    , weight = +7 -- TODO: Check for errata???
    }

let pipe-wrench-hooked : Mod =
    { complexity = 1
    , cost = +9
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.GenericDEQFactory "Disarm Enemy on Hit" "On successful attack, spend 2 AP to disarm opponent, knocking one held weapon away."
        ]
    , mtype = "Pipe Wrench"
    , name = "Hooked"
    , prefix = "Hooked"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let pipe-wrench-puncturing : Mod =
    { complexity = 2
    , cost = +13
    , effects =
        [ DEQ.DamageChangeFactory +2
        , DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Pipe Wrench"
    , name = "Puncturing"
    , prefix = "Puncturing"
    , requirements = [ (Perks.Perks.blacksmith 1) ]
    , weight = +1
    }

let pool-cue-barbed : Mod =
    { complexity = 1
    , cost = +2
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Pool Cue"
    , name = "Barbed"
    , prefix = "Barbed"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let pool-cue-sharp : Mod =
    { complexity = 1
    , cost = +3
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.physical ) True
        ]
    , mtype = "Pool Cue"
    , name = "Sharp"
    , prefix = "Sharp"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let ripper-curved-blade : Mod =
    { complexity = 2
    , cost = +15
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.GenericDEQFactory "Disarm Enemy on Hit" "On successful attack, spend 2 AP to disarm opponent, knocking one held weapon away."
        ]
    , mtype = "Ripper"
    , name = "Curved Blade"
    , prefix = "Curved"
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let ripper-extended-blade : Mod =
    { complexity = 5
    , cost = +25
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.physical ) True
        ]
    , mtype = "Ripper"
    , name = "Extended Blade"
    , prefix = "Extended"
    , requirements = [ (Perks.Perks.blacksmith 3) ]
    , weight = +3
    }

let rolling-pin-sharp : Mod =
    { complexity = 1
    , cost = +3
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.physical ) True
        ]
    , mtype = "Rolling Pin"
    , name = "Sharp"
    , prefix = "Sharp"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let rolling-pin-spiked : Mod =
    { complexity = 1
    , cost = +3
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Rolling Pin"
    , name = "Spiked"
    , prefix = "Barbed"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let shishkebab-extra-flame-jets : Mod =
    { complexity = 5
    , cost = +100
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.energy ) True
        ]
    , mtype = "Shishkebab"
    , name = "Extra Flame Jets"
    , prefix = "Searing"
    , requirements = [ (Perks.Perks.blacksmith 3) ]
    , weight = +1
    }

let sledgehammer-heavy : Mod =
    { complexity = 3
    , cost = +30
    , effects =
        [ DEQ.DamageChangeFactory +2
        ]
    , mtype = "Sledgehammer"
    , name = "Heavy"
    , prefix = "Heavy"
    , requirements = [ (Perks.Perks.blacksmith 2) ]
    , weight = +9
    }

let sledgehammer-puncturing : Mod =
    { complexity = 3
    , cost = +18
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Sledgehammer"
    , name = "Puncturing"
    , prefix = "Puncturing"
    , requirements = [ (Perks.Perks.blacksmith 2) ]
    , weight = +5
    }

let super-sledge-heating-coil : Mod =
    { complexity = 3
    , cost = +180
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.DamageTypeChangeFactory DEQ.DamageTypes.energy
        ]
    , mtype = "Super Sledgehammer"
    , name = "Heating Coil"
    , prefix = "Heated"
    , requirements = [ (Perks.Perks.blacksmith 2) ]
    , weight = +0
    }

let super-sledge-stun-pack : Mod =
    { complexity = 5
    , cost = +360
    , effects =
        [ DEQ.DamageChangeFactory +2
        , DEQ.DamageTypeChangeFactory DEQ.DamageTypes.energy
        , DEQ.ToggleDEQ DEQ.Effects.stun True
        ]
    , mtype = "Super Sledgehammer"
    , name = "Stun Pack"
    , prefix = "Stunning"
    , requirements = [ (Perks.Perks.blacksmith 3), (Perks.Perks.science 1) ]
    , weight = +0
    }

let switchblade-serrated-blade : Mod =
    { complexity = 3
    , cost = +10
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.physical ) True
        ]
    , mtype = "Switchblade"
    , name = "Serrated Blade"
    , prefix = "Serrated"
    , requirements = [ (Perks.Perks.blacksmith 1) ]
    , weight = +0
    }

let sword-electrified-blade : Mod =
    { complexity = 3
    , cost = +25
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.DamageTypeChangeFactory DEQ.DamageTypes.energy
        ]
    , mtype = "Sword"
    , name = "Electrified Blade"
    , prefix = "Electrified"
    , requirements = [ (Perks.Perks.blacksmith 2), (Perks.Perks.science 1) ]
    , weight = +0
    }

let sword-electrified-serrated-blade : Mod =
    { complexity = 5
    , cost = +75
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.DamageTypeChangeFactory DEQ.DamageTypes.energy
        , DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.physical ) True
        ]
    , mtype = "Sword"
    , name = "Electrified Serrated Blade"
    , prefix = "Electrified Serrated"
    , requirements = [ (Perks.Perks.blacksmith 3), (Perks.Perks.science 1) ]
    , weight = +0
    }

let sword-serrated-blade : Mod =
    { complexity = 4
    , cost = +25
    , effects =
        [ DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.physical ) True
        ]
    , mtype = "Sword"
    , name = "Serrated Blade"
    , prefix = "Serrated"
    , requirements = [ (Perks.Perks.blacksmith 2) ]
    , weight = +0
    }

let sword-stun-pack : Mod =
    { complexity = 5
    , cost = +100
    , effects =
        [ DEQ.DamageChangeFactory +2
        , DEQ.DamageTypeChangeFactory DEQ.DamageTypes.energy
        , DEQ.ToggleDEQ DEQ.Effects.stun True
        ]
    , mtype = "Sword"
    , name = "Stun Pack"
    , prefix = "Stunning"
    , requirements = [ (Perks.Perks.blacksmith 3), (Perks.Perks.science 1) ]
    , weight = +0
    }

let tire-iron-bladed : Mod =
    { complexity = 3
    , cost = +12
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.physical ) True
        ]
    , mtype = "Tire Iron"
    , name = "Bladed"
    , prefix = "Bladed"
    , requirements = [ (Perks.Perks.blacksmith 2) ]
    , weight = +1
    }

let walking-cane-barbed : Mod =
    { complexity = 1
    , cost = +3
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Walking Cane"
    , name = "Barbed"
    , prefix = "Barbed"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let walking-cane-spiked : Mod = -- TODO: Check for errata. This is IDENTICAL to the barbed walking cane mod.
    { complexity = 1
    , cost = +3
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Walking Cane"
    , name = "Spiked"
    , prefix = "Spiked"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let boxing-glove-lead-lining : Mod =
    { complexity = 2
    , cost = +7
    , effects =
        [ DEQ.DamageChangeFactory +2
        ]
    , mtype = "Boxing Glove"
    , name = "Lead lining"
    , prefix = "Lead-lined"
    , requirements = [ (Perks.Perks.blacksmith 1) ]
    , weight = +1
    }

let boxing-glove-puncturing : Mod =
    { complexity = 2
    , cost = +4
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Boxing Glove"
    , name = "Puncturing"
    , prefix = "Puncturing"
    , requirements = [ (Perks.Perks.blacksmith 1) ]
    , weight = +0
    }

let boxing-glove-spiked : Mod =
    { complexity = 1
    , cost = +3
    , effects =
        [ DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Boxing Glove"
    , name = "Spiked"
    , prefix = "Spiked"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let deathclaw-gauntlet-extra-claw : Mod =
    { complexity = 1
    , cost = +22
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.GenericDEQFactory "Disarm Enemy on Hit" "On successful attack, spend 2 AP to disarm opponent, knocking one held weapon away."
        ]
    , mtype = "Deathclaw Gauntlet"
    , name = "Extra Claw"
    , prefix = "Large"
    , requirements = [] : List Perks.Perk
    , weight = +2
    }

let knuckles-bladed : Mod =
    { complexity = 2
    , cost = +5
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.physical ) True
        ]
    , mtype = "Knuckles"
    , name = "Bladed"
    , prefix = "Bladed"
    , requirements = [ (Perks.Perks.blacksmith 1) ]
    , weight = +0
    }

let knuckles-puncturing : Mod =
    { complexity = 2
    , cost = +4
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Knuckles"
    , name = "Puncturing"
    , prefix = "Puncturing"
    , requirements = [ (Perks.Perks.blacksmith 1) ]
    , weight = +0
    }

let knuckles-sharp : Mod =
    { complexity = 1
    , cost = +3
    , effects =
        [ DEQ.ToggleDEQ ( DEQ.Effects.persistent DEQ.DamageTypes.physical ) True
        ]
    , mtype = "Knuckles"
    , name = "Sharp"
    , prefix = "Sharp"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let knuckles-spiked : Mod =
    { complexity = 1
    , cost = +3
    , effects =
        [ DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Knuckles"
    , name = "Spiked"
    , prefix = "Spiked"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let power-fist-heating-coil : Mod =
    { complexity = 4
    , cost = +100
    , effects =
        [ DEQ.DamageChangeFactory +2
        , DEQ.DamageTypeChangeFactory DEQ.DamageTypes.energy
        ]
    , mtype = "Power Fist"
    , name = "Heating Coil"
    , prefix = "Heated"
    , requirements = [ (Perks.Perks.blacksmith 3) ]
    , weight = +0
    }

let power-fist-puncturing : Mod =
    { complexity = 3
    , cost = +45
    , effects =
        [ DEQ.DamageChangeFactory +2
        , DEQ.ToggleDEQ ( DEQ.Effects.piercing 1 ) True
        ]
    , mtype = "Power Fist"
    , name = "Puncturing"
    , prefix = "Puncturing"
    , requirements = [ (Perks.Perks.blacksmith 2) ]
    , weight = +1
    }

-- Small Guns

let s-barrel-bull : Mod =
    { complexity = 5
    , cost = +10
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.reliable True
        ]
    , mtype = "Barrel"
    , name = "Bull Barrel"
    , prefix = "Bull Barrel"
    , requirements = [ (Perks.Perks.gun-nut 3) ]
    , weight = +0
    }

let s-barrel-finned : Mod =
    { complexity = 4
    , cost = +15
    , effects =
        [ DEQ.DamageChangeFactory +1
        , DEQ.RangeChangeFactory +1
        ]
    , mtype = "Barrel"
    , name = "Finned"
    , prefix = "Finned"
    , requirements = [ (Perks.Perks.gun-nut 2) ]
    , weight = +2
    }

let s-barrel-long : Mod =
    { complexity = 5
    , cost = +20
    , effects =
        [ DEQ.RangeChangeFactory +1
        ]
    , mtype = "Barrel"
    , name = "Long"
    , prefix = "Long"
    , requirements = [ (Perks.Perks.gun-nut 1) ]
    , weight = +1
    }

let s-barrel-ported : Mod =
    { complexity = 6
    , cost = +35
    , effects =
        [ DEQ.FireRateChangeFactory +1
        , DEQ.RangeChangeFactory +1
        ]
    , mtype = "Barrel"
    , name = "Ported"
    , prefix = "Ported"
    , requirements = [ (Perks.Perks.gun-nut 4) ]
    , weight = +1
    }

let s-barrel-sawed-off : Mod =
    { complexity = 2
    , cost = +3
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.close-quarters True
        , DEQ.ToggleDEQ DEQ.Qualities.two-handed False
        ]
    , mtype = "Barrel"
    , name = "Sawed-Off"
    , prefix = "Sawed Off"
    , requirements = [] : List Perks.Perk
    , weight = -2
    }

let s-barrel-snubnose : Mod =
    { complexity = 2
    , cost = +0
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.inaccurate True
        ]
    , mtype = "Barrel"
    , name = "Snubnose"
    , prefix = "Snub-noses"
    , requirements = [] : List Perks.Perk
    , weight = -1
    }

let s-barrel-vented : Mod =
    { complexity = 6
    , cost = +36
    , effects =
        [ DEQ.FireRateChangeFactory +1
        , DEQ.RangeChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Qualities.reliable True
        ]
    , mtype = "Barrel"
    , name = "Vented"
    , prefix = "Vented"
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let s-grip-comfort : Mod =
    { complexity = 2
    , cost = +6
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Grip"
    , name = "Comfort Grip"
    , prefix = "Comfort"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let s-grip-sharpshooters : Mod =
    { complexity = 3
    , cost = +10
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        , DEQ.ToggleDEQ (DEQ.Effects.piercing 1) True
        ]
    , mtype = "Grip"
    , name = "Sharpshooter's Grip"
    , prefix = "Sharpshooter's"
    , requirements = [ (Perks.Perks.gun-nut 1) ]
    , weight = +0
    }

let s-muzzle-bayonet : Mod =
    { complexity = 2
    , cost = +10
    , effects =
        [ DEQ.GenericDEQFactory "Melee weapon" "Your Gun Bash attack is modified according to any additional mod effects."
        , DEQ.GenericDEQFactory "Deals 4 CD Piercing 1 physical damage" "Your Gun Bash attack is modified to deal 4 combat dice of damage, inflicting Piercing 1 as an Effect."
        ]
    , mtype = "Muzzle"
    , name = "Bayonet"
    , prefix = "Bayoneted"
    , requirements = [] : List Perks.Perk
    , weight = +2
    }

let s-muzzle-brake : Mod =
    { complexity = 3
    , cost = +30
    , effects =
        [ DEQ.FireRateChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Muzzle"
    , name = "Muzzle Break"
    , prefix = "Muzzled"
    , requirements = [ (Perks.Perks.gun-nut 1) ]
    , weight = +1
    }

let s-muzzle-compensator : Mod =
    { complexity = 3
    , cost = +15
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Muzzle"
    , name = "Compensator"
    , prefix = "Compensated"
    , requirements = [ (Perks.Perks.gun-nut 1) ]
    , weight = +1
    }

let s-muzzle-suppressor : Mod =
    { complexity = 4
    , cost = +45
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.suppressed True
        ]
    , mtype = "Muzzle"
    , name = "Suppressor"
    , prefix = "Suppressed"
    , requirements = [ (Perks.Perks.gun-nut 2) ]
    , weight = +2
    }

let s-receiver-308cal : Mod =
    { complexity = 6
    , cost = +40
    , effects =
        [ DEQ.AmmoChangeFactory ".308"
        , DEQ.DamageBaseChangeFactory 7
        ]
    , mtype = "Receiver"
    , name = ".308 Receiver"
    , prefix = ".308"
    , requirements = [ (Perks.Perks.gun-nut 4) ]
    , weight = +4
    }

let s-receiver-38cal : Mod =
    { complexity = 6
    , cost = +20
    , effects =
        [ DEQ.AmmoChangeFactory ".38"
        , DEQ.DamageBaseChangeFactory 4
        ]
    , mtype = "Receiver"
    , name = ".38 Receiver"
    , prefix = ".38"
    , requirements = [ (Perks.Perks.gun-nut 4) ]
    , weight = +3
    }

let s-receiver-45cal : Mod =
    { complexity = 4
    , cost = +19
    , effects =
        [ DEQ.AmmoChangeFactory ".45"
        , DEQ.DamageBaseChangeFactory 4
        , DEQ.FireRateChangeFactory +1
        ]
    , mtype = "Receiver"
    , name = ".45 Receiver"
    , prefix = ".45"
    , requirements = [ (Perks.Perks.gun-nut 2) ]
    , weight = +2
    }

let s-receiver-50cal : Mod =
    { complexity = 5
    , cost = +30
    , effects =
        [ DEQ.AmmoChangeFactory ".50"
        , DEQ.DamageBaseChangeFactory 8
        , DEQ.ToggleDEQ DEQ.Effects.vicious True
        ]
    , mtype = "Receiver"
    , name = ".50 Receiver"
    , prefix = ".50"
    , requirements = [ (Perks.Perks.gun-nut 4) ]
    , weight = +4
    }

let s-receiver-advanced : Mod =
    { complexity = 5
    , cost = +35
    , effects =
        [ DEQ.DamageChangeFactory +3
        , DEQ.FireRateChangeFactory +1
        ]
    , mtype = "Receiver"
    , name = "Advanced"
    , prefix = "Advanced"
    , requirements = [ (Perks.Perks.gun-nut 2) ]
    , weight = +2
    }

let s-receiver-automatic : Mod =
    { complexity = 3
    , cost = +30
    , effects =
        [ DEQ.DamageChangeFactory -1
        , DEQ.FireRateChangeFactory +2
        , DEQ.ToggleDEQ DEQ.Effects.burst True
        , DEQ.ToggleDEQ DEQ.Qualities.inaccurate True
        ]
    , mtype = "Receiver"
    , name = "Automatic"
    , prefix = "Auto"
    , requirements = [ (Perks.Perks.gun-nut 1) ]
    , weight = +1
    }

let s-receiver-automatic-piston : Mod =
    { complexity = 4
    , cost = +75
    , effects =
        [ DEQ.FireRateChangeFactory +2
        , DEQ.RangeChangeFactory +1
        ]
    , mtype = "Receiver"
    , name = "Automatic Piston"
    , prefix = "Automatic"
    , requirements = [ (Perks.Perks.gun-nut 2) ]
    , weight = +2
    }

let s-receiver-calibrated : Mod =
    { complexity = 2
    , cost = +25
    , effects =
        [ DEQ.ToggleDEQ DEQ.Effects.vicious True
        ]
    , mtype = "Receiver"
    , name = "Calibrated"
    , prefix = "Calibrated"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let s-receiver-hair-trigger : Mod =
    { complexity = 4
    , cost = +20
    , effects =
        [ DEQ.FireRateChangeFactory +1
        ]
    , mtype = "Receiver"
    , name = "Hair Trigger"
    , prefix = "Hair Trigger"
    , requirements = [ (Perks.Perks.gun-nut 2) ]
    , weight = +0
    }

let s-receiver-hardened : Mod =
    { complexity = 2
    , cost = +20
    , effects =
        [ DEQ.DamageChangeFactory +1
        ]
    , mtype = "Receiver"
    , name = "Hardened"
    , prefix = "Hardened"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let s-receiver-powerful : Mod =
    { complexity = 3
    , cost = +25
    , effects =
        [ DEQ.DamageChangeFactory +2
        ]
    , mtype = "Receiver"
    , name = "Powerful"
    , prefix = "Powerful"
    , requirements = [ (Perks.Perks.gun-nut 1) ]
    , weight = +1
    }

let s-sight-long : Mod =
    { complexity = 4
    , cost = +29
    , effects =
        [ DEQ.RangeChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Qualities.accurate True
        ]
    , mtype = "Sight"
    , name = "Long Scope"
    , prefix = "Scoped"
    , requirements = [ (Perks.Perks.science 2) ]
    , weight = +1
    }

let s-sight-long-nv : Mod =
    { complexity = 5
    , cost = +50
    , effects =
        [ DEQ.RangeChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Qualities.accurate True
        , DEQ.ToggleDEQ DEQ.Qualities.night-vision True
        ]
    , mtype = "Sight"
    , name = "Long Night Vision Scope"
    , prefix = "Night Vision"
    , requirements = [ (Perks.Perks.science 3) ]
    , weight = +1
    }

let s-sight-recon : Mod =
    { complexity = 5
    , cost = +59
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.accurate True
        , DEQ.ToggleDEQ DEQ.Qualities.recon True
        ]
    , mtype = "Sight"
    , name = "Recon Scope"
    , prefix = "Recon"
    , requirements = [ (Perks.Perks.science 3) ]
    , weight = +1
    }

let s-sight-reflex : Mod =
    { complexity = 2
    , cost = +14
    , effects =
        [ DEQ.GenericDEQFactory "May re-roll hit location die" "When rolling the d20 to determine hit location, you may re-roll it once."
        ]
    , mtype = "Sight"
    , name = "Reflex Sight"
    , prefix = "Tactical"
    , requirements = [] : List Perks.Perk
    , weight = +0
    }

let s-sight-short : Mod =
    { complexity = 2
    , cost = +11
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.accurate True
        ]
    , mtype = "Sight"
    , name = "Short Scope"
    , prefix = "Scoped"
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let s-sight-short-nv : Mod =
    { complexity = 4
    , cost = +38
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.accurate True
        , DEQ.ToggleDEQ DEQ.Qualities.night-vision True
        ]
    , mtype = "Sight"
    , name = "Short Night Vision Scope"
    , prefix = "Night Vision"
    , requirements = [ (Perks.Perks.science 2) ]
    , weight = +1
    }

let s-stock-full : Mod =
    { complexity = 2
    , cost = +10
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.two-handed True
        , DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Stock"
    , name = "Full Stock"
    , prefix = ""
    , requirements = [] : List Perks.Perk
    , weight = +1
    }

let s-stock-marksmans : Mod =
    { complexity = 4
    , cost = +20
    , effects =
        [ DEQ.ToggleDEQ DEQ.Qualities.accurate True
        , DEQ.ToggleDEQ DEQ.Qualities.two-handed True
        , DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Stock"
    , name = "Marksman's Stock"
    , prefix = "Marksman's"
    , requirements = [ (Perks.Perks.gun-nut 2) ]
    , weight = +2
    }

let s-stock-recoil-compensating : Mod =
    { complexity = 5
    , cost = +3 -- TODO: Check for errata? This is just cheaper than a full stock for better effects...
    , effects =
        [ DEQ.FireRateChangeFactory +1
        , DEQ.ToggleDEQ DEQ.Qualities.two-handed True
        , DEQ.ToggleDEQ DEQ.Qualities.inaccurate False
        ]
    , mtype = "Stock"
    , name = "Recoil Compensating Stock"
    , prefix = "Recoil Compensated"
    , requirements = [ (Perks.Perks.gun-nut 3) ]
    , weight = +2
    }

-- Output --

in  { Mod
    , ProcessedMod
    , ProcessMod
    , ProcessMods
    , ModFactory
    , Mods =
        {
        -- Big Guns
        , flamer-long-barrel
        , flamer-napalm
        , flamer-compression-nozzle
        , flamer-vaporization-nozzle
        , flamer-large-tank
        , flamer-huge-tank
        , gatling-laser-charging-barrels
        , gatling-laser-beta-wave-tuner
        , gatling-laser-boosted-capacitor
        , gatling-laser-photon-agitator
        , gatling-laser-photon-exciter
        , gatling-laser-beam-focuser
        , gatling-laser-reflex-sight
        , junk-jet-long-barrel
        , junk-jet-electrification-module
        , junk-jet-ignition-module
        , junk-jet-gunner-sight
        , junk-jet-recoil-compensating-stock
        , minigun-accelerated-barrel
        , minigun-tri-barrel
        , minigun-shredder
        , minigun-gunner-sight
        , missile-launcher-triple-barrel
        , missile-launcher-quad-barrel
        , missile-launcher-bayonet
        , missile-launcher-stabilizer
        , missile-launcher-night-vision-scope
        , missile-launcher-scope
        , missile-launcher-targeting-computer
        -- Energy Weapons
        , e-automatic-barrel
        , e-beam-focuser
        , e-beam-splitter
        , e-beta-wave-tuner
        , e-boosted-capacitor
        , e-bracketed-long-barrel
        , e-bracketed-short-barrel
        , e-flamer-barrel
        , e-full-stock
        , e-gyro-compensating-lens
        , e-improved-barrel
        , e-long-barrel
        , e-long-night-vision-scope
        , e-long-scope
        , e-marksmans-stock
        , e-photon-agitator
        , e-photon-exciter
        , e-recoil-compensating-stock
        , e-recon-scope
        , e-reflex-sight
        , e-sharpshooters-grip
        , e-short-night-vision-scope
        , e-short-scope
        , e-sniper-barrel
        , e-splitter
        , e-standard-stock
        , gamma-gun-deep-dish
        , gamma-gun-electric-signal-carrier-antennae
        , gamma-gun-signal-repeater
        , laser-musket-five-crank-capacitor
        , laser-musket-four-crank-capacitor
        , laser-musket-six-crank-capacitor
        , laser-musket-three-crank-capacitor
        -- Melee Weapons
        , baseball-bat-barbed
        , baseball-bat-bladed
        , baseball-bat-chain-wrapped
        , baseball-bat-sharp
        , baseball-bat-spiked
        , baton-electrified
        , baton-stun-pack
        , board-bladed
        , board-puncturing
        , board-spiked
        , combat-knife-serrated-blade
        , combat-knife-stealth-blade
        , lead-pipe-heavy
        , lead-pipe-spiked
        , machete-serrated-blade
        , pipe-wrench-extra-heavy
        , pipe-wrench-heavy
        , pipe-wrench-hooked
        , pipe-wrench-puncturing
        , pool-cue-barbed
        , pool-cue-sharp
        , ripper-curved-blade
        , ripper-extended-blade
        , rolling-pin-sharp
        , rolling-pin-spiked
        , shishkebab-extra-flame-jets
        , sledgehammer-heavy
        , sledgehammer-puncturing
        , super-sledge-heating-coil
        , super-sledge-stun-pack
        , switchblade-serrated-blade
        , sword-electrified-blade
        , sword-electrified-serrated-blade
        , sword-serrated-blade
        , sword-stun-pack
        , tire-iron-bladed
        , walking-cane-barbed
        , walking-cane-spiked
        , boxing-glove-lead-lining
        , boxing-glove-puncturing
        , boxing-glove-spiked
        , deathclaw-gauntlet-extra-claw
        , knuckles-bladed
        , knuckles-puncturing
        , knuckles-sharp
        , knuckles-spiked
        , power-fist-heating-coil
        , power-fist-puncturing
        -- Small Guns
        , s-barrel-bull
        , s-barrel-finned
        , s-barrel-long
        , s-barrel-ported
        , s-barrel-sawed-off
        , s-barrel-snubnose
        , s-barrel-vented
        , s-grip-comfort
        , s-grip-sharpshooters
        , s-muzzle-bayonet
        , s-muzzle-brake
        , s-muzzle-compensator
        , s-muzzle-suppressor
        , s-receiver-308cal
        , s-receiver-38cal
        , s-receiver-45cal
        , s-receiver-50cal
        , s-receiver-advanced
        , s-receiver-automatic
        , s-receiver-automatic-piston
        , s-receiver-calibrated
        , s-receiver-hair-trigger
        , s-receiver-hardened
        , s-receiver-powerful
        , s-sight-long
        , s-sight-long-nv
        , s-sight-recon
        , s-sight-reflex
        , s-sight-short
        , s-sight-short-nv
        , s-stock-full
        , s-stock-marksmans
        , s-stock-recoil-compensating
        }
    }
