-- src/package.dhall

{-|
TODO: define and fill in armor
    * power armor
    * armor
    * clothing
    * headgear
    * outfits
    * robot armor
TODO: define and fill in consumables
TODO: define and fill in items
TODO: define and fill in crafting info
TODO: define and fill in bestiary?
TODO: define and fill in kits
TODO: define and fill in origins
TODO: find small gun magazine mods list
|-}

{ Chems = ./chems.dhall
, DEQ = ./genericDEQ.dhall
, Mods = ./mods.dhall
, Perks = ./perks.dhall
, Prelude = ./prelude.dhall
, Refreshments = ./refreshments.dhall
, Utils = ./utils.dhall
, Weapons = ./weapons.dhall
}
