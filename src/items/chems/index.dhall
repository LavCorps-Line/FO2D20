-- src/items/chems/index.dhall

let data = ./src/chems.dhall
let utils = ./src/utils.dhall
let prelude = ./src/prelude.dhall

let mappedChemRecord : Type =
    { mapKey : Text
    , mapValue : data.Chem
    }

let processChem = \(input : mappedChemRecord) ->
    (data.ChemFactory input.mapValue).entryLinked

let processChemsList = \(input : List mappedChemRecord) ->
    let out1 = utils.Map mappedChemRecord Text processChem input
    let output = prelude.Text.concatSep "\n" out1
    in output

let listChem = \(input : mappedChemRecord) ->
    (data.ChemFactory input.mapValue).description

let listChemList = \(input : List mappedChemRecord) ->
    let out1 = utils.Map mappedChemRecord Text listChem input
    let output = prelude.Text.concatSep "\n---\n" out1
    in output

let Document =
''
| Chem | Duration | Addictive | Weight | Cost | Rarity |
|------|----------|-----------|--------|------|--------|
${processChemsList (toMap data.Chems)}

${listChemList (toMap data.Chems)}
''

in Document