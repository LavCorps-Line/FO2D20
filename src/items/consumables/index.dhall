-- src/items/consumables/index.dhall

let data = ./src/consumables.dhall
let utils = ./src/utils.dhall
let prelude = ./src/prelude.dhall

let mappedConsumableRecord : Type =
    { mapKey : Text
    , mapValue : data.Consumable
    }

let processConsumable = \(input : mappedConsumableRecord) ->
    (data.ConsumableFactory input.mapValue).entryLinked

let processConsumablesList = \(input : List mappedConsumableRecord) ->
    let out1 = utils.Map mappedConsumableRecord Text processConsumable input
    let output = prelude.Text.concatSep "\n" out1
    in output

let listConsumable = \(input : mappedConsumableRecord) ->
    (data.ConsumableFactory input.mapValue).description

let listConsumableList = \(input : List mappedConsumableRecord) ->
    let out1 = utils.Map mappedConsumableRecord Text listConsumable input
    let output = prelude.Text.concatSep "\n---\n" out1
    in output

let Document =
''
| Consumable | Effects | Weight | Cost | Rarity |
|------------|---------|--------|------|--------|
${processConsumablesList (toMap data.ConsumablesMisc)}

${listConsumableList (toMap data.ConsumablesMisc)}
''

in Document
