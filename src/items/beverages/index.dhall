-- src/items/food/index.dhall

let data = ./src/refreshments.dhall
let utils = ./src/utils.dhall
let prelude = ./src/prelude.dhall

let mappedRefreshmentRecord : Type =
    { mapKey : Text
    , mapValue : data.Refreshment
    }

let processRefreshment = \(input : mappedRefreshmentRecord) ->
    (data.RefreshmentFactory input.mapValue).entryLinked

let processRefreshmentsList = \(input : List mappedRefreshmentRecord) ->
    let out1 = utils.Map mappedRefreshmentRecord Text processRefreshment input
    let output = prelude.Text.concatSep "\n" out1
    in output

let listRefreshment = \(input : mappedRefreshmentRecord) ->
    (data.RefreshmentFactory input.mapValue).description

let listRefreshmentList = \(input : List mappedRefreshmentRecord) ->
    let out1 = utils.Map mappedRefreshmentRecord Text listRefreshment input
    let output = prelude.Text.concatSep "\n---\n" out1
    in output

let Document =
''
| Beverage | HP Healed | Effects | Irradiated | Weight | Cost | Rarity |
|----------|-----------|---------|------------|--------|------|--------|
${processRefreshmentsList (toMap data.Beverages)}

${listRefreshmentList (toMap data.Beverages)}
''

in Document
