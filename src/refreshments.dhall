-- src/refreshments.dhall

-- Imports --

let Prelude = ./prelude.dhall
let DEQ = ./genericDEQ.dhall
let Utils = ./utils.dhall

-- Type Defs --

let Refreshment : Type =
    { cost : Natural
    , description : Text
    , effects : List DEQ.GenericDEQ
    , irradiated : Natural
    , name : Text
    , rarity : Natural
    , restores : Natural
    , weight : Natural
    }

let ProcessedRefreshment : Type =
    { cost : Text
    , description : Text
    , effects : Text
    , entryGeneric : Text
    , entryLinked : Text
    , irradiated : Text
    , name : Text
    , rarity : Text
    , restores : Text
    , slug : Text
    , weight : Text
    }

-- Variables --

let Fresh =
    ''
    When grown in non-irradiated conditions, such as within vaults, this is identical to
    the listed version, but is not irradiated, and has +1 rarity.
    ''

let Preserved =
    ''
    Sometimes, preserved boxes of this item can be found. These are identical to the
    listed version, but are not irradiated, and increase their rarity by +1.
    ''

-- Type Factories --

let RadlessFactory : Refreshment -> Bool -> Refreshment = \(input : Refreshment) -> \(isPreserved : Bool) ->
    { cost = input.cost
    , description = input.description
    , effects = input.effects
    , irradiated = 0
    , name = "${input.name} ${if isPreserved then "(Preserved)" else "(Fresh)"}"
    , rarity = input.rarity + 1
    , restores = input.restores
    , weight = input.weight
    }

let RefreshmentFactory : forall(rfr : Refreshment) -> ProcessedRefreshment = \(rfr : Refreshment) ->
    let cost = Utils.Tooltipify (Utils.ParseValue rfr.cost) "in caps"
    let effects = DEQ.ProcessDEQList rfr.effects
    let irradiated = if (Prelude.Natural.equal rfr.irradiated 0) then (Utils.Mono "No") else (Utils.Mono "Yes ${Natural/show rfr.irradiated}")
    let name = rfr.name
    let rarity = Utils.Mono (Utils.ParseValue rfr.rarity)
    let restores = Utils.Mono (Utils.ParseValue rfr.restores)
    let slug = Utils.Sluginator name
    let weight = Utils.Tooltipify (Utils.ConvertWeight rfr.weight) "in pounds (lbs)"

    let entryGeneric = Prelude.Text.concat
    [ "| "
    , "${name} | "
    , "${restores} | "
    , "${effects} | "
    , "${irradiated} | "
    , "${weight} | "
    , "${cost} | "
    , "${rarity} |"
    ]

    let entryLinked = Prelude.Text.concat
    [ "| "
    , "[${name}](#${slug}) | "
    , "${restores} | "
    , "${effects} | "
    , "${irradiated} | "
    , "${weight} | "
    , "${cost} | "
    , "${rarity} |"
    ]

    let attributes = Prelude.Text.concat
    [ "* HP Healed: ${restores}\n"
    , if Prelude.Natural.equal (List/length DEQ.GenericDEQ rfr.effects) 0 then "" else "* Effects: ${effects}\n"
    , "* Irradiated: ${irradiated}\n"
    , "* Weight: ${weight}\n"
    , "* Cost: ${cost}\n"
    , "* Rarity: ${rarity}\n"
    ]

    let description = Prelude.Text.concat
    [ "## ${name}\n\n"
    , "${rfr.description}\n"
    , "${attributes}\n"
    ]

    in
    { cost = cost
    , description = description
    , effects = effects
    , entryGeneric = entryGeneric
    , entryLinked = entryLinked
    , irradiated = irradiated
    , name = name
    , rarity = rarity
    , restores = restores
    , slug = slug
    , weight = weight
    }

-- Consumables Defs --

-- Beverages

let beer : Refreshment =
    { cost = 5
    , description =
        ''
        One of the oldest beverages created by humans, beer is a carbonated alcoholic
        drink made from fermented cereal grains, popular pre-War to relax after a hard
        day's work or during a sunny afternoon. Ale, beer, lager, stout, and a variety of
        similar beverages all have distinct combinations of ingredients, and each have
        their own proponents, but the effects are largely the same.

        You receive 1 cap upon opening and drinking a bottle of beer.
        ''
    , effects =
        [DEQ.Effects.alcoholic] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Beer"
    , rarity = 1
    , restores = 0
    , weight = 1
    }

let blood-pack : Refreshment =
    { cost = 10
    , description =
        ''
        A sealed, sterile plastic bag of preserved blood used pre-War for blood
        transfusions. Not the most conventional of beverages, but useful if you're
        desperate. Some people come to like the taste.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Blood Pack"
    , rarity = 2
    , restores = 3
    , weight = 0
    }

let blood-pack-glowing : Refreshment =
    { cost = 30
    , description =
        ''
        A blood bag filled with a luminescent green fluid. If you can stomach the acrid,
        metallic taste, they have potent restorative properties and help protect the body
        against radiation poisoning for a while.
        ''
    , effects =
        [DEQ.ChemDefenseBuffFactory 5 DEQ.DamageTypes.radiation] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Glowing Blood Pack"
    , rarity = 3
    , restores = 4
    , weight = 0
    }

let blood-pack-irradiated : Refreshment =
    { cost = 50
    , description =
        ''
        A blood bag filled with discolored--often green--blood-like fluid. About as
        useful as a normal blood pack, but highly irradiated.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 2
    , name = "Irradiated Blood"
    , rarity = 2
    , restores = 3
    , weight = 0
    }

let bourbon : Refreshment =
    { cost = 7
    , description =
        ''
        A distinctly American type of whiskey, bourbon is a barrel-aged, distilled spirit
        made mainly of corn. Pre-War, those who favored it tended to get a reputation as
        being tough or rugged.
        ''
    , effects =
        [DEQ.Effects.alcoholic, DEQ.ChemSkillRerollFactory 1 ["END"]] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Bourbon"
    , rarity = 2
    , restores = 0
    , weight = 1
    }

let brahmin-milk : Refreshment =
    { cost = 15
    , description =
        ''
        Milk from a brahmin. This drink is a true lifesaver, as it has properties that
        cleanse radiation poisoning from the body... at least, if it hasn't spoiled yet.
        ''
    , effects =
        [DEQ.GenericDEQFactory "Heal 2 Radiation Damage" "Heals 2 Radiation Damage upon Consumption, cannot be used during First Aid."] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Brahmin Milk"
    , rarity = 2
    , restores = 1
    , weight = 0
    }

let dirty-wastelander : Refreshment =
    { cost = 10
    , description =
        ''
        An extremely potent alcoholic beverage, made from a blend of whiskey, Nuka-Cola,
        and Mutfruit, which has much the same effect as most alcoholic drinks... just
        moreso.
        ''
    , effects =
        [DEQ.Effects.alcoholic, DEQ.ChemSkillChallengeFactory False 1 ["STR"], DEQ.ChemSkillChallengeFactory True 2 ["INT"]] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Dirty Wastelander"
    , rarity = 3
    , restores = 0
    , weight = 1
    }

let melon-juice : Refreshment =
    { cost = 5
    , description =
        ''
        Juice made from pressing the pulp of a melon. A refreshing and healthy drink,
        which promotes the body's natural healing.
        ''
    , effects =
        [DEQ.GenericDEQFactory "Instant Regenerating HP" "Regain 1 HP at the start of each turn."] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Melon Juice"
    , rarity = 2
    , restores = 3
    , weight = 0
    }

let moonshine : Refreshment =
    { cost = 30
    , description =
        ''
        Largely a slang term for high-proof distilled spirits produced in illicit or
        makeshift manner. Due to their unorthodox and improvised origins, moonshine can
        be dangerous to consume, often contaminated with dangerous chemicals. The stuff
        that doesn't kill you... will probably render you insensible and impervious
        to pain if it doesn't knock you out entirely.
        ''
    , effects =
        [DEQ.Effects.alcoholic, DEQ.ChemMaxHPChangeFactory +2] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Moonshine"
    , rarity = 3
    , restores = 0
    , weight = 0
    }

let mutfruit-juice : Refreshment =
    { cost = 8
    , description =
        ''
        Juice made from pressing the pulp of a mutfruit. A sharp and invigorating drink,
        which wakes up the mind and helps get the body moving.
        ''
    , effects =
        [DEQ.ChemSkillRerollFactory 1 ["AGI"]] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Mutfruit Juice"
    , rarity = 2
    , restores = 3
    , weight = 1
    }

let nuka-cherry : Refreshment =
    { cost = 40
    , description =
        ''
        Produced by the Nuka-Cola Corporation after it bought the patent for a rival
        beverage, Merle's Very Cherry Soda, Nuka-Cherry is a blend of the typical
        beloved Nuka-Cola recipe with a distinctive cherry flavoring and a bright red
        color. The refreshing taste and high sugar and caffeine contents of the drink
        make it an ideal pick-me-up for those needing a burst of energy.

        You receive 1 cap upon opening and drinking a bottle of Nuka-Cherry.
        ''
    , effects =
        [DEQ.GenericDEQFactory "+2 AP" "Immediately add 2 AP to the AP pool."] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Nuka-Cherry"
    , rarity = 3
    , restores = 3
    , weight = 1
    }

let nuka-cola : Refreshment =
    { cost = 20
    , description =
        ''
        First entering the market in 2044, Nuka-Cola rapidly became the number one soft
        drink in the united states and was the most popular beverage in the world soon
        after. A combination of seventeen fruit essences, precisely balanced to enhance
        the classic cola flavor. Each bottle contains 120% of the recommended daily
        amount of sugar, and excessive amounts of caffeine, but is also fortified with
        vitamins, minerals, and "health tonics". Due to it's popularity, it can still be
        found in vast quantities in the wasteland, and it is still popular even if the
        bottles are now warm and the carbonated fizz went flat long ago.

        You receive 1 cap upon opening and drinking a bottle of Nuka-Cola.
        ''
    , effects =
        [DEQ.GenericDEQFactory "+1 AP" "Immediately add 1 AP to the AP pool."] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Nuka-Cola"
    , rarity = 2
    , restores = 2
    , weight = 1
    }

let nuka-cola-quantum : Refreshment =
    { cost = 50
    , description =
        ''
        Introduced the same day the bombs fell, and thus difficult to find, Nuka-Cola
        Quantum was the newest flavor of Nuka-Cola to be released to the public, with
        twice the calories, twice the carbohydrates, twice the caffeine, and twice the
        taste. The drink's distinctive blue glow comes from the (same for human
        consumption) isotope additive developed by beverageers working with the United
        States military. Most bottles found were from special pre-release batches sent to
        chosen retailers or found in trucks in the process of shipping the product. The
        drink's effects are extremely potent.

        You receive 1 cap upon opening and drinking a bottle of Nuka-Cola Quantum.
        ''
    , effects =
        [DEQ.GenericDEQFactory "+5 AP" "Immediately add 5 AP to the AP pool."] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Nuka-Cola Quantum"
    , rarity = 5
    , restores = 10
    , weight = 1
    }

let refreshing-beverage : Refreshment =
    { cost = 110
    , description =
        ''
        A mysterious panacea made from medical supplies, this chemical concoction
        restores health and cleanses the body of radiation and chemical dependencies.
        The recipe has spread across the wasteland by word of mouth, but it requires
        numerous ingredients to produce, so it remains rare.
        ''
    , effects =
        [DEQ.GenericDEQFactory "Heal 10 Radiation Damage" "Heals 10 Radiation Damage upon Consumption, cannot be used during First Aid.", DEQ.GenericDEQFactory "Cure All Addictions" "Cure all addictions upon consumption."] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Refreshing Beverage"
    , rarity = 5
    , restores = 11
    , weight = 1
    }

let rum : Refreshment =
    { cost = 8
    , description =
        ''
        A liquor made from fermenting then distilling sugarcane molasses. Rum has strong
        historic ties to naval and maritime traditions, though there are few boats left
        that ply the irradiated seas, so it turns up often in coastal regions,
        particularly near to old harbors and ports.
        ''
    , effects =
        [DEQ.Effects.alcoholic, DEQ.ChemSkillRerollFactory 1 ["AGI"]] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Rum"
    , rarity = 2
    , restores = 0
    , weight = 1
    }

let tarberry-juice : Refreshment =
    { cost = 5
    , description =
        ''
        Juice made from pressing tarberries. Drinking Tarberry juice has a potent
        stimulant effect, making the drinker energetic and ready for action.
        ''
    , effects =
        [DEQ.GenericDEQFactory "+6 AP" "Immediately add 6 AP to the AP pool."] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Tarberry Juice"
    , rarity = 4
    , restores = 3
    , weight = 0
    }

let tato-juice : Refreshment =
    { cost = 7
    , description =
        ''
        Juice made from pressing Tatos. Despite the unpleasant taste, Tato juice is a
        fine way to prepare for vigorous action, as it allows the drinker to dig more
        deeply into reserves of stamina and push themselves further.
        ''
    , effects =
        [DEQ.GenericDEQFactory "Larger AP Pool" "The Group AP pool can hold 1 more AP than normal, until the end of the current scene."] : List DEQ.GenericDEQ -- TODO: Make sure there isn't a specified limit on this?
    , irradiated = 0
    , name = "Tato Juice"
    , rarity = 3
    , restores = 3
    , weight = 0
    }

let vodka : Refreshment =
    { cost = 5
    , description =
        ''
        A clear distilled alcoholic beverage, made from fermented rye, wheat, potatoes,
        or sugar beet molasses, typically imported from the Soviet Union Pre-War.
        ''
    , effects =
        [DEQ.Effects.alcoholic] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Vodka"
    , rarity = 3
    , restores = 2
    , weight = 1
    }

let water-dirty : Refreshment =
    { cost = 5
    , description =
        ''
        Water, collected from rivers, lakes, swimming pools, and any other unfiltered
        water source. It's not recommended to drink dirty water without filtering or
        boiling it, as it contains a fair amount of radiation... but water is scarce in
        some places and the choice of clean water isn't always available.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Dirty Water"
    , rarity = 0
    , restores = 2
    , weight = 0
    }

let water-purified : Refreshment =
    { cost = 20
    , description =
        ''
        Water which has been cleansed of any contaminants or radiation. Sometimes found
        in sealed cans or bottles in the wasteland, but often produced by taking water
        from other sources and boiling or filtering it. Locations with water pumps tend
        to tap into uncontaminated aquifers deep underground, giving them a consistent
        supply of clean water highly sought-after by survivors in the wasteland. In places
        where water is scarce, access to purified water is a precious commodity.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Purified Water"
    , rarity = 1
    , restores = 3
    , weight = 1
    }

let whiskey : Refreshment =
    { cost = 5
    , description =
        ''
        A triply-distilled alcoholic beverage made using grain mash. Pre-War versions
        were created across the United States and beyond. Some wasteland distilleries
        exist producing post-War versions using corn and razorgrain.
        ''
    , effects =
        [DEQ.Effects.alcoholic, DEQ.ChemSkillRerollFactory 2 ["STR"]] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Whiskey"
    , rarity = 3
    , restores = 0
    , weight = 1
    }

let wine : Refreshment =
    { cost = 5
    , description =
        ''
        An alcoholic beverage made from the fermentation of grape juice. There are
        numerous pre-War vintages to be found across the wasteland, often collected by
        the wealthy before the bombs fell, and a survivor who stumbles across an intact
        wine cellar can enjoy a drunken stupor for weeks or months.
        ''
    , effects =
        [DEQ.Effects.alcoholic, DEQ.GenericDEQFactory "+1 AP" "Immediately add 1 AP to the AP pool."] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Wine"
    , rarity = 3
    , restores = 0
    , weight = 1
    }

-- Food

let blamco-mac-and-cheese : Refreshment =
    { cost = 10
    , description =
        ''
        A pre-War fast-food product found in the wasteland many years later. They are
        normally found in brightly colored boxes with an image of the food--ready-made
        macaroni and cheese--on the front.

        ${Preserved}
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "BlamCo Brand Mac and Cheese"
    , rarity = 1
    , restores = 4
    , weight = 0
    }

let blamco-mac-and-cheese-preserved : Refreshment =
    RadlessFactory blamco-mac-and-cheese True

let bloatfly-baked : Refreshment =
    { cost = 15
    , description =
        ''
        A piece of Bloatfly meat, cooked with dry heat. The resultant food is unappetizing
        but filling and helps absorb radioactive contamination in the body for a while
        after eating.
        ''
    , effects =
        [DEQ.ChemDefenseBuffFactory 2 DEQ.DamageTypes.radiation] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Baked Bloatfly"
    , rarity = 1
    , restores = 6
    , weight = 0
    }

let bloatfly-meat : Refreshment =
    { cost = 8
    , description =
        ''
        A rough chunk of meat cut from the body of a dead Bloatfly. Filling and
        nutritious, but unappetizing even when cooked.

        Bloatfly meat can be cooked to produce Baked Bloatfly.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Bloatfly Meat"
    , rarity = 0
    , restores = 2
    , weight = 0
    }

let bloodbug-meat : Refreshment =
    { cost = 8
    , description =
        ''
        A chunk of bloody meat from a Bloodbug, a large and aggressive mutant mosquito
        that normally preys on livestock and other large animals.

        Bloodbug meat can be cooked to produce Bloodbug steak.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Bloodbug Meat"
    , rarity = 1
    , restores = 7
    , weight = 0
    }

let bloodbug-steak : Refreshment =
    { cost = 18
    , description =
        ''
        A piece of Bloodbug meat cooked over an open flame. If you can stand to eat it,
        and can ignore the aftertaste, it's reasonably nutritious and even leaves a
        feeling of renewed vigor and health for a while after eating.
        ''
    , effects =
        [DEQ.ChemMaxHPChangeFactory +3] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Bloodbug Steak"
    , rarity = 2
    , restores = 10
    , weight = 0
    }

let brahmin-meat : Refreshment =
    { cost = 28
    , description =
        ''
        A slab of meat from the two-headed mutant cattle that roam the wastelands. Not
        massively dissimilar to beef from before the War, though somewhat radioactive due
        to coming from a mutant animal.

        Can be cooked to produce ribeye steak.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Brahmin Meat"
    , rarity = 1
    , restores = 3
    , weight = 1
    }

let brain-fungus : Refreshment =
    { cost = 6
    , description =
        ''
        An off-white mushroom which resembles a brain, and which tends to be found in
        clusters growing in dark, dank corners. Slightly irradiated due simply to growing
        in the wastelands, but otherwise harmless, despite its slightly alarming name
        and appearance.

        Brain fungus is an ingredient in Mentats.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Brain Fungus"
    , rarity = 1
    , restores = 3
    , weight = 0
    }

let canned-dog-food : Refreshment =
    { cost = 6
    , description =
        ''
        A metal can of processed meat intended to be fed to dogs. Despite being made for
        dogs, it's entirely edible by humans, and the sealed can keeps the food within
        preserved and relatively safe to eat for long periods.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Canned Dog Food"
    , rarity = 0
    , restores = 3
    , weight = 0
    }

let carrot : Refreshment =
    { cost = 3
    , description =
        ''
        A slightly mutated form of the common, orange root vegetable. Easy enough to grow
        in decent quantities, and useful for a variety of recipes, small wasteland farms
        often include a few carrot patches.

        Carrots are a component of several different recipes for food items.

        ${Fresh}
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Carrot"
    , rarity = 1
    , restores = 3
    , weight = 0
    }

let carrot-fresh : Refreshment =
    RadlessFactory carrot False

let corn : Refreshment =
    { cost = 6
    , description =
        ''
        A highly versatile crop cultivated across the Americas for centuries. Corn remains
        largely unchanged by the War, and it remains a staple of wasteland agriculture.

        Corn is a main ingredient in vegetable starch, which can be used to make adhesives
        for crafting.

        ${Fresh}
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Corn"
    , rarity = 1
    , restores = 3
    , weight = 0
    }

let corn-fresh : Refreshment =
    RadlessFactory corn False

let cram : Refreshment =
    { cost = 25
    , description =
        ''
        A tin of processed meat produced in vast quantities pre-War to address food
        shortages and serve as a meat ration for soldiers. The tin, with an easy-open
        pull tab, helps keep the contents preserved for long periods, making Cram more
        useful for survivalists and those scavenging for food.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Cram"
    , rarity = 1
    , restores = 5
    , weight = 0
    }

let dandy-boy-apples : Refreshment =
    { cost = 7
    , description =
        ''
        Candied apples produced by the Dandy Boy company. An extremely sweet fruit-based
        snack packaged in a cardboard box. Normally well-preserved due to the high sugar
        content, boxes of these can still be found in the ruins of convenience stores
        and groceries across the wasteland, and in the possession of people who've
        recently raided said ruined convenience stores and groceries.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Dandy Boy Apples"
    , rarity = 0
    , restores = 3
    , weight = 0
    }

let deathclaw-egg : Refreshment =
    { cost = 69
    , description =
        ''
        A large egg laid by a deathclaw. Dangerous to obtain, as deathclaws lay them and
        hatch from them, but potentially valuable as they can be used in a few recipes
        which produce highly nutritious food.

        A deathclaw egg is a key ingredient in deathclaw omelettes.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Deathclaw Egg"
    , rarity = 3
    , restores = 7
    , weight = 0
    }

let deathclaw-meat : Refreshment =
    { cost = 110
    , description =
        ''
        A cut of meat from a deathclaw. Dangerous to obtain, as it can only be taken from
        a dead deathclaw or someone who killed a deathclaw, but valuable as they can be
        extremely nutritious, especially when properly prepared.

        Deathclaw meat can be cooked to produce deathclaw steak.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Deathclaw Meat"
    , rarity = 3
    , restores = 9
    , weight = 1
    }

let deathclaw-omelette : Refreshment =
    { cost = 80
    , description =
        ''
        An omelette made from a deathclaw egg. In addition to being highly nutritious, it
        stimulates the natural healing processes of the body, allowing a person to
        recover more quickly from harm for a while after eating.
        ''
    , effects =
        [DEQ.GenericDEQFactory "Delayed Regenerating HP" "If the next scene is combat, then you regain 1 HP at the start of each turn."] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Deathclaw Omelette"
    , rarity = 4
    , restores = 11
    , weight = 0
    }

let deathclaw-steak : Refreshment =
    { cost = 130
    , description =
        ''
        A slab of cooked deathclaw meat. Sufficiently filling and nutritious that it
        could easily be your only meal for the day, while also leaving you fitter and
        stronger for a while after eating.
        ''
    , effects =
        [DEQ.ChemSkillRerollFactory 1 ["STR"]] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Deathclaw Steak"
    , rarity = 4
    , restores = 11
    , weight = 0
    }

let fancy-lads-snack-cakes : Refreshment =
    { cost = 18
    , description =
        ''
        Small, frosted cakes, so laden with sugar and other sweeteners that they're
        essentially preserved forever. Proclaimed in advertising to be "America's
        Favorite Snackfood", these cakes can be found across the wastelands, in many
        teeth-achingly sweet varieties.

        ${Preserved}
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Fancy Lads Snack Cakes"
    , rarity = 0
    , restores = 3
    , weight = 0
    }

let fancy-lads-snack-cakes-preserved : Refreshment =
    RadlessFactory fancy-lads-snack-cakes True

let food-paste : Refreshment =
    { cost = 0
    , description =
        ''
        In the years before the Great War, several charter schools across America entered
        an experimental program: The Nutritional Alternative Paste Program (NAPP).
        Developed by Vault-Tec in conjunction with the Federal Government, this supplied
        participating schools with a supply of food paste, fortified with vitamins,
        minerals, and all the nutrients a growing body needs, and which would remain
        unspoiled for over a century.

        The paste is a flavorless mush, garish pink in color, and may have unknown
        long-term effects on psychology or physiology. It does, however, make the body
        healthier after consumption.
        ''
    , effects =
        [DEQ.ChemSkillRerollFactory 1 ["END"]] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Food Paste"
    , rarity = 2
    , restores = 7
    , weight = 0
    }

let gourd : Refreshment =
    { cost = 6
    , description =
        ''
        A large edible fruit, which is fleshy with a hard skin, like a pumpkin. The pulp
        inside can be scooped out and eaten or used in cooking, while the outer skin is
        often carved and used as decoration.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Gourd"
    , rarity = 1
    , restores = 3
    , weight = 1
    }

let gum-drops : Refreshment =
    { cost = 5
    , description =
        ''
        A pre-War confection, gum drops are small drops of congealed gelatin sweetened
        and flavored with a variety of overpowering flavors. The most common variety
        to survive the War, ironically, were labelled as radioactive for their powerful
        sour flavor. Long years sat in cupboards and on shelves has made this more
        literal, however.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Gum Drops"
    , rarity = 0
    , restores = 3
    , weight = 0
    }

let iguana-bits : Refreshment =
    { cost = 8
    , description =
        ''
        Chunks of raw iguana meat, in a small can or another sealed container. At least,
        you *hope* that's iguana meat. You don't see many iguanas around.

        Iguana bits are an ingredient in Iguana on a Stick and Iguana Soup.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Iguana Bits"
    , rarity = 1
    , restores = 4
    , weight = 0
    }

let iguana-on-a-stick : Refreshment =
    { cost = 33
    , description =
        ''
        Chunks of iguana meat on a wooden skewer, which have then been cooked. Not the
        best meal, but it'll do the trick if you're desperate or need to secure the
        loyalty of a canine companion.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Iguana On A Stick"
    , rarity = 2
    , restores = 6
    , weight = 0
    }

let iguana-soup : Refreshment =
    { cost = 21
    , description =
        ''
        A soup made with carrot and iguana bits. A more satisfying meal than any of the
        ingredients would have been alone.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Iguana Soup"
    , rarity = 3
    , restores = 10
    , weight = 1
    }

let instamash : Refreshment =
    { cost = 20
    , description =
        ''
        A packet of freeze-dried powdered mashed potato still sealed inside its cardboard
        box. Just add water!

        ${Preserved}
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Instamash"
    , rarity = 0
    , restores = 4
    , weight = 0
    }

let instamash-preserved : Refreshment =
    RadlessFactory instamash True

let institute-food-packet : Refreshment =
    { cost = 10
    , description =
        ''
        A small white box of food marked with the symbol of the Institute. Each box
        contains enriched ration bars filled with essential nutrients. Each ration bar
        contains your daily requirements of forty different vitamins, minerals, and
        nutrients, which are listed on the back of the box.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Institute Food Packet"
    , rarity = 2
    , restores = 5
    , weight = 0
    }

let melon : Refreshment =
    { cost = 6
    , description =
        ''
        A large, green, juicy fruit with a hard outer rind. Melons are grown in wasteland
        farms, or found growing in the wasteland. Either way, once cut open they're often
        very satisfying, sating hunger and quenching thirst all in one go.

        ${Fresh}
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Melon"
    , rarity = 1
    , restores = 3
    , weight = 1
    }

let melon-fresh : Refreshment =
    RadlessFactory melon False

let mirelurk-cake : Refreshment =
    { cost = 35
    , description =
        ''
        Flakes and fragments of mirelurk meat, mixed with mirelurk egg to bind it
        together into a patty and then cooked. In addition to some modest nutritional
        value, eating mirelurk cake allows you to survive underwater for longer.
        ''
    , effects =
        [DEQ.GenericDEQFactory "Water Breathing" "You can breathe underwater until the end of the next scene."] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Mirelurk Cake"
    , rarity = 3
    , restores = 12
    , weight = 0
    }

let mirelurk-egg : Refreshment =
    { cost = 0
    , description =
        ''
        Eggs found in mirelurk nests. Normally found in clutches of 2-3, these eggs hatch
        into mirelurk hatchlings if left undisturbed.

        Mirelurk eggs are a key ingredient of both Mirelurk Egg Omelette and Mirelurk
        Cake.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Mirelurk Egg"
    , rarity = 2
    , restores = 3
    , weight = 1
    }

let mirelurk-egg-omelette : Refreshment =
    { cost = 30
    , description =
        ''
        An omelette made from a mirelurk egg. In addition to their nutritional value,
        when properly prepared, these omelettes can leave you feeling energized and
        ready for action.
        ''
    , effects =
        [DEQ.GenericDEQFactory "+2 AP" "Immediately add 2 AP to the AP pool."] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Mirelurk Egg Omelette"
    , rarity = 3
    , restores = 7
    , weight = 0
    }

let mirelurk-meat : Refreshment =
    { cost = 18
    , description =
        ''
        The meat from dead mirelurks. This tends to be tougher and of poorer quality than
        the meat which comes from the weaker softshell mirelurk, but it is still entirely
        edible.

        Mirelurk meat is a key ingredient in Mirelurk Cake and can be cooked to produce
        roasted mirelurk meat.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Mirelurk Meat"
    , rarity = 1
    , restores = 6
    , weight = 0
    }

let mirelurk-meat-roasted : Refreshment =
    { cost = 40
    , description =
        ''
        A cooked portion of meat from a mirelurk. Though not to the same extent as the
        softshell meat, roasted mirelurk meat invigorates the body and mind and leaves
        you ready for action.
        ''
    , effects =
        [DEQ.GenericDEQFactory "Delayed +1 AP" "Gain +1 AP at the start of the next scene."] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Roasted Mirelurk Meat"
    , rarity = 2
    , restores = 8
    , weight = 0
    }

let mirelurk-meat-softshell : Refreshment =
    { cost = 22
    , description =
        ''
        Meat from a softshell mirelurk. Compared to normal mirelurk meat, this is
        especially tender and flaky, and makes for a much more satisfying food.

        Softshell Mirelurk Meat can be cooked to produce Cooked Softshell Meat.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Softshell Mirelurk Meat"
    , rarity = 2
    , restores = 6
    , weight = 0
    }

let mirelurk-softshell-meat-cooked : Refreshment =
    { cost = 40
    , description =
        ''
        The softer meat from mirelurks, cooked into a rough steak. A high-energy meal,
        those who eat cooked soft-shell meat are often invigorated and eager for action
        for a while afterwards.
        ''
    , effects =
        [DEQ.GenericDEQFactory "Delayed +1 AP" "Gain +1 AP at the start of the next scene."] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Cooked Softshell Meat"
    , rarity = 3
    , restores = 9
    , weight = 0
    }

let mirelurk-queen-meat : Refreshment =
    { cost = 22
    , description =
        ''
        The meat from a dead mirelurk Queen. This meat can make for a great fest, as it
        is nutrient rich and incredibly filling, leaving a sense of health and well-being
        in anyone who eats it, even raw. When cooked, these effects are even more
        pronounced.

        Queen Mirelurk Meat can be cooked to produce Mirelurk Queen Steak
        ''
    , effects =
        [DEQ.ChemSkillRerollFactory 1 ["END"]] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Queen Mirelurk Meat"
    , rarity = 4
    , restores = 10
    , weight = 0
    }

let mirelurk-queen-steak : Refreshment =
    { cost = 130
    , description =
        ''
        The rare, nutrient-rich meat of a mirelurk Queen, when properly prepared, can
        fortify the body and provided a surge of health and vitality, making you feel
        healthier and more resilient for a while after eating.
        ''
    , effects =
        [DEQ.ChemSkillChallengeFactory True 1 ["END"]] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Mirelurk Queen Steak"
    , rarity = 5
    , restores = 14
    , weight = 1
    }

let mole-rat-chunks : Refreshment =
    { cost = 8
    , description =
        ''
        Pieces of mole rat meat which've been cooked over a fire. The meat is somewhat
        tough and chewy, but leaves a feeling of vigor, as if your reserves of stamina
        have increased.
        ''
    , effects =
        [DEQ.GenericDEQFactory "Increase AP Cap" "Group pool can store +1 more AP until the end of the current scene."] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Mole Rat Chunks"
    , rarity = 1
    , restores = 7
    , weight = 0
    }

let mole-rat-meat : Refreshment =
    { cost = 5
    , description =
        ''
        A chunk of meat taken from a dead mole rat.

        Mole Rat Meat can be cooked to make Mole Rat Chunks.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Mole Rat Meat"
    , rarity = 0
    , restores = 5
    , weight = 0
    }

let mongrel-dog-meat : Refreshment =
    { cost = 8
    , description =
        ''
        A chunk of meat taken from a mongrel dog.

        Mongrel Dog Meat can be cooked to make Mutt Chops.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Mongrel Dog Meat"
    , rarity = 0
    , restores = 4
    , weight = 0
    }

let mutant-hound-chops : Refreshment =
    { cost = 12
    , description =
        ''
        A piece of mutant hound meat cooked to make it safer to eat. For reasons unknown,
        once cooked, the meat helps absorb radiation already in the body, lessening the
        effects of radiation poisoning. This effect doesn't compare to proper
        anti-radiation meds like RadAway, but it can be useful to have a few on hand
        to soak up the radiation you picked up during an excursion.
        ''
    , effects =
        [DEQ.GenericDEQFactory "Heal 2 Radiation Damage" "Heals 2 Radiation Damage upon Consumption, cannot be used during First Aid."] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Mutant Hound Chops"
    , rarity = 3
    , restores = 8
    , weight = 0
    }

let mutant-hound-meat : Refreshment =
    { cost = 8
    , description =
        ''
        A cut of meat taken from a dead mutant hound. This meat's off-green color and
        lumpy texture suggests that it would be quite unpleasant to eat. Eaten raw, it
        contains a fair amount of radiation and is unpalatable. Cooked, they're still
        not the tastiest food, but they're not hazardous.

        Mutant Hound Meat can be cooked to make Mutant Hound Chops.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Mutant Hound Meat"
    , rarity = 2
    , restores = 5
    , weight = 0
    }

let mutfruit : Refreshment =
    { cost = 8
    , description =
        ''
        Pronounced "mute-fruit", and short for mutated fruit, mutfruit is a mutated form
        of apple, which comes in several different varieties depending on where you are.
        These different varieties are cultivated for their sweet flavor, their use in
        cooking, and even medicinal properties in some rare cases.

        Mutfruit is part of a few different recipes, including being turned into Mutfruit
        Juice.

        ${Fresh}
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Mutfruit"
    , rarity = 0
    , restores = 3
    , weight = 0
    }

let mutfruit-fresh : Refreshment =
    RadlessFactory mutfruit False

let mutt-chops : Refreshment =
    { cost = 12
    , description =
        ''
        Cuts of mongrel dog meat cooked to make them more nutritious and edible.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Mutt Chops"
    , rarity = 1
    , restores = 6
    , weight = 0
    }

let noodle-cup : Refreshment =
    { cost = 20
    , description =
        ''
        A simple cup of noodle soup. Moderately filling and thirst-quenching, and simple
        enough to make if you can find the ingredients.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Noodle Cup"
    , rarity = 2
    , restores = 6
    , weight = 0
    }

let perfectly-preserved-pie : Refreshment =
    { cost = 20
    , description =
        ''
        Contained within the storage of a Port-A-Diner, this slice of fruit pie ahs been
        shielded from the radioactive environment since before the Great War, meaning that
        it isn't irradiated at all. Copious quantities of preservatives were used in
        making the pie, so it's still as edible as it was the day it was made.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Perfectly Preserved Pie"
    , rarity = 3
    , restores = 5
    , weight = 0
    }

let pork-n-beans : Refreshment =
    { cost = 10
    , description =
        ''
        A can containing a complete meal: beans stewed in tomato sauce with chunks of
        cured pork belly. The tin may be slightly rusted, and the label has partly
        fallen off due to age and water damage, but the contents are still as edible as
        ever, though moreso if warmed up first.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Pork 'n' Beans"
    , rarity = 0
    , restores = 4
    , weight = 0
    }

let potato-crisps : Refreshment =
    { cost = 7
    , description =
        ''
        A can of salted or flavored potato chips, sealed in an inert environment for
        freshness. A common snack pre-War, you can still find the odd pack of these in
        most places where people lived or worked.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Potato Crisps"
    , rarity = 0
    , restores = 3
    , weight = 0
    }

let potted-meat : Refreshment =     
    { cost = 25                     
    , description =                 
        ''
        A small metal tin of mixed processed meat, typically containing mixtures of meat
        from brahmin, radstags, mole rats, mongrel dogs, and anything else the maker can
        get their hands on (some of which may not be fit for human consumption).
        ''
    , effects =                     -- DEVIATION FROM SOURCE MATERIAL: Core Rulebook
        [] : List DEQ.GenericDEQ    -- states that this has an irradiation value of 1,
    , irradiated = 2                -- and an effect to roll 2 CD for radiation damage
    , name = "Potted Meat"          -- rather than one. That's stupid, and makes no sense,
    , rarity = 0                    -- so we're just marking this down here with an
    , restores = 6                  -- irradiation value of 2.
    , weight = 1                    
    }

let radroach-grilled : Refreshment =
    { cost = 7
    , description =
        ''
        A grilled chunk of meat taken from a radroach. Not especially appetizing,
        especially if you know where it came from, but sufficiently nutritious and
        filling for those with few other choices.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Grilled Radroach"
    , rarity = 1
    , restores = 5
    , weight = 0
    }

let radroach-meat : Refreshment =
    { cost = 3
    , description =
        ''
        Meat from the mutated, irradiated cockroaches known commonly as radroaches. The
        meat is mildly radioactive and offers little in the way of nourishment or
        satisfaction, and it isn't especially appetizing either. Cooking it provides
        only a marginal improvement.

        Radroach Meat can be cooked to make Grilled Radroach.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Radroach Meat"
    , rarity = 0
    , restores = 4
    , weight = 1
    }

let radscorpion-egg : Refreshment =
    { cost = 48
    , description =
        ''
        The egg of a radscorpion. Not easy to get hold of, as they tend to be found in
        places where there are radscorpions.

        Radscorpion Eggs can be used to make Radscorpion Egg Omelette.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Radscorpion Egg"
    , rarity = 3
    , restores = 6
    , weight = 0
    }

let radscorpion-egg-omelette : Refreshment =
    { cost = 65
    , description =
        ''
        Made from a radscorpion egg, these omelettes are highly prized by those who make
        considerable use of combat drugs, as something about the food cleanses the body
        to remove chemical dependencies and addictions.
        ''
    , effects =
        [DEQ.GenericDEQFactory "Removes all addictions" "All addictions afflicting the user are removed."] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Radscorpion Egg Omelette"
    , rarity = 4
    , restores = 9
    , weight = 0
    }

let radscorpion-meat : Refreshment =
    { cost = 55
    , description =
        ''
        Meat taken from inside the carapace of a radscorpion, normally from the tail or
        one of the legs where it's easier to get to. Difficult to obtain, as it requires
        killing a radscorpion or taking it from something that killed a radscorpion.

        Radscorpion meat can be cooked to make Radscorpion Steak.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Radscorpion Meat"
    , rarity = 2
    , restores = 9
    , weight = 1
    }

let radscorpion-steak : Refreshment =
    { cost = 65
    , description =
        ''
        A cooked slab of radscorpion meat. The process of cooking the meat has reduced
        the radiation within below dangerous levels and enhanced the nutritional value.
        Further, eating Radscorpion Steak heightens your resistance to extreme heat for
        a while.
        ''
    , effects =
        [DEQ.ChemDefenseBuffFactory 2 DEQ.DamageTypes.energy] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Radscorpion Steak"
    , rarity = 3
    , restores = 12
    , weight = 1
    }

let radstag-grilled : Refreshment =
    { cost = 60
    , description =
        ''
        A grilled rack of ribs from a radstag. Easy enough to make when you've got the
        main ingredient--radstag meat--but tasty and satisfying enough that your burdens
        feel a little lighter once you've finished eating.
        ''
    , effects =
        [DEQ.GenericDEQFactory "+25 Carry Weight" "Carry weight increases by +25 until the end of the next scene."] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Grilled Radstag"
    , rarity = 2
    , restores = 11
    , weight = 1
    }

let radstag-meat : Refreshment =
    { cost = 50
    , description =
        ''
        A piece of meat cut from the body of a dead radstag. It's bulky but can produce
        more nutritious and satisfying meals.

        Radstag meat can be cooked to make Grilled Radstag or used as an ingredient to
        make Radstag Stew
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Radstag Meat"
    , rarity = 1
    , restores = 8
    , weight = 1
    }

let radstag-stew : Refreshment =
    { cost = 60
    , description =
        ''
        Strips of radstag meat, with vegetables and some alcohol, cooked to make a thick,
        hearty stew. Filling and satisfying, the stew also helps fortify the body,
        making you more resistant to extreme heat for a while.
        ''
    , effects =
        [DEQ.ChemDefenseBuffFactory 3 DEQ.DamageTypes.energy] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Radstag Stew"
    , rarity = 3
    , restores = 12
    , weight = 1
    }

let razorgrain : Refreshment =
    { cost = 5
    , description =
        ''
        A tall, fast-growing grass similar to wheat, which can be ground down to make
        flour for making bread and other staple foods.

        Razorgrain is an ingredient in a few recipes.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Razorgrain"
    , rarity = 1
    , restores = 3
    , weight = 0
    }

let ribeye-steak : Refreshment =
    { cost = 40
    , description =
        ''
        A grilled piece of meat from a brahmin. The closest thing to a pre-War steak
        you're likely to find, a good piece of grilled brahmin makes for a satisfying
        meal.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Ribeye Steak"
    , rarity = 2
    , restores = 10
    , weight = 1
    }

let salisbury-steak : Refreshment =
    { cost = 20
    , description =
        ''
        A ready-to-eat meal of ground beef mixed with breadcrumbs, onion, and egg, served
        with gravy. The meal is pre-packaged, preserved, and sealed for freshness, and
        requires only heat to be made properly edible again.

        ${Preserved}
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Salisbury Steak"
    , rarity = 0
    , restores = 5
    , weight = 0
    }

let salisbury-steak-preserved : Refreshment =
    RadlessFactory salisbury-steak True

let silt-bean : Refreshment =
    { cost = 6
    , description =
        ''
        Growing from the vine of a silt bean plant, silt beans are red-brown pods of
        legumes growing in the wilderness. They're not easily cultivated for farming,
        though many have tried. The beans are an ingredient in a few recipes, such as
        radstag stew.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Silt Bean"
    , rarity = 1
    , restores = 3
    , weight = 0
    }

let squirrel-bits : Refreshment =
    { cost = 4
    , description =
        ''
        A few scraps and chunks of squirrel meat. Squirrels, not being especially
        large creatures, do not have much meat on them.

        Squirrel bits can be cooked to make Crispy Squirrel Bits, Squirrel on a Stick, or
        Squirrel Stew.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Squirrel Bits"
    , rarity = 1
    , restores = 4
    , weight = 0
    }

let squirrel-bits-crispy : Refreshment =
    { cost = 6
    , description =
        ''
        Chunks of squirrel meat, which have been fried until crispy, often found held
        together on a skewer. An easy way to get a little nutrition, and not so heavy a
        meal that it weighs you down while you digest it.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Crispy Squirrel Bits"
    , rarity = 2
    , restores = 6
    , weight = 0
    }

let squirrel-on-a-stick : Refreshment =
    { cost = 15
    , description =
        ''
        Chunks of squirrel meat skewered on a thin piece of wood for cooking, and then
        roasted over an open flame. Not enough meat for a decent meal, but certainly
        enough for a snack.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Squirrel on a Stick"
    , rarity = 2
    , restores = 7
    , weight = 0
    }

let squirrel-stew : Refreshment =
    { cost = 24
    , description =
        ''
        Chunk of squirrel meat, along with carrot, tato, and some bloodleaf, cooked
        together to create a thick stew. More filling and appetizing than the ingredients
        individually.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Squirrel Stew"
    , rarity = 2
    , restores = 10
    , weight = 1
    }

let stingwing-filet : Refreshment =
    { cost = 35
    , description =
        ''
        A cooked piece of stingwing meat. Not the most appetizing of foods, but
        surprisingly useful for those surviving in the wastelands, as it can sharpen
        the senses and make you feel more aware of your surroundings.
        ''
    , effects =
        [DEQ.ChemSkillRerollFactory 1 ["PER"]] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Stingwing Filet"
    , rarity = 2
    , restores = 11
    , weight = 0
    }

let stingwing-meat : Refreshment =
    { cost = 30
    , description =
        ''
        Meat taken from the body of a stingwing, a mutated scorpionfly with a nasty
        sting. Doesn't look especially tasty, but it cooks reasonably well once you've
        pried the exoskeleton off.

        Stingwing meat can be cooked to create Stingwing Filet.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Stingwing Meat"
    , rarity = 1
    , restores = 8
    , weight = 0
    }

let sugar-bombs : Refreshment =
    { cost = 1
    , description =
        ''
        A pre-War breakfast cereal with "explosive great taste", the box contains little
        cereal shapes resembling stylized atomic bombs coated in copious amounts of sugar.
        Some boxes claim to contain a prize inside.

        ${Preserved}
        ''
    , effects =
        [DEQ.GenericDEQFactory "Delayed +1 AP" "Gain +1 AP at the start of the next scene."] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Sugar Bombs"
    , rarity = 0
    , restores = 4
    , weight = 0
    }

let sugar-bombs-preserved : Refreshment =
    RadlessFactory sugar-bombs True

let sweet-roll : Refreshment =
    { cost = 9
    , description =
        ''
        A small, sweetened pastry or baked confection normally made as a treat for
        children or people who need a bit of cheering up.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Sweet Roll"
    , rarity = 1
    , restores = 4
    , weight = 0
    }

let tarberry : Refreshment =
    { cost = 5
    , description =
        ''
        Small purple berries of the Tarberry plant, a water-grown crop similar to pre-War
        cranberries. A useful ingredient in several recipes, but difficult to cultivate
        in any location without a decent supply of reasonably cheap water. Communities
        able to grow tarberries often need to protect their crop from raiders.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Tarberry"
    , rarity = 3
    , restores = 1
    , weight = 0
    }

let tato : Refreshment =
    { cost = 7
    , description =
        ''
        A mutated hybrid of pre-War tomato and potato plants, with the stem and reddish
        skin of the former and the brownish flesh of the latter. Tatos provide decent
        nutrition, but taste disgusting. However, they're relatively easy to grow and
        thus are a staple of wasteland agriculture and is an ingredient in a variety of
        recipes.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Tato"
    , rarity = 1
    , restores = 3
    , weight = 0
    }

let vegetable-soup : Refreshment =
    { cost = 13
    , description =
        ''
        A simple soup made with carrot and tato. A reasonably filling meal, sating both
        hunger and thirst, while also helping to fortify the body against radiation
        poisoning for a while.
        ''
    , effects =
        [DEQ.ChemDefenseBuffFactory 2 DEQ.DamageTypes.radiation] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Vegetable Soup"
    , rarity = 2
    , restores = 7
    , weight = 1
    }

let yao-guai-meat : Refreshment =
    { cost = 85
    , description =
        ''
        Another dangerous meat to obtain, Yao Guai Meat comes from the bodies of Yao Guai,
        a ferocious mutated form of bear that roams the wastelands. The meat is highly
        prized and nutritious, though still irradiated if consumed raw.

        Yao Guai Meat can be cooked to make Yao Guai Ribs or Yao Guai Roast.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Yao Guai Meat"
    , rarity = 3
    , restores = 9
    , weight = 1
    }

let yao-guai-ribs : Refreshment =
    { cost = 90
    , description =
        ''
        A rack of ribs made with Yao Guai Meat. Yao Guai Ribs are both a satisfying meal
        and inspire a sense of invincibility, while also boosting your tolerance for pain
        for a short while.
        ''
    , effects =
        [DEQ.ChemDefenseBuffFactory 2 DEQ.DamageTypes.physical] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Yao Guai Ribs"
    , rarity = 4
    , restores = 13
    , weight = 1
    }

let yao-guai-roast : Refreshment =
    { cost = 110
    , description =
        ''
        A roasted piece of Yao Guai meat cooked with carrot and tato. An extremely
        filling and satisfying meal, and many have claimed that it heightens their killer
        instinct and ability to inflict harm for a while after eating.
        ''
    , effects =
        [DEQ.ChemDamageBuffFactory 2 "Melee"] : List DEQ.GenericDEQ
    , irradiated = 0
    , name = "Yao Guai Roast"
    , rarity = 4
    , restores = 14
    , weight = 1
    }

let yum-yum-deviled-eggs : Refreshment =
    { cost = 20
    , description =
        ''
        Hard-boiled eggs stuffed with a spicy filling, which was preserved and sealed for
        freshness before the Great War.
        ''
    , effects =
        [] : List DEQ.GenericDEQ
    , irradiated = 1
    , name = "Yum-Yum Deviled Eggs"
    , rarity = 0
    , restores = 4
    , weight = 0
    }

in  { Refreshment
    , ProcessedRefreshment
    , RefreshmentFactory
    , Beverages =
        { beer
        , blood-pack
        , blood-pack-glowing
        , blood-pack-irradiated
        , bourbon
        , brahmin-milk
        , dirty-wastelander
        , melon-juice
        , moonshine
        , mutfruit-juice
        , nuka-cherry
        , nuka-cola
        , nuka-cola-quantum
        , refreshing-beverage
        , rum
        , tarberry-juice
        , tato-juice
        , vodka
        , water-dirty
        , water-purified
        , whiskey
        , wine
        }
    , Food =
        { blamco-mac-and-cheese
        , bloatfly-baked
        , bloatfly-meat
        , bloodbug-meat
        , bloodbug-steak
        , brahmin-meat
        , brain-fungus
        , canned-dog-food
        , carrot
        , corn
        , cram
        , dandy-boy-apples
        , deathclaw-egg
        , deathclaw-meat
        , deathclaw-omelette
        , deathclaw-steak
        , fancy-lads-snack-cakes
        , food-paste
        , gourd
        , gum-drops
        , iguana-bits
        , iguana-on-a-stick
        , iguana-soup
        , instamash
        , institute-food-packet
        , melon
        , mirelurk-cake
        , mirelurk-egg
        , mirelurk-egg-omelette
        , mirelurk-meat
        , mirelurk-meat-roasted
        , mirelurk-meat-softshell
        , mirelurk-softshell-meat-cooked
        , mirelurk-queen-meat
        , mirelurk-queen-steak
        , mole-rat-chunks
        , mole-rat-meat
        , mongrel-dog-meat
        , mutant-hound-chops
        , mutant-hound-meat
        , mutfruit
        , mutt-chops
        , noodle-cup
        , perfectly-preserved-pie
        , pork-n-beans
        , potato-crisps
        , potted-meat
        , radroach-grilled
        , radroach-meat
        , radscorpion-egg
        , radscorpion-egg-omelette
        , radscorpion-meat
        , radscorpion-steak
        , radstag-grilled
        , radstag-meat
        , radstag-stew
        , razorgrain
        , ribeye-steak
        , salisbury-steak
        , silt-bean
        , squirrel-bits
        , squirrel-bits-crispy
        , squirrel-on-a-stick
        , squirrel-stew
        , stingwing-filet
        , stingwing-meat
        , sugar-bombs
        , sweet-roll
        , tarberry
        , tato
        , vegetable-soup
        , yao-guai-meat
        , yao-guai-ribs
        , yao-guai-roast
        , yum-yum-deviled-eggs
        }
}
