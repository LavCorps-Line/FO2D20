-- src/weapons.dhall

-- Imports --

let Prelude = ./prelude.dhall
let DEQ = ./genericDEQ.dhall
let Utils = ./utils.dhall
let Mods = ./mods.dhall

-- Type Defs --

let Weapon : Type =
    { ammunition : Optional Text
    , cost : Natural
    , damage : Natural
    , description : Text
    , dtype : DEQ.GenericDEQ
    , effects : List DEQ.GenericDEQ
    , mods : List Mods.Mod
    , name : Text
    , qualities : List DEQ.GenericDEQ
    , range : Optional Natural
    , rarity : Natural
    , rate : Optional Natural
    , weight : Natural
    , wtype : Text
    }

let ProcessedWeapon : Type =
    { ammunition : Optional Text
    , cost : Text
    , damage : Text
    , description : Text
    , dtype : Text
    , effects : Text
    , entryGeneric : Text
    , entryLinked : Text
    , modsList : Text
    , name : Text
    , qualities : Text
    , range : Optional Text
    , rarity : Text
    , rate : Optional Text
    , slug : Text
    , weight : Text
    , wtype : Text
    }

-- Type Factories --

let WeaponFactory : forall(wpn : Weapon) -> ProcessedWeapon = \(wpn : Weapon) ->
    let ammunition = Prelude.Optional.default Text "N/A" wpn.ammunition
    let cost = Utils.Tooltipify (Utils.ParseValue wpn.cost) "in caps"
    let damage = Utils.Tooltipify (Utils.ParseValue wpn.damage) "Base damage rating of the weapon measured as combat dice rolled on hit."
    let dtype = DEQ.ProcessDEQ wpn.dtype
    let effects = DEQ.ProcessDEQList wpn.effects
    let name = wpn.name
    let qualities = DEQ.ProcessDEQList wpn.qualities
    let range = Utils.Tooltipify (Utils.ConvertRange (Prelude.Optional.default Natural Utils.Values.NotApplicable wpn.range)) "Determines the optimal firing range of the gun."
    let rarity = Utils.Mono (Utils.ParseValue wpn.rarity)
    let rate = Utils.Tooltipify (Utils.ParseValue (Prelude.Optional.default Natural Utils.Values.NotApplicable wpn.rate)) "Determines the amount of extra ammunition that can be expended to increase damage die rolled."
    let slug = Utils.Sluginator name
    let weight = Utils.Tooltipify (Utils.ConvertWeight wpn.weight) "in pounds (lbs)"
    let wtype = Utils.Tooltipify wpn.wtype "Determines the Skill check made to attack with."

    -- Table entry for use in weapons index
    let entryGeneric = Prelude.Text.concat
    [ "| "
    , "${name} | "
    , "${wtype} | "
    , "${damage} | "
    , "${effects} | "
    , "${dtype} | "
    , "${qualities} | "
    , "${weight} | "
    , "${cost} | "
    , "${rarity} |"
    ]

    -- Table entry for use in i.e. type-specific pages
    let entryLinked = Prelude.Text.concat
    [ "| [${name}](#${slug}) "
    , "| ${wtype} "
    , "| ${damage} "
    , "| ${effects} "
    , "| ${dtype} "
    , if (Utils.Exists Natural wpn.rate) then "| ${rate} " else ""
    , if (Utils.Exists Natural wpn.range) then "| ${range} " else ""
    , "| ${qualities} "
    , "| ${weight} "
    , "| ${cost} "
    , "| ${rarity} |"
    ]

    let modsList = Mods.ProcessMods wpn.mods

    let attributes = Prelude.Text.concat
    [ "* Weapon Type: ${wtype}\n"
    , "* Damage Rating: ${damage}\n"
    , if Prelude.Natural.equal (List/length DEQ.GenericDEQ wpn.effects) 0 then "" else "* Damage Effects: ${effects}\n"
    , "* Damage Type: ${dtype}\n"
    , if (Utils.Exists Natural wpn.rate) then "* Fire Rate: ${rate}\n" else ""
    , if (Utils.Exists Natural wpn.range) then "* Range: ${range}\n" else ""
    , if Prelude.Natural.equal (List/length DEQ.GenericDEQ wpn.qualities) 0 then "" else "* Qualities: ${qualities}\n"
    , "* Weight: ${weight}\n"
    , "* Cost: ${cost}\n"
    , "* Rarity: ${rarity}\n"
    ]

    let description = Prelude.Text.concat
    [ "## ${name}\n\n"
    , if (Utils.Exists Text wpn.ammunition) then "**Ammunition**: ${ammunition}\n\n" else ""
    , "${wpn.description}\n"
    , "${attributes}\n"
    , "${modsList}\n"
    ]

    in
    { ammunition = if (Utils.Exists Text wpn.ammunition) then (Some ammunition) else (None Text)
    , cost = cost
    , damage = damage
    , description = description
    , dtype = dtype
    , effects = effects
    , entryGeneric = entryGeneric
    , entryLinked = entryLinked
    , modsList = modsList
    , name = name
    , qualities = qualities
    , range = if (Utils.Exists Natural wpn.range) then (Some range) else (None Text)
    , rarity = rarity
    , rate = if (Utils.Exists Natural wpn.rate) then (Some rate) else (None Text)
    , slug = slug
    , weight = weight
    , wtype = wtype
    }

-- Gun Defs --

-- Big Guns

let fat-man : Weapon =
    { ammunition = Some "Mini-Nuke"
    , cost = 512
    , damage = 21
    , description =
        ''
        The M42 Nuclear Catapult is a shoulder-fired infantry support weapon which
        launches compact tactical nuclear warheads-mini-nukes, each about the size of a
        football-over a moderate distance to inflict massive destruction upon the target
        location.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.breaking
        , DEQ.Effects.radioactive
        , DEQ.Effects.vicious
        ]
    , mods =
        [] : List Mods.Mod
    , name = "Fat Man"
    , qualities =
        [ DEQ.Qualities.blast
        , DEQ.Qualities.inaccurate
        , DEQ.Qualities.two-handed
        ]
    , range = Some 1
    , rarity = 4
    , rate = Some 0
    , weight = 31
    , wtype = "Big Guns"
    }

let flamer : Weapon =
    { ammunition = Some "Flamer Fuel"
    , cost = 137
    , damage = 3
    , description =
        ''
        A flamethrower, or flamer, is a weapon which sprays an ignited fuel mixture over
        an area, essentially creating a jet of flame that can burn or ignite targets
        at a distance. Ideal for clearing dead foliage, irritating vermin, and fortified
        bunkers!

        A flamer can accept one of each of the following mods, which are unique to
        Flamers and **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.energy
    , effects =
        [ DEQ.Effects.burst
        , DEQ.Effects.persistent DEQ.DamageTypes.energy
        , DEQ.Effects.spread
        ]
    , mods =
        [ Mods.Mods.flamer-long-barrel
        , Mods.Mods.flamer-napalm
        , Mods.Mods.flamer-compression-nozzle
        , Mods.Mods.flamer-vaporization-nozzle
        , Mods.Mods.flamer-large-tank
        , Mods.Mods.flamer-huge-tank
        ]
    , name = "Flamer"
    , qualities =
        [ DEQ.Qualities.debilitating
        , DEQ.Qualities.inaccurate
        , DEQ.Qualities.two-handed
        ]
    , range = Some 0
    , rarity = 3
    , rate = Some 4
    , weight = 16
    , wtype = "Big Guns"
    }

let gatling-laser : Weapon =
    { ammunition = Some "Fusion Cells, or Fusion Core"
    , cost = 804
    , damage = 3
    , description =
        ''
        A high rate-of-fire laser weapon, the Gating laser uses several rotating barrels
        fired in quick succession to produce a devastating fusillade of laser pulses.
        While the individual laser pulses lack in stopping power, the sheer mass of them
        can wear down almost any target. Gatling lasers can operate from any sufficient
        power source, and while fusion cells can be used to power them, the rate-of-fire
        means that they are often quickly expended. Most users instead rely on a larger
        power source, such as a a fusion core.

        A Gatling laser can accept one of each of the following mods which are
        **installed with the Science skill.** These resemble some common Energy Weapon
        mods, but somewhat larger and slightly different in effect due to the Gatling
        laser's size.
        ''
    , dtype = DEQ.DamageTypes.energy
    , effects =
        [ DEQ.Effects.burst
        , DEQ.Effects.piercing 1
        ]
    , mods =
        [ Mods.Mods.gatling-laser-charging-barrels
        , Mods.Mods.gatling-laser-beta-wave-tuner
        , Mods.Mods.gatling-laser-boosted-capacitor
        , Mods.Mods.gatling-laser-photon-agitator
        , Mods.Mods.gatling-laser-photon-exciter
        , Mods.Mods.gatling-laser-beam-focuser
        , Mods.Mods.gatling-laser-reflex-sight
        ]
    , name = "Gatling Laser"
    , qualities =
        [ DEQ.Qualities.gatling
        , DEQ.Qualities.inaccurate
        , DEQ.Qualities.two-handed
        ]
    , range = Some 1
    , rarity = 3
    , rate = Some 6
    , weight = 19
    , wtype = "Big Guns"
    }

let heavy-incinerator : Weapon =
    { ammunition = Some "Flamer Fuel"
    , cost = 350
    , damage = 5
    , description =
        ''
        Like a flamethrower, a heavy incinerator also propels ignited fuel mixtures to
        burn a target. However, the heavy incinerator differs in that it appears to lob
        a succession of fireballs at the target, lobbing them in an arc like a
        grenade launcher. These fireballs burst upon impact, spreading flames as the fuel
        splashes off the target.
        ''
    , dtype = DEQ.DamageTypes.energy
    , effects =
        [ DEQ.Effects.burst
        , DEQ.Effects.persistent DEQ.DamageTypes.energy
        , DEQ.Effects.spread
        ]
    , mods =
        [] : List Mods.Mod
    , name = "Heavy Incinerator"
    , qualities =
        [ DEQ.Qualities.debilitating
        , DEQ.Qualities.two-handed
        ]
    , range = Some 1
    , rarity = 4
    , rate = Some 3
    , weight = 20
    , wtype = "Big Guns"
    }

let junk-jet : Weapon =
    { ammunition = Some "Anything"
    , cost = 285
    , damage = 6
    , description =
        ''
        The Junk Jet is a makeshift heavy weapon which fires anything. Any items loaded
        into the weapon's hopper are hurled with deadly force from the business end
        of the gun. Naturally, this makes it quite useful in situations where random
        junk is abundant but actual ammunition is scarce.

        **Special**: When you fire the Junk Jet, choose any item you are currently
        carrying; that item is fired by the gun. Choose one addition item for each
        additional shot of ammunition spent by the attack. Items must be small enough
        to be held in one hand and loaded into the hopper. At the end of the scene,
        you may roll 1 CD for each item fired from the Junk Jet; on an Effect, that item
        survived the attack and can be reclaimed.

        A junk jet can accept one each of the following mods, which are distinct to the
        Junk Jet and **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.junk-jet-long-barrel
        , Mods.Mods.junk-jet-electrification-module
        , Mods.Mods.junk-jet-ignition-module
        , Mods.Mods.junk-jet-gunner-sight
        , Mods.Mods.junk-jet-recoil-compensating-stock
        ]
    , name = "Junk Jet"
    , qualities =
        [ DEQ.Qualities.two-handed
        ]
    , range = Some 1
    , rarity = 3
    , rate = Some 1
    , weight = 30
    , wtype = "Big Guns"
    }

let minigun : Weapon =
    { ammunition = Some "5mm"
    , cost = 382
    , damage = 3
    , description =
        ''
        The minigun is a motorized Gatling-style rapid fire weapon commonly employed
        by the military, or other lesser factions that managed to acquire such a weapon.
        Miniguns can be found as mounted door guns on Vertibirds or as a handheld heavy
        assault and support weapon. The high rate-of-fire of the weapon means that it
        can produce devastating results, but at a cost of consuming ammo at incredible
        rates, making it difficult to keep the weapon supplied.

        A minigun can accept one each of the following mods, which are unique to the
        minigun and **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.burst
        , DEQ.Effects.spread
        ]
    , mods =
        [ Mods.Mods.minigun-accelerated-barrel
        , Mods.Mods.minigun-tri-barrel
        , Mods.Mods.minigun-shredder
        , Mods.Mods.minigun-gunner-sight
        ]
    , name = "Minigun"
    , qualities =
        [ DEQ.Qualities.gatling
        , DEQ.Qualities.inaccurate
        , DEQ.Qualities.two-handed
        ]
    , range = Some 1
    , rarity = 2
    , rate = Some 5
    , weight = 27
    , wtype = "Big Guns"
    }

let missile-launcher : Weapon =
    { ammunition = Some "Missile"
    , cost = 314
    , damage = 11
    , description =
        ''
        A refined, highly adaptable multipurpose missile weapon introduced into the
        American arsenal, the modular launcher is smaller and more maneuverable than
        the earlier model. It consists of the two-part launch tube, firing mechanism,
        mounting brackets, open battle sights, and a mount for a guidance system.
        It is a breech-loading weapon, reloaded by lifting the front section of the
        launch tube and sliding the 72mm fin-stabilized missile inside. The rear part
        of the tube is designed to safely disperse launch gasses, allowing it to be
        launched from confined spaces. Other forms and designs of the Missile Launcher
        exist across the wastelands, but this form is common enough that most consider it
        the standard.

        A Missile Launcher can accept one of each of the following mods, which are unique
        to the Missile Launcher and **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.missile-launcher-triple-barrel
        , Mods.Mods.missile-launcher-quad-barrel
        , Mods.Mods.missile-launcher-bayonet
        , Mods.Mods.missile-launcher-stabilizer
        , Mods.Mods.missile-launcher-night-vision-scope
        , Mods.Mods.missile-launcher-scope
        , Mods.Mods.missile-launcher-targeting-computer
        ]
    , name = "Missile Launcher"
    , qualities =
        [ DEQ.Qualities.blast
        , DEQ.Qualities.two-handed
        ]
    , range = Some 2
    , rarity = 4
    , rate = Some 0
    , weight = 21
    , wtype = "Big Guns"
    }

-- Energy Weapons

let gamma-gun : Weapon =
    { ammunition = Some "Gamma Rounds"
    , cost = 156
    , damage = 3
    , description =
        ''
        A crude-looking weapon, the gamma gun emits directed blasts of high-intensity
        ionizing radiation which can be devastating to any creature not immune nor
        massively resistant to radiation, while leaving other targets largely unharmed.

        A gamma gun can accept one type of each of the following mods, which are unique
        to the gamma gun and **installed with the Science skill**:
        ''
    , dtype = DEQ.DamageTypes.radiation
    , effects =
        [ DEQ.Effects.piercing 1
        , DEQ.Effects.stun
        ]
    , mods =
        [ Mods.Mods.gamma-gun-deep-dish
        , Mods.Mods.gamma-gun-electric-signal-carrier-antennae
        , Mods.Mods.gamma-gun-signal-repeater
        ]
    , name = "Gamma Gun"
    , qualities =
        [ DEQ.Qualities.blast
        , DEQ.Qualities.inaccurate
        ]
    , range = Some 1
    , rarity = 5
    , rate = Some 1
    , weight = 3
    , wtype = "Energy Weapon"
    }

let institute-laser : Weapon =
    { ammunition = Some "Fusion Cells"
    , cost = 50
    , damage = 3
    , description =
        ''
        Developed by The Institute after the Great War, Institute lasers differ in
        design and function from the pre-War lasers used by other factions such as the
        Brotherhood of Steel. Eschewing raw power for a rapid succession of beams,
        these lasers can lay down a searing volley of suppressive fire.

        The default profile is for an Institute laser pistol. An Institute laser can
        accept one type of each of the following mods. Any Stock mods change the weapon
        to an Institute laser rifle.
        ''
    , dtype = DEQ.DamageTypes.energy
    , effects =
        [ DEQ.Effects.burst
        ]
    , mods =
        [ Mods.Mods.e-automatic-barrel
        , Mods.Mods.e-improved-barrel
        , Mods.Mods.e-long-barrel
        , Mods.Mods.e-beta-wave-tuner
        , Mods.Mods.e-boosted-capacitor
        , Mods.Mods.e-photon-agitator
        , Mods.Mods.e-photon-exciter
        , Mods.Mods.e-beam-focuser
        , Mods.Mods.e-beam-splitter
        , Mods.Mods.e-gyro-compensating-lens
        , Mods.Mods.e-long-night-vision-scope
        , Mods.Mods.e-long-scope
        , Mods.Mods.e-recon-scope
        , Mods.Mods.e-reflex-sight
        , Mods.Mods.e-short-night-vision-scope
        , Mods.Mods.e-short-scope
        , Mods.Mods.e-standard-stock
        ]
    , name = "Institute Laser"
    , qualities =
        [ DEQ.Qualities.close-quarters
        , DEQ.Qualities.inaccurate
        ]
    , range = Some 0
    , rarity = 2
    , rate = Some 3
    , weight = 4
    , wtype = "Energy Weapon"
    }

let laser-gun : Weapon =
    { ammunition = Some "Fusion Cells"
    , cost = 69
    , damage = 3
    , description =
        ''
        Laser guns are high-tech weapons which emit a concentrated beam of coherent,
        directional light, intense enough to inflict serious damage. They were a
        relatively new development in the years before the Great War, and thus are not
        a common sight in the wasteland, outside of those groups who've secured old
        military weapons caches. Most surviving models of laser gun--such as the
        dependable AER9, widely used by the U.S. Army--use an internal capacitor
        charged by a microfusion power cell.

        The default profile is for a laser pistol. A laser gun can accept one of each
        type of the following mods. Any stock mods change the weapon to a laser rifle.
        ''
    , dtype = DEQ.DamageTypes.energy
    , effects =
        [ DEQ.Effects.piercing 1
        ]
    , mods =
        [ Mods.Mods.e-automatic-barrel
        , Mods.Mods.e-improved-barrel
        , Mods.Mods.e-long-barrel
        , Mods.Mods.e-sniper-barrel
        , Mods.Mods.e-beta-wave-tuner
        , Mods.Mods.e-boosted-capacitor
        , Mods.Mods.e-photon-agitator
        , Mods.Mods.e-photon-exciter
        , Mods.Mods.e-sharpshooters-grip
        , Mods.Mods.e-beam-focuser
        , Mods.Mods.e-beam-splitter
        , Mods.Mods.e-gyro-compensating-lens
        , Mods.Mods.e-long-night-vision-scope
        , Mods.Mods.e-long-scope
        , Mods.Mods.e-recon-scope
        , Mods.Mods.e-reflex-sight
        , Mods.Mods.e-short-night-vision-scope
        , Mods.Mods.e-short-scope
        , Mods.Mods.e-marksmans-stock
        , Mods.Mods.e-recoil-compensating-stock
        , Mods.Mods.e-standard-stock
        ]
    , name = "Laser Gun"
    , qualities =
        [ DEQ.Qualities.close-quarters
        ]
    , range = Some 0
    , rarity = 2
    , rate = Some 2
    , weight = 4
    , wtype = "Energy Weapon"
    }

let laser-musket : Weapon =
    { ammunition = Some "Fusion Cells"
    , cost = 57
    , damage = 5
    , description =
        ''
        The laser musket is a homemade form of laser rifle, blending advanced
        technology with strangely primitive mechanisms, popular with militia groups
        such as the Minutemen. The laser musket must be manually primed, charging the
        makeshift capacitor through turning a hand-crank on the weapon's rear. While
        seemingly crude, they can make for a potent sniper's weapons, especially with
        a modified capacitor able to hold a larger charge.

        **Special**: Firing a laser musket consumes two shots of ammunition each time
        it is fired. Each capacitor mod for the laser musket increases both damage and
        number of shots consumed, reducing the damage by -1 for each shot consumed
        fewer than normal (to a minimum of 4 and 1 shot consumed).

        A laser musket can accept the following mods:
        ''
    , dtype = DEQ.DamageTypes.energy
    , effects =
        [ DEQ.Effects.piercing 1
        ]
    , mods =
        [ Mods.Mods.e-bracketed-long-barrel
        , Mods.Mods.e-bracketed-short-barrel
        , Mods.Mods.e-long-barrel
        , Mods.Mods.e-beam-focuser
        , Mods.Mods.e-beam-splitter
        , Mods.Mods.e-gyro-compensating-lens
        , Mods.Mods.e-long-night-vision-scope
        , Mods.Mods.e-long-scope
        , Mods.Mods.e-recon-scope
        , Mods.Mods.e-reflex-sight
        , Mods.Mods.e-short-night-vision-scope
        , Mods.Mods.e-short-scope
        , Mods.Mods.e-full-stock
        , Mods.Mods.laser-musket-three-crank-capacitor
        , Mods.Mods.laser-musket-four-crank-capacitor
        , Mods.Mods.laser-musket-five-crank-capacitor
        , Mods.Mods.laser-musket-six-crank-capacitor
        ]
    , name = "Laser Musket"
    , qualities =
        [ DEQ.Qualities.two-handed
        ]
    , range = Some 1
    , rarity = 1
    , rate = Some 0
    , weight = 13
    , wtype = "Energy Weapon"
    }

let plasma-gun : Weapon =
    { ammunition = Some "Plasma Cartridges"
    , cost = 123
    , damage = 6
    , description =
        ''
        Plasma guns, or plasma casters, are high-tech weapons firing super-heated
        ionized gas, or plasma. Powered by a plasma cartridge, these weapons are rare
        and potent, delivering damage through the high-speed impact of the plasma bolt
        and then thermal transfer as the heat from the plasma bolt is transferred into
        the target at the moment of impact.

        **Special**: Plasma guns inflict both Physical and Energy damage. Roll damage
        as normal, and then reduce the total by whichever of the target's damage
        resistances is lower out of Physical or Energy. Any persistent damage effects
        (applied by weapon mods) inflict Energy damage.

        The default profile is for a plasma pistol. A plasma gun can accept one type
        each of the following mods. Any stock mods change the weapon to a plasma rifle.
        ''
    , dtype =
        DEQ.GenericDEQFactory "Physical/Energy" "Deals Physical and Energy, targeting whichever resistance is lowest. Any persistent damage effects inflict Energy damage."
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.e-automatic-barrel
        , Mods.Mods.e-flamer-barrel
        , Mods.Mods.e-improved-barrel
        , Mods.Mods.e-sniper-barrel
        , Mods.Mods.e-splitter
        , Mods.Mods.e-beta-wave-tuner
        , Mods.Mods.e-boosted-capacitor
        , Mods.Mods.e-photon-agitator
        , Mods.Mods.e-photon-exciter
        , Mods.Mods.e-long-night-vision-scope
        , Mods.Mods.e-long-scope
        , Mods.Mods.e-recon-scope
        , Mods.Mods.e-reflex-sight
        , Mods.Mods.e-short-night-vision-scope
        , Mods.Mods.e-short-scope
        , Mods.Mods.e-marksmans-stock
        , Mods.Mods.e-recoil-compensating-stock
        , Mods.Mods.e-standard-stock
        ]
    , name = "Plasma Gun"
    , qualities =
        [ DEQ.Qualities.close-quarters
        ]
    , range = Some 0
    , rarity = 3
    , rate = Some 1
    , weight = 4
    , wtype = "Energy Weapon"
    }

-- Small Guns

let pipe-bolt-action : Weapon =
    { ammunition = Some ".308"
    , cost = 30
    , damage = 5
    , description =
        ''
        A makeshift weapon, normally seen in the hands of those who can't make, buy, or
        scavenge anything better, pipe guns are crude and somewhat unreliable, but
        effective enough. Bolt-action pipe guns tend to be chambered for rifle caliber
        rounds, trading rate-of-fire for power and simplicity.

        A Pipe Bolt-Action can accept on each of the following mods. You may not take both
        a Grip and a Stock Mod. Any Stock mods add the word "Rifle" to the end of the
        weapon's name.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.piercing 1
        ]
    , mods =
        [ Mods.Mods.s-barrel-finned
        , Mods.Mods.s-barrel-long
        , Mods.Mods.s-barrel-ported
        , Mods.Mods.s-barrel-snubnose -- TODO: Confirm that "Stub Barrel" isn't intended to refer to something else here...
        , Mods.Mods.s-grip-sharpshooters
        , Mods.Mods.s-muzzle-bayonet
        , Mods.Mods.s-muzzle-brake
        , Mods.Mods.s-muzzle-compensator
        , Mods.Mods.s-muzzle-suppressor
        , Mods.Mods.s-receiver-38cal
        , Mods.Mods.s-receiver-50cal
        , Mods.Mods.s-receiver-calibrated
        , Mods.Mods.s-receiver-hardened
        , Mods.Mods.s-receiver-powerful
        , Mods.Mods.s-sight-long
        , Mods.Mods.s-sight-long-nv
        , Mods.Mods.s-sight-recon
        , Mods.Mods.s-sight-reflex
        , Mods.Mods.s-sight-short
        , Mods.Mods.s-sight-short-nv
        , Mods.Mods.s-stock-full -- TODO: Double check that this is what "standard stock" refers to...
        , Mods.Mods.s-stock-marksmans
        , Mods.Mods.s-stock-recoil-compensating
        ]
    , name = "Pipe Bolt-Action"
    , qualities =
        [ DEQ.Qualities.unreliable
        ]
    , range = Some 0
    , rarity = 0
    , rate = Some 0
    , weight = 3
    , wtype = "Small Guns"
    }

let pipe-gun : Weapon =
    { ammunition = Some ".38"
    , cost = 30
    , damage = 3
    , description =
        ''
        A crude, homemade weapon, normally seen in the hands of those who can't make,
        buy, or scavenge anything better, pipe guns are crude and somewhat unreliable,
        but effective enough. The most common form of pipe gun is a simple little
        semi-auto handgun, chambered for the common .38 round, but enterprising (or
        desperate) souls have been known to modify these guns for automatic fire or
        precision sniping.

        A Pipe Gun can accept one each of the following mods. Any Stock mods replace
        "Gun" with "Rifle" in the weapon's name.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.s-barrel-finned
        , Mods.Mods.s-barrel-long
        , Mods.Mods.s-barrel-ported
        , Mods.Mods.s-grip-sharpshooters
        , Mods.Mods.s-muzzle-bayonet
        , Mods.Mods.s-muzzle-brake
        , Mods.Mods.s-muzzle-compensator
        , Mods.Mods.s-muzzle-suppressor
        , Mods.Mods.s-receiver-45cal
        , Mods.Mods.s-receiver-automatic
        , Mods.Mods.s-receiver-calibrated
        , Mods.Mods.s-receiver-hair-trigger
        , Mods.Mods.s-receiver-hardened
        , Mods.Mods.s-receiver-powerful
        , Mods.Mods.s-sight-long
        , Mods.Mods.s-sight-long-nv
        , Mods.Mods.s-sight-recon
        , Mods.Mods.s-sight-reflex
        , Mods.Mods.s-sight-short
        , Mods.Mods.s-sight-short-nv
        , Mods.Mods.s-stock-full -- TODO: Double check that this is what "standard stock" refers to...
        , Mods.Mods.s-stock-marksmans
        , Mods.Mods.s-stock-recoil-compensating
        ]
    , name = "Pipe Gun"
    , qualities =
        [ DEQ.Qualities.close-quarters
        , DEQ.Qualities.unreliable
        ]
    , range = Some 0
    , rarity = 0
    , rate = Some 2
    , weight = 2
    , wtype = "Small Guns"
    }

let pipe-revolver : Weapon =
    { ammunition = Some ".45"
    , cost = 25
    , damage = 4
    , description =
        ''
        Like the other forms of pipe gun, the pipe revolver is a makeshift or homemade
        gun, in this case modelled after a revolver. It strikes a balance between the
        stopping power of the pipe bolt-action, and the fire rate of the pipe gun, and
        its improvised nature makes it similarly suited for modification.

        A Pipe Revolver can accept one each of the following mods. Any Stock mods add
        "Rifle" to the end of the weapon's name.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.s-barrel-finned
        , Mods.Mods.s-barrel-long
        , Mods.Mods.s-barrel-ported
        , Mods.Mods.s-grip-sharpshooters
        , Mods.Mods.s-muzzle-bayonet
        , Mods.Mods.s-muzzle-brake
        , Mods.Mods.s-muzzle-compensator
        , Mods.Mods.s-muzzle-suppressor
        , Mods.Mods.s-receiver-308cal
        , Mods.Mods.s-receiver-38cal
        , Mods.Mods.s-receiver-calibrated
        , Mods.Mods.s-receiver-hardened
        , Mods.Mods.s-receiver-powerful
        ]
    , name = "Pipe Revolver"
    , qualities =
        [ DEQ.Qualities.close-quarters
        , DEQ.Qualities.unreliable
        ]
    , range = Some 0
    , rarity = 0
    , rate = Some 1
    , weight = 4
    , wtype = "Small Guns"
    }

let pistol-44cal : Weapon =
    { ammunition = Some ".44 Magnum"
    , cost = 99
    , damage = 6
    , description =
        ''
        The .44 pistol is a double-action revolver, chambered to use the .44 Magnum
        cartridge. It's a powerful gun, though not as customizable as other handguns.
        Due to their simple, rugged design, they're prized by their owners, but not
        so uncommon as to be truly rare.

        A .44 Pistol can accept one each of the following mods:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.vicious
        ]
    , mods =
        [ Mods.Mods.s-barrel-bull
        , Mods.Mods.s-barrel-snubnose
        , Mods.Mods.s-grip-comfort
        , Mods.Mods.s-receiver-advanced
        , Mods.Mods.s-receiver-hardened
        , Mods.Mods.s-receiver-powerful
        , Mods.Mods.s-sight-recon
        , Mods.Mods.s-sight-reflex
        , Mods.Mods.s-sight-short
        ]
    , name = ".44 Pistol"
    , qualities =
        [ DEQ.Qualities.close-quarters
        ]
    , range = Some 0
    , rarity = 2
    , rate = Some 1
    , weight = 4
    , wtype = "Small Guns"
    }

let pistol-10mm : Weapon =
    { ammunition = Some "10mm"
    , cost = 50
    , damage = 4
    , description =
        ''
        Small, dependable, reasonably powerful, and widely available, the 10mm pistol
        has been a staple of wasteland combat since the bombs first fell. Its
        versatility and potential for customization mean that a well-looked-after 10mm
        pistol will keep its owner safe for a long time.

        A 10mm Pistol can accept one each of the following mods:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.s-barrel-long
        , Mods.Mods.s-barrel-ported
        , Mods.Mods.s-grip-comfort
        , Mods.Mods.s-grip-sharpshooters
        , Mods.Mods.s-muzzle-compensator
        , Mods.Mods.s-muzzle-suppressor
        , Mods.Mods.s-receiver-advanced
        , Mods.Mods.s-receiver-automatic
        , Mods.Mods.s-receiver-calibrated
        , Mods.Mods.s-receiver-hair-trigger
        , Mods.Mods.s-receiver-hardened
        , Mods.Mods.s-receiver-powerful
        , Mods.Mods.s-sight-reflex
        , Mods.Mods.s-sight-recon
        ]
    , name = "10mm Pistol"
    , qualities =
        [ DEQ.Qualities.close-quarters
        , DEQ.Qualities.reliable
        ]
    , range = Some 0
    , rarity = 1
    , rate = Some 2
    , weight = 4
    , wtype = "Small Guns"
    }

let pistol-flare : Weapon =
    { ammunition = Some "Flare"
    , cost = 50
    , damage = 3
    , description =
        ''
        A Flare gun is a break-action single-shot device not really designed to inflict
        damage. Rather, it shoots signal flares which are used by many factions to signal
        for assistance by firing them high into the air.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [] : List Mods.Mod
    , name = "Flare Gun"
    , qualities =
        [ DEQ.Qualities.reliable
        ]
    , range = Some 1
    , rarity = 1
    , rate = Some 0
    , weight = 2
    , wtype = "Small Guns"
    }

let rifle-assault : Weapon =
    { ammunition = Some "5.56mm Rifle"
    , cost = 144
    , damage = 5
    , description =
        ''
        This gas-operated rifle is a common sight across the wasteland, and well-liked
        because of the low recoil and reasonable accuracy, and the ease of modifications.
        Most versions found are only capable of semi-automatic fire, but the weapon is
        easily modified for automatic fire.

        An Assault Rifle can accept one each of the following mods:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.burst
        ]
    , mods =
        [ Mods.Mods.s-barrel-long
        , Mods.Mods.s-barrel-ported
        , Mods.Mods.s-barrel-vented
        , Mods.Mods.s-muzzle-compensator
        , Mods.Mods.s-muzzle-suppressor
        , Mods.Mods.s-receiver-advanced
        , Mods.Mods.s-receiver-automatic
        , Mods.Mods.s-receiver-calibrated
        , Mods.Mods.s-receiver-hair-trigger
        , Mods.Mods.s-receiver-hardened
        , Mods.Mods.s-receiver-powerful
        , Mods.Mods.s-sight-long
        , Mods.Mods.s-sight-long-nv
        , Mods.Mods.s-sight-recon
        , Mods.Mods.s-sight-reflex
        , Mods.Mods.s-sight-short
        , Mods.Mods.s-sight-short-nv
        , Mods.Mods.s-stock-full
        , Mods.Mods.s-stock-marksmans
        , Mods.Mods.s-stock-recoil-compensating
        ]
    , name = "Assault Rifle"
    , qualities =
        [ DEQ.Qualities.two-handed
        ]
    , range = Some 1
    , rarity = 2
    , rate = Some 2
    , weight = 13
    , wtype = "Small Guns"
    }

let rifle-combat : Weapon =
    { ammunition = Some ".45"
    , cost = 117
    , damage = 5
    , description =
        ''
        A pre-War weapon found across the wastelands and used in large numbers by various
        aggressive factions. Combat Rifles are a rugged and adaptable design, able to
        be modified to fill a variety of different battlefield roles.

        A Combat Rifle can accept one each of the following mods:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.s-barrel-long
        , Mods.Mods.s-barrel-ported
        , Mods.Mods.s-barrel-vented
        , Mods.Mods.s-muzzle-bayonet
        , Mods.Mods.s-muzzle-compensator
        , Mods.Mods.s-muzzle-suppressor
        , Mods.Mods.s-receiver-308cal
        , Mods.Mods.s-receiver-38cal
        , Mods.Mods.s-receiver-advanced
        , Mods.Mods.s-receiver-automatic
        , Mods.Mods.s-receiver-calibrated
        , Mods.Mods.s-receiver-hair-trigger
        , Mods.Mods.s-receiver-hardened
        , Mods.Mods.s-receiver-powerful
        , Mods.Mods.s-sight-long
        , Mods.Mods.s-sight-long-nv
        , Mods.Mods.s-sight-recon
        , Mods.Mods.s-sight-reflex
        , Mods.Mods.s-sight-short
        , Mods.Mods.s-sight-short-nv
        , Mods.Mods.s-stock-full
        , Mods.Mods.s-stock-marksmans
        , Mods.Mods.s-stock-recoil-compensating
        ]
    , name = "Combat Rifle"
    , qualities =
        [ DEQ.Qualities.two-handed
        ]
    , range = Some 1
    , rarity = 2
    , rate = Some 2
    , weight = 11
    , wtype = "Small Guns"
    }

let rifle-gauss : Weapon =
    { ammunition = Some "2mm EC"
    , cost = 228
    , damage = 10
    , description =
        ''
        The gauss rifle uses magnetic induction to propel a projectile at incredible,
        devastating speeds. Each shot can be "charged" for maximum damage by holding the
        trigger for a moment before releasing to fire.

        A Gauss Rifle can accept one each of the following mods:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.piercing 1
        ]
    , mods = -- TODO: what is a shielded barrel??? what are full capacitors??? what are capacitor boosting coils???
        [ Mods.Mods.s-muzzle-suppressor
        , Mods.Mods.s-sight-long
        , Mods.Mods.s-sight-long-nv
        , Mods.Mods.s-sight-recon
        , Mods.Mods.s-sight-reflex
        , Mods.Mods.s-sight-short
        , Mods.Mods.s-sight-short-nv
        , Mods.Mods.s-stock-recoil-compensating
        ]
    , name = "Gauss Rifle"
    , qualities =
        [ DEQ.Qualities.two-handed
        ]
    , range = Some 2
    , rarity = 4
    , rate = Some 1
    , weight = 16
    , wtype = "Small Guns"
    }

let rifle-hunting : Weapon =
    { ammunition = Some ".308"
    , cost = 55
    , damage = 6
    , description =
        ''
        A bolt-action rifle commonly found in the hands of super mutants and raiders. The
        simple design made them common amongst hunters and survivalists before the Great
        War, and they're endlessly customizable. It seems fanciful to suggest that the
        simple short-barreled slug-thrower carried by a super mutant could be turned into
        a precision sniper's rifle, yet that describes the hunting rifle perfectly.

        A Hunting Rifle can accept one each of the following mods:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.piercing 1
        ]
    , mods = --TODO: What is a Tuned receiver?
        [ Mods.Mods.s-barrel-long
        , Mods.Mods.s-barrel-ported
        , Mods.Mods.s-barrel-vented
        , Mods.Mods.s-muzzle-bayonet
        , Mods.Mods.s-muzzle-suppressor
        , Mods.Mods.s-receiver-38cal
        , Mods.Mods.s-receiver-50cal
        , Mods.Mods.s-receiver-calibrated
        , Mods.Mods.s-receiver-hardened
        , Mods.Mods.s-receiver-powerful
        , Mods.Mods.s-sight-long
        , Mods.Mods.s-sight-long-nv
        , Mods.Mods.s-sight-recon
        , Mods.Mods.s-sight-reflex
        , Mods.Mods.s-sight-short
        , Mods.Mods.s-sight-short-nv
        , Mods.Mods.s-stock-full
        , Mods.Mods.s-stock-marksmans
        ,
        ]
    , name = "Hunting Rifle"
    , qualities =
        [ DEQ.Qualities.two-handed
        ]
    , range = Some 1
    , rarity = 2
    , rate = Some 0
    , weight = 10
    , wtype = "Small Guns"
    }

let rifle-railway : Weapon =
    { ammunition = Some "Railway Spike"
    , cost = 290
    , damage = 10
    , description =
        ''
        A makeshift weapon created by enterprising engineers in the wastelands, the
        railway rifle uses high-pressure steam to propel railway spikes at speeds
        sufficient to pierce and impale a wide range of targets.

        A Railway Rifle can accept on each of the following mods:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.breaking
        ]
    , mods =
        [ Mods.Mods.s-barrel-long
        , Mods.Mods.s-muzzle-bayonet
        , Mods.Mods.s-receiver-automatic-piston
        , Mods.Mods.s-sight-long
        , Mods.Mods.s-sight-long-nv
        , Mods.Mods.s-sight-recon
        , Mods.Mods.s-sight-reflex
        , Mods.Mods.s-sight-short
        , Mods.Mods.s-sight-short-nv
        , Mods.Mods.s-stock-recoil-compensating
        ]
    , name = "Railway Rifle"
    , qualities =
        [ DEQ.Qualities.debilitating
        , DEQ.Qualities.two-handed
        , DEQ.Qualities.unreliable
        ]
    , range = Some 1
    , rarity = 4
    , rate = Some 0
    , weight = 14
    , wtype = "Small Guns"
    }

let shotgun-combat : Weapon =
    { ammunition = Some "Shotgun Shells"
    , cost = 87
    , damage = 5
    , description =
        ''
        Combat shotguns are shotguns intended for an offensive role, typically by a
        military force; they're ideal for close-range fighting in tunnels, within
        buildings, or through the ruined streets of old cities.

        A Combat Shotgun can accept one each of the following mods:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.spread
        ]
    , mods =
        [ Mods.Mods.s-barrel-long
        , Mods.Mods.s-barrel-ported
        , Mods.Mods.s-muzzle-bayonet
        , Mods.Mods.s-muzzle-brake
        , Mods.Mods.s-muzzle-compensator
        , Mods.Mods.s-muzzle-suppressor
        , Mods.Mods.s-receiver-advanced
        , Mods.Mods.s-receiver-automatic
        , Mods.Mods.s-receiver-calibrated
        , Mods.Mods.s-receiver-hair-trigger
        , Mods.Mods.s-receiver-hardened
        , Mods.Mods.s-receiver-powerful
        , Mods.Mods.s-sight-long
        , Mods.Mods.s-sight-long-nv
        , Mods.Mods.s-sight-recon
        , Mods.Mods.s-sight-reflex
        , Mods.Mods.s-sight-short
        , Mods.Mods.s-sight-short-nv
        , Mods.Mods.s-stock-full
        , Mods.Mods.s-stock-marksmans
        , Mods.Mods.s-stock-recoil-compensating
        ]
    , name = "Combat Shotgun"
    , qualities =
        [ DEQ.Qualities.inaccurate
        , DEQ.Qualities.two-handed
        ]
    , range = Some 0
    , rarity = 2
    , rate = Some 2
    , weight = 11
    , wtype = "Small Guns"
    }

let shotgun-double-barrel : Weapon =
    { ammunition = Some "Shotgun Shells"
    , cost = 39
    , damage = 5
    , description =
        ''
        Before the Great War, double-barrel shotguns were mainly used for hunting and
        home defense, and this has changed little. They are break-action and come in an
        either a side-by-side or over-and-under barrel configuration. Simple to make and
        maintain, these shotguns are seen frequently in the Commonwealth and the rest of
        the wasteland.

        A Double-Barrel Shotgun can accept one each of the following mods:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.spread
        , DEQ.Effects.vicious
        ]
    , mods =
        [ Mods.Mods.s-barrel-long
        , Mods.Mods.s-barrel-sawed-off
        , Mods.Mods.s-muzzle-brake
        , Mods.Mods.s-receiver-advanced
        , Mods.Mods.s-receiver-hair-trigger
        , Mods.Mods.s-receiver-hardened
        , Mods.Mods.s-receiver-powerful
        , Mods.Mods.s-sight-reflex
        , Mods.Mods.s-stock-full
        ]
    , name = "Double-Barrel Shotgun"
    , qualities =
        [ DEQ.Qualities.inaccurate
        , DEQ.Qualities.two-handed
        ]
    , range = Some 0
    , rarity = 1
    , rate = Some 0
    , weight = 9
    , wtype = "Small Guns"
    }

let submachine-gun : Weapon =
    { ammunition = Some ".45"
    , cost = 109
    , damage = 3
    , description =
        ''
        These submachine guns were an old design, even before the War, and while they're
        hideously inaccurate, their rate-of-fire means you don't need to be accurate.

        A Submachine Gun can accept one each of the following mods:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.burst
        ]
    , mods =
        [ Mods.Mods.s-barrel-snubnose --TODO: check that this is what's meant by "short barrel"
        , Mods.Mods.s-muzzle-brake
        , Mods.Mods.s-muzzle-compensator
        , Mods.Mods.s-muzzle-suppressor
        , Mods.Mods.s-receiver-automatic --TODO: check that this is what's meant by "rapid receiver"
        , Mods.Mods.s-receiver-hardened --TODO: whats an armor piercing receiver?
        , Mods.Mods.s-receiver-powerful
        , Mods.Mods.s-sight-reflex
        , Mods.Mods.s-stock-full
        , Mods.Mods.s-stock-recoil-compensating
        ]
    , name = "Submachine Gun"
    , qualities =
        [ DEQ.Qualities.inaccurate
        , DEQ.Qualities.two-handed
        ]
    , range = Some 0
    , rarity = 1
    , rate = Some 3
    , weight = 12
    , wtype = "Small Guns"
    }

let syringer : Weapon =
    { ammunition = Some "Syringer ammo"
    , cost = 132
    , damage = 3
    , description =
        ''
        A makeshift weapon using air pressure to propel customized syringes at a target.
        This inflicts little damage by itself, but the syringes can inflict a wide range
        of debilitating DEQ.Effects.

        A Syringer can accept one each of the following mods:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.s-barrel-long
        , Mods.Mods.s-barrel-snubnose --TODO: check that this is what is meant by "stub barrel"
        , Mods.Mods.s-sight-long
        , Mods.Mods.s-sight-long-nv
        , Mods.Mods.s-sight-recon
        , Mods.Mods.s-sight-reflex
        , Mods.Mods.s-sight-short
        , Mods.Mods.s-sight-short-nv
        , Mods.Mods.s-stock-marksmans
        , Mods.Mods.s-stock-recoil-compensating
        ]
    , name = "Syringer"
    , qualities =
        [ DEQ.Qualities.two-handed
        ]
    , range = Some 1
    , rarity = 2
    , rate = Some 0
    , weight = 6
    , wtype = "Small Guns"
    }

-- Weapon Defs --

-- Explosives

let baseball-grenade : Weapon =
    { ammunition = None Text
    , cost = 40
    , damage = 5
    , description =
        ''
        Little more than a hollowed-out baseball filled with an improved explosive
        mixture, these grenades are crude, low-tech, and easy to make, dealing a
        moderate amount of damage to those within the blast.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [] : List Mods.Mod
    , name = "Baseball Grenade"
    , qualities =
        [ DEQ.Qualities.blast
        , DEQ.Qualities.thrown "M"
        ]
    , range = None Natural
    , rarity = 1
    , rate = None Natural
    , weight = 1
    , wtype = "Explosive"
    }

let bottlecap-mine : Weapon =
    { ammunition = None Text
    , cost = 75
    , damage = 6
    , description =
        ''
        The Bottlecap mine is crudely put together, yet a formidable explosive.
        Bottlecap mines are constructed from a simple container such as a lunchbox,
        filled with bottle caps and a crude explosive mixture, and fitted with a
        sensor and a detonator. The explosive force hurls the caps over a wide area
        at such a speed that they are essentially shrapnel.

        **Special**: when constructing a Bottlecap mine, you may add extra bottlecaps;
        Every 10 caps added increases the damage by +1. When the mine detonates, roll
        the damage rating a second time: the total rolled is the number of intact caps
        found in the zone where the mine detonated.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [] : List Mods.Mod
    , name = "Bottlecap Mine"
    , qualities =
        [ DEQ.Qualities.blast
        , DEQ.Qualities.mine
        ]
    , range = None Natural
    , rarity = 2
    , rate = None Natural
    , weight = 1
    , wtype = "Explosive"
    }

let frag-grenade : Weapon =
    { ammunition = None Text
    , cost = 50
    , damage = 6
    , description =
        ''
        A basic fragmentation grenade, consisting of an explosive core within a metal
        casing designed to shatter into shrapnel upon detonation. They're simple and
        highly effective. Just remember to throw the grenade, and not the pin.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [] : List Mods.Mod
    , name = "Frag Grenade"
    , qualities =
        [ DEQ.Qualities.blast
        , DEQ.Qualities.thrown "M"
        ]
    , range = None Natural
    , rarity = 2
    , rate = None Natural
    , weight = 0
    , wtype = "Explosive"
    }

let frag-mine : Weapon =
    { ammunition = None Text
    , cost = 50
    , damage = 6
    , description =
        ''
        Similar in design and construction to a frag grenade, a fragmentation mine is
        instead placed on the ground and uses a pressure plate or proximity sensor to
        trigger a detonation when someone gets too close.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [] : List Mods.Mod
    , name = "Frag Mine"
    , qualities =
        [ DEQ.Qualities.blast
        , DEQ.Qualities.mine
        ]
    , range = None Natural
    , rarity = 2
    , rate = None Natural
    , weight = 1
    , wtype = "Explosive"
    }

let molotov-cocktail : Weapon =
    { ammunition = None Text
    , cost = 20
    , damage = 4
    , description =
        ''
        Consisting of a glass bottle--normally from an alcoholic beverage--containing a
        flammable fuel mixture, a bot of motor oil to help it spread, and an
        alcohol-soaked rag stuffed in the top to act as a wick. When used, the wick
        is ignited and the bottle hurled at the target; the thrown bottle spreads the
        agitated mixture, which quickly ignites and begins to burn anything in the
        vicinity.
        ''
    , dtype = DEQ.DamageTypes.energy
    , effects =
        [ DEQ.Effects.persistent DEQ.DamageTypes.energy
        ]
    , mods =
        [] : List Mods.Mod
    , name = "Molotov Cocktail"
    , qualities =
        [ DEQ.Qualities.blast
        , DEQ.Qualities.thrown "M"
        ]
    , range = None Natural
    , rarity = 1
    , rate = None Natural
    , weight = 1
    , wtype = "Explosive"
    }

let nuka-grenade : Weapon =
    { ammunition = None Text
    , cost = 100
    , damage = 9
    , description =
        ''
        A tiny, handheld nuclear weapon, a Nuka-Grenade is extremely dangerous to use--
        dropping one is ill-advised--but devastatingly effective, comparable to the
        mini-nukes used by the Fat Man launcher.
        ''
    , dtype = DEQ.DamageTypes.energy
    , effects =
        [ DEQ.Effects.breaking
        , DEQ.Effects.radioactive
        , DEQ.Effects.vicious
        ]
    , mods =
        [] : List Mods.Mod
    , name = "Nuka-Grenade"
    , qualities =
        [ DEQ.Qualities.blast
        , DEQ.Qualities.thrown "M"
        ]
    , range = None Natural
    , rarity = 4
    , rate = None Natural
    , weight = 1
    , wtype = "Explosive"
    }

let nuke-mine : Weapon =
    { ammunition = None Text
    , cost = 100
    , damage = 9
    , description =
        ''
        It is a nuclear variant of a landmine. The Nuke mine operates in all respects
        as other mines do although its explosion is more akin to that of a mini-nuke.
        Naturally, this means that they inflict damage from both the explosion and from
        the radiation the blast emits.
        ''
    , dtype = DEQ.DamageTypes.energy
    , effects =
        [ DEQ.Effects.breaking
        , DEQ.Effects.radioactive
        , DEQ.Effects.vicious
        ]
    , mods =
        [] : List Mods.Mod
    , name = "Nuke Mine"
    , qualities =
        [ DEQ.Qualities.blast
        , DEQ.Qualities.mine
        ]
    , range = None Natural
    , rarity = 4
    , rate = None Natural
    , weight = 1
    , wtype = "Explosive"
    }

let plasma-grenade : Weapon =
    { ammunition = None Text
    , cost = 135
    , damage = 9
    , description =
        ''
        A canister containing chemicals which are rapidly transformed into superheated
        ionized gas--or plasma--upon detonation. The blast of a plasma grenade generates
        vast amounts of heat which more than makes up for the lack of shrapnel or
        concussive shockwaves.

        **Special**: Plasma grenades inflict both physical and energy damage. Roll damage
        as normal, and then reduce the total by whichever of the target's damage
        resistances is lower out of Physical or Energy
        ''
    , dtype =
        DEQ.GenericDEQFactory "Physical/Energy" "Deals Physical and Energy, targeting whichever resistance is lowest. Any persistent damage effects inflict Energy damage."
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [] : List Mods.Mod
    , name = "Plasma Grenade"
    , qualities =
        [ DEQ.Qualities.blast
        , DEQ.Qualities.thrown "M"
        ]
    , range = None Natural
    , rarity = 3
    , rate = None Natural
    , weight = 0
    , wtype = "Explosive"
    }

let plasma-mine : Weapon =
    { ammunition = None Text
    , cost = 135
    , damage = 9
    , description =
        ''
        These magnetically sealed mines unleash a blast of energized plasma upon being
        triggered, searing those who tripped the mine's sensors.

        **Special**: Plasma mines inflict both physical and energy damage. Roll damage
        as normal, and then reduce the total by whichever of the target's damage
        resistances is lower out of Physical or Energy
        ''
    , dtype =
        DEQ.GenericDEQFactory "Physical/Energy" "Deals Physical and Energy, targeting whichever resistance is lowest. Any persistent damage effects inflict Energy damage."
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [] : List Mods.Mod
    , name = "Plasma Mine"
    , qualities =
        [ DEQ.Qualities.blast
        , DEQ.Qualities.mine
        ]
    , range = None Natural
    , rarity = 3
    , rate = None Natural
    , weight = 0
    , wtype = "Explosive"
    }

let pulse-grenade : Weapon =
    { ammunition = None Text
    , cost = 100
    , damage = 6
    , description =
        ''
        Pulse grenades emit a burst of electromagnetism which inflicts massive damage
        to machines--such as robots, synths, and Power Armor--within the vicinity,
        while leaving living creatures unharmed.

        **Special**: Pulse grenades only inflict damage to robots, synths, Power Armor
        (though not the wearer within), and other mechanical or technological targets.
        Any computers in the blast are also damaged by the grenade. Against any other
        target, Pulse grenades inflict 0 damage.
        ''
    , dtype = DEQ.DamageTypes.energy
    , effects =
        [ DEQ.Effects.stun
        ]
    , mods =
        [] : List Mods.Mod
    , name = "Pulse Grenade"
    , qualities =
        [ DEQ.Qualities.blast
        , DEQ.Qualities.thrown "M"
        ]
    , range = None Natural
    , rarity = 3
    , rate = None Natural
    , weight = 0
    , wtype = "Explosive"
    }

let pulse-mine : Weapon =
    { ammunition = None Text
    , cost = 100
    , damage = 6
    , description =
        ''
        The mine consists of a powerful EMP device wired to a proximity fuse and then
        secured inside a heavy metal casing. While only mildly harmful to organic
        targets, these mines deal severe damage to robotic enemies, stunning or even
        permanently disabling them.

        **Special**: Pulse mines only inflict damage to robots, synths, Power Armor
        (though not the wearer within), and other mechanical or technological targets.
        Any computers in the blast are also damaged by the grenade. Against any other
        target, Pulse grenades inflict 0 damage.
        ''
    , dtype = DEQ.DamageTypes.energy
    , effects =
        [ DEQ.Effects.stun
        ]
    , mods =
        [] : List Mods.Mod
    , name = "Pulse Mine"
    , qualities =
        [ DEQ.Qualities.blast
        , DEQ.Qualities.mine
        ]
    , range = None Natural
    , rarity = 3
    , rate = None Natural
    , weight = 0
    , wtype = "Explosive"
    }

-- Melee Weapons

let baseball-bat : Weapon =
    { ammunition = None Text
    , cost = 25
    , damage = 4
    , description =
        ''
        Baseball bats were once used in the sport of baseball, considered America's
        number-one pastime during the pre-War era. Two hundred years later, they have
        been re-purposed as crude but effective melee weapons. While most are made a
        single piece of wood-normally ash, but other hardwoods are often used-a few can
        be made of aluminum instead, making for a lighter, harder bat.

        A baseball bat can accept one of the following mods which are
        **installed with the Repair skill**. You may only apply mods to an aluminium
        baseball bat if you have the Blacksmith 1 perk.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.baseball-bat-barbed
        , Mods.Mods.baseball-bat-spiked
        , Mods.Mods.baseball-bat-sharp
        , Mods.Mods.baseball-bat-chain-wrapped
        , Mods.Mods.baseball-bat-bladed
        ]
    , name = "Baseball Bat"
    , qualities =
        [ DEQ.Qualities.two-handed
        ]
    , range = None Natural
    , rarity = 1
    , rate = None Natural
    , weight = 3
    , wtype = "Melee Weapon"
    }


let baseball-bat-aluminum : Weapon =
    { ammunition = None Text
    , cost = 32
    , damage = 5
    , description =
        baseball-bat.description
    , dtype = baseball-bat.dtype
    , effects =
        baseball-bat.effects
    , mods =
        baseball-bat.mods
    , name = "Aluminum Baseball Bat"
    , qualities =
        baseball-bat.qualities
    , range = None Natural
    , rarity = 2
    , rate = None Natural
    , weight = 2
    , wtype = baseball-bat.wtype
    }


let baton : Weapon =
    { ammunition = None Text
    , cost = 15
    , damage = 3
    , description =
        ''
        A collapsible baton of the sort used by police departments and private security
        forces before the Great War. Vault-Tec security within each vault had a stock
        of these, so they remain in common use as a light-weight back-up melee weapon.

        A baton can accept one of the following mods which are
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.baton-electrified
        , Mods.Mods.baton-stun-pack
        ]
    , name = "Baton"
    , qualities =
        [] : List DEQ.GenericDEQ
    , range = None Natural
    , rarity = 1
    , rate = None Natural
    , weight = 2
    , wtype = "Melee Weapon"
    }


let board : Weapon =
    { ammunition = None Text
    , cost = 20
    , damage = 4
    , description =
        ''
        Little more than a long plank of wood with a handle wrapped in duct tape. Boards
        are heavy melee weapons, normally favored by super mutants who have the strength
        to wield them effectively.

        A board can accept one of the following mods which are
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.board-spiked
        , Mods.Mods.board-puncturing
        , Mods.Mods.board-bladed
        ]
    , name = "Board"
    , qualities =
        [ DEQ.Qualities.two-handed
        ]
    , range = None Natural
    , rarity = 0
    , rate = None Natural
    , weight = 3
    , wtype = "Melee Weapon"
    }


let combat-knife : Weapon =
    { ammunition = None Text
    , cost = 25
    , damage = 3
    , description =
        ''
        For both fighting and for utility uses, soldiers-and those who seek to emulated
        soldiers-frequently carry knives. The Combat Knife found across the wastelands
        is normally of the same type as was issued to U.S. Army soldiers during the
        Great War, a single-edged blade with a clipped point. The weapon is lightweight
        and sturdy, and intended for heavy use.

        Combat knives can accept one of the following mods which are
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.piercing 1
        ]
    , mods =
        [ Mods.Mods.combat-knife-serrated-blade
        , Mods.Mods.combat-knife-stealth-blade
        ]
    , name = "Combat Knife"
    , qualities =
        [] : List DEQ.GenericDEQ
    , range = None Natural
    , rarity = 1
    , rate = None Natural
    , weight = 1
    , wtype = "Melee Weapon"
    }


let lead-pipe : Weapon =
    { ammunition = None Text
    , cost = 15
    , damage = 3
    , description =
        ''
        A length of heavy, metal (normally lead) pipe, with a duct-tape wrapped grip at
        one end and a few nuts and bolts attached at the other. Crude, simple, and hurts
        like hell.

        A lead pipe can accept one of the following mods which are
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.lead-pipe-spiked
        , Mods.Mods.lead-pipe-heavy
        ]
    , name = "Lead Pipe"
    , qualities =
        [] : List DEQ.GenericDEQ
    , range = None Natural
    , rarity = 0
    , rate = None Natural
    , weight = 3
    , wtype = "Melee Weapon"
    }


let machete : Weapon =
    { ammunition = None Text
    , cost = 25
    , damage = 3
    , description =
        ''
        A long, sharp knife designed to hack through dense or tough vegetation, the same
        properties that make it a useful tool also make it an effective weapon. While
        some machetes found in the wasteland were purpose-made blades, others are
        makeshift tools, typically made from a sharpened metal blade such as that
        salvaged from a lawnmower, bolted, tied, taped, or otherwise affixed to a simple
        handle.

        Machetes can accept the following mod which is
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.piercing 1
        ]
    , mods =
        [ Mods.Mods.machete-serrated-blade
        ]
    , name = "Machete"
    , qualities =
        [] : List DEQ.GenericDEQ
    , range = None Natural
    , rarity = 1
    , rate = None Natural
    , weight = 2
    , wtype = "Melee Weapon"
    }


let pipe-wrench : Weapon =
    { ammunition = None Text
    , cost = 30
    , damage = 3
    , description =
        ''
        A heavy, metal, adjustable wrench, long enough to be used as a makeshift melee
        weapon. The adjustable jaws are often used to hold attachments, allowing the
        wrench to be modded extremely easily.

        A pipe wrench can accept one of the following mods which are
        **installed with the Repair skill**. Reduce the difficulty of any test to install
        a mod onto a pipe wrench by 1.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.pipe-wrench-hooked
        , Mods.Mods.pipe-wrench-heavy
        , Mods.Mods.pipe-wrench-puncturing
        , Mods.Mods.pipe-wrench-extra-heavy
        ]
    , name = "Pipe Wrench"
    , qualities =
        [] : List DEQ.GenericDEQ
    , range = None Natural
    , rarity = 0
    , rate = None Natural
    , weight = 2
    , wtype = "Melee Weapon"
    }


let pool-cue : Weapon =
    { ammunition = None Text
    , cost = 10
    , damage = 3
    , description =
        ''
        A long, slender piece of wood used in games like pool. Useful as a melee weapon
        if there's nothing to hand with more heft.

        A pool cue can accept one of the following mods which are
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.pool-cue-barbed
        , Mods.Mods.pool-cue-sharp
        ]
    , name = "Pool Cue"
    , qualities =
        [ DEQ.Qualities.two-handed
        ]
    , range = None Natural
    , rarity = 0
    , rate = None Natural
    , weight = 1
    , wtype = "Melee Weapon"
    }


let ripper : Weapon =
    { ammunition = None Text
    , cost = 50
    , damage = 4
    , description =
        ''
        The Ripper is a small, handheld, militarized chainsaw that saw extensive use in
        the armed conflicts before the Great War, with a commercial-grade, scaled-down
        version made available for the general market. It can saw through both flesh
        and metal alike with ease.

        Rippers can accept one of the following mods which are
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.vicious
        ]
    , mods =
        [ Mods.Mods.ripper-curved-blade
        , Mods.Mods.ripper-extended-blade
        ]
    , name = "Ripper"
    , qualities =
        [] : List DEQ.GenericDEQ
    , range = None Natural
    , rarity = 2
    , rate = None Natural
    , weight = 6
    , wtype = "Melee Weapon"
    }


let rolling-pin : Weapon =
    { ammunition = None Text
    , cost = 10
    , damage = 3
    , description =
        ''
        A sturdy cylinder of wood found in kitchens everywhere. Heavy enough to deter
        an assortment of attackers if you haven't got something more dangerous.

        A rolling pin can accept one of the following mods which are
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.rolling-pin-spiked
        , Mods.Mods.rolling-pin-sharp
        ]
    , name = "Rolling Pin"
    , qualities =
        [] : List DEQ.GenericDEQ
    , range = None Natural
    , rarity = 0
    , rate = None Natural
    , weight = 1
    , wtype = "Melee Weapon"
    }


let shishkebab : Weapon =
    { ammunition = None Text
    , cost = 200
    , damage = 5
    , description =
        ''
        A makeshift "flaming sword", the Shishkebab is found in various forms across the
        wastelands, with some versions being built around actual swords, while others are
        constructed from a variety of components. They all have a few common elements,
        however: some form of fuel tank or canister (either affixed to the weapon itself,
        or worn by the wielder and connected by a hose), a pilot light, and a triggering
        mechanism normally salvaged from a motorcycle. During use, the fuel is released
        along the blade and ignited by the pilot light, wreathing the blade in flames.

        A shishkebab can accept the following mod which is
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.energy
    , effects =
        [ DEQ.Effects.piercing 1
        ]
    , mods =
        [ Mods.Mods.shishkebab-extra-flame-jets
        ]
    , name = "Shishkebab"
    , qualities =
        [ DEQ.Qualities.parry
        ]
    , range = None Natural
    , rarity = 3
    , rate = None Natural
    , weight = 3
    , wtype = "Melee Weapon"
    }


let sledgehammer : Weapon =
    { ammunition = None Text
    , cost = 40
    , damage = 5
    , description =
        ''
        Sledgehammers are primitive weapons, used as tools before the Great War, for
        breaking ground or demolishing things. Although not usually considered as a
        powerful weapon, a strong person-or a super mutant-can wield one of these
        like a club, taking full advantage of the hammer's weight.

        A sledgehammer can accept one of the following mods which are
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.sledgehammer-puncturing
        , Mods.Mods.sledgehammer-heavy
        ]
    , name = "Sledgehammer"
    , qualities =
        [] : List DEQ.GenericDEQ -- TODO: Check for errata relating to sledgehammers, this should almost definitely be a two-handed weapon
    , range = None Natural
    , rarity = 2
    , rate = None Natural
    , weight = 12
    , wtype = "Melee Weapon"
    }


let super-sledge : Weapon =
    { ammunition = None Text
    , cost = 180
    , damage = 6
    , description =
        ''
        Super Sledgehammers are powered variants of the regular sledgehammer. Older,
        pre-War designs were outfitted with kinetic storage devices to increase the
        impact force, while post-War ones mount a small rocket motor within the hammer's
        head to accelerate the swing and deliver a heavier blow.

        A Super Sledgehammer can accept one of the following mods which are
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.breaking
        ]
    , mods =
        [ Mods.Mods.super-sledge-heating-coil
        , Mods.Mods.super-sledge-stun-pack
        ]
    , name = "Super Sledge"
    , qualities =
        [ DEQ.Qualities.two-handed
        ]
    , range = None Natural
    , rarity = 3
    , rate = None Natural
    , weight = 20
    , wtype = "Melee Weapon"
    }


let switchblade : Weapon =
    { ammunition = None Text
    , cost = 20
    , damage = 2
    , description =
        ''
        The Switchblade is a commonly found melee weapon. Its small size means that it is
        easily smuggled into restricted areas and is easy to handle. It doesn't do as
        much damage as larger knives or blades, however.

        Switchblades can accept the following mod which is
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.piercing 1
        ]
    , mods =
        [ Mods.Mods.switchblade-serrated-blade
        ]
    , name = "Switchblade"
    , qualities =
        [ DEQ.Qualities.concealed
        ]
    , range = None Natural
    , rarity = 0
    , rate = None Natural
    , weight = 1
    , wtype = "Melee Weapon"
    }


let sword : Weapon =
    { ammunition = None Text
    , cost = 50
    , damage = 4
    , description =
        ''
        Swords have been in use for almost as long as wars have been fought and remained
        widespread as symbols of status and prestige amongst soldiers-particularly
        officers-across the world right up until the Great War. many of the swords found
        in the wastelands are those used by Chinese officers during the Great War, often
        taken from the battlefields as a trophy by American troops. Others are museum
        pieces or replicas from private collections, with many resembling the cavalry
        sabers used during the American Revolution.

        Swords can accept one of the following mods which are
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.piercing 1
        ]
    , mods =
        [ Mods.Mods.sword-electrified-blade
        , Mods.Mods.sword-electrified-serrated-blade
        , Mods.Mods.sword-serrated-blade
        , Mods.Mods.sword-stun-pack
        ]
    , name = "Sword"
    , qualities =
        [ DEQ.Qualities.parry
        ]
    , range = None Natural
    , rarity = 2
    , rate = None Natural
    , weight = 3
    , wtype = "Melee Weapon"
    }


let tire-iron : Weapon =
    { ammunition = None Text
    , cost = 25
    , damage = 3
    , description =
        ''
        A simple L-shaped tool found commonly in cars and garages; tire irons make for
        handy weapons when not being used to change the tires on a car.

        A tire iron can accept the following mod which is
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.tire-iron-bladed
        ]
    , name = "Tire Iron"
    , qualities =
        [] : List DEQ.GenericDEQ
    , range = None Natural
    , rarity = 1
    , rate = None Natural
    , weight = 2
    , wtype = "Melee Weapon"
    }


let walking-cane : Weapon =
    { ammunition = None Text
    , cost = 10
    , damage = 3
    , description =
        ''
        A simply stick used to support those who can't walk so well, a walking cane is
        sturdy enough to be an impromptu weapon if you've not got any other options.

        A walking cane can accept one of the following mods which are
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.walking-cane-barbed
        , Mods.Mods.walking-cane-spiked
        ]
    , name = "Walking Cane"
    , qualities =
        [] : List DEQ.GenericDEQ
    , range = None Natural
    , rarity = 0
    , rate = None Natural
    , weight = 2
    , wtype = "Melee Weapon"
    }


let boxing-glove : Weapon =
    { ammunition = None Text
    , cost = 10
    , damage = 3
    , description =
        ''
        A large, leather mitt, used to protect the hands and wrists while punching.
        While originally used during sporting matches, a skilled fighter can use these to
        deliver solid blows to enemies in less organized fights, particularly if they've
        modified the glove in some way.

        A boxing glove can accept one of the following mods which are
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.stun
        ]
    , mods =
        [ Mods.Mods.boxing-glove-spiked
        , Mods.Mods.boxing-glove-puncturing
        , Mods.Mods.boxing-glove-lead-lining
        ]
    , name = "Boxing Glove"
    , qualities =
        [] : List DEQ.GenericDEQ
    , range = None Natural
    , rarity = 1
    , rate = None Natural
    , weight = 1
    , wtype = "Unarmed"
    }


let deathclaw-gauntlet : Weapon =
    { ammunition = None Text
    , cost = 75
    , damage = 5
    , description =
        ''
        Made from the severed fingers and talons of a deathclaw and mounted on a cuff
        which fits over the wrist, these allow the wearer to strike with some of the
        lethal force of the claws' previous owner.

        A Deathclaw Gauntlet can accept the following mod which is
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.piercing 1
        ]
    , mods =
        [ Mods.Mods.deathclaw-gauntlet-extra-claw
        ]
    , name = "Deathclaw Gauntlet"
    , qualities =
        [] : List DEQ.GenericDEQ
    , range = None Natural
    , rarity = 3
    , rate = None Natural
    , weight = 10
    , wtype = "Unarmed"
    }


let knuckles : Weapon =
    { ammunition = None Text
    , cost = 10
    , damage = 3
    , description =
        ''
        Developed in antiquity, this weapon is nevertheless quite prevalent in the
        post-Great War wastelands. Brass Knuckles, or Knuckledusters, take the form of
        four linked metal rings in a shallow convex formation with the bumper attached
        to the concave face. The rings are slipped over a combatant's fingers with the
        bumper pressed into the palm of the hand. When a punch is thrown, the
        Brass Knuckles take most of the impact impulse, transferring much of the punch's
        kinetic energy to the target by way of the harder, smaller impact surface of the
        metal knuckles. This drastically increases the likelihood of serious tissue
        disruption and bone damage.

        Knuckles can accept one of the following mods which are
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [ Mods.Mods.knuckles-sharp
        , Mods.Mods.knuckles-spiked
        , Mods.Mods.knuckles-puncturing
        , Mods.Mods.knuckles-bladed
        ]
    , name = "Knuckles"
    , qualities =
        [ DEQ.Qualities.concealed
        ]
    , range = None Natural
    , rarity = 1
    , rate = None Natural
    , weight = 0
    , wtype = "Unarmed"
    }


let power-fist : Weapon =
    { ammunition = None Text
    , cost = 100
    , damage = 4
    , description =
        ''
        A Power Fist is a reinforced gauntlet or vambrace which mounts a heavy-duty
        pneumatic ram over the knuckles. Designed for use by demolition crews, it has
        also seen military use for breaching fortifications and clearing enemy
        barricades... and enemies.

        A Power Fist can accept the following mod which is
        **installed with the Repair skill**:
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.stun
        ]
    , mods =
        [ Mods.Mods.power-fist-puncturing
        , Mods.Mods.power-fist-heating-coil
        ]
    , name = "Power Fist"
    , qualities =
        [] : List DEQ.GenericDEQ
    , range = None Natural
    , rarity = 2
    , rate = None Natural
    , weight = 4
    , wtype = "Unarmed"
    }


let gun-bash-1h : Weapon =
    { ammunition = None Text
    , cost = Utils.Values.Inherited
    , damage = 2
    , description =
        ''
        Bash an enemy with your one-handed weapon.

        **Special**: The cost, qualities, rarity, and weight of this attack are all inherited from the weapon you are bashing with.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.stun
        ]
    , mods =
        [] : List Mods.Mod
    , name = "One-Handed Gun Bash"
    , qualities =
        [ DEQ.GenericDEQFactory "Inherited" "The cost, qualities, rarity, and weight of this attack are all inherited from the weapon you are bashing with."
        ]
    , range = None Natural
    , rarity = Utils.Values.Inherited
    , rate = None Natural
    , weight = Utils.Values.Inherited
    , wtype = "Melee Weapon"
    }


let gun-bash-2h : Weapon =
    { ammunition = None Text
    , cost = Utils.Values.Inherited
    , damage = 3
    , description =
        ''
        Bash an enemy with your two-handed weapon.

        **Special**: The cost, qualities, rarity, and weight of this attack are all inherited from the weapon you are bashing with.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.stun
        ]
    , mods =
        [] : List Mods.Mod
    , name = "Two-Handed Gun Bash"
    , qualities =
        [ DEQ.GenericDEQFactory "Inherited" "The cost, qualities, rarity, and weight of this attack are all inherited from the weapon you are bashing with."
        ]
    , range = None Natural
    , rarity = Utils.Values.Inherited
    , rate = None Natural
    , weight = Utils.Values.Inherited
    , wtype = "Melee Weapon"
    }


let rock : Weapon =
    { ammunition = None Text
    , cost = Utils.Values.NotApplicable
    , damage = 2
    , description =
        ''
        You pick up any old rock off the ground, and toss it.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.vicious
        ]
    , mods =
        [] : List Mods.Mod
    , name = "Handy Rock"
    , qualities =
        [ DEQ.Qualities.thrown "C"
        ]
    , range = None Natural
    , rarity = Utils.Values.NotApplicable
    , rate = None Natural
    , weight = 1
    , wtype = "Unarmed"
    }


let unarmed : Weapon =
    { ammunition = None Text
    , cost = Utils.Values.NotApplicable
    , damage = 2
    , description =
        ''
        Strike an enemy without aid from any weapon or gun.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [] : List DEQ.GenericDEQ
    , mods =
        [] : List Mods.Mod
    , name = "Unarmed Strike"
    , qualities =
        [] : List DEQ.GenericDEQ
    , range = None Natural
    , rarity = Utils.Values.NotApplicable
    , rate = None Natural
    , weight = Utils.Values.NotApplicable
    , wtype = "Unarmed"
    }

-- Throwing Weapons

let javelin : Weapon =
    { ammunition = None Text
    , cost = 10
    , damage = 4
    , description =
        ''
        A short spear, not typically sturdy enough for close-in fighting, a javelin can
        be thrown a considerable distance.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.piercing 1
        ]
    , mods =
        [] : List Mods.Mod
    , name = "Javelin"
    , qualities =
        [ DEQ.Qualities.suppressed
        , DEQ.Qualities.thrown "M"
        ]
    , range = None Natural
    , rarity = 1
    , rate = None Natural
    , weight = 4
    , wtype = "Throwing Weapon"
    }

let throwing-knives : Weapon =
    { ammunition = None Text
    , cost = 10
    , damage = 3
    , description =
        ''
        A small, sharp knife, specially balanced for throwing. Normally made out of a
        lightweight metal, with the grip hollowed out to reduce weight. They do little
        damage by themselves, but are light, quiet, and quick to handle.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.piercing 1
        ]
    , mods =
        [] : List Mods.Mod
    , name = "Throwing Knives"
    , qualities =
        [ DEQ.Qualities.concealed
        , DEQ.Qualities.suppressed
        , DEQ.Qualities.thrown "C"
        ]
    , range = None Natural
    , rarity = 1
    , rate = None Natural
    , weight = 0
    , wtype = "Throwing Weapon"
    }

let tomahawk : Weapon =
    { ammunition = None Text
    , cost = 15
    , damage = 4
    , description =
        ''
        A small, short-handled axe, akin to a small hatchet. Less subtle than a
        throwing knife, a hurled tomahawk can certainly make an impression in an enemy.
        ''
    , dtype = DEQ.DamageTypes.physical
    , effects =
        [ DEQ.Effects.piercing 1
        ]
    , mods =
        [] : List Mods.Mod
    , name = "Tomahawk"
    , qualities =
        [ DEQ.Qualities.suppressed
        , DEQ.Qualities.thrown "C"
        ]
    , range = None Natural
    , rarity = 2
    , rate = None Natural
    , weight = 0
    , wtype = "Throwing Weapon"
    }

-- Output --

in  { Weapon
    , ProcessedWeapon
    , WeaponFactory
    , Weapons =
        {
        -- Big Guns
        , fat-man
        , flamer
        , gatling-laser
        , heavy-incinerator
        , junk-jet
        , minigun
        , missile-launcher
        -- Energy Weapons
        , gamma-gun
        , institute-laser
        , laser-gun
        , laser-musket
        , plasma-gun
        -- Explosives
        , baseball-grenade
        , bottlecap-mine
        , frag-grenade
        , frag-mine
        , molotov-cocktail
        , nuka-grenade
        , nuke-mine
        , plasma-grenade
        , plasma-mine
        , pulse-grenade
        , pulse-mine
        -- Melee Weapons
        , baseball-bat
        , baseball-bat-aluminum
        , baton
        , board
        , combat-knife
        , lead-pipe
        , machete
        , pipe-wrench
        , pool-cue
        , ripper
        , rolling-pin
        , shishkebab
        , sledgehammer
        , super-sledge
        , switchblade
        , sword
        , tire-iron
        , walking-cane
        , boxing-glove
        , deathclaw-gauntlet
        , knuckles
        , power-fist
        , gun-bash-1h
        , gun-bash-2h
        , rock
        , unarmed
        -- Small Guns
        , pipe-bolt-action
        , pipe-gun
        , pipe-revolver
        , pistol-44cal
        , pistol-10mm
        , pistol-flare
        , rifle-assault
        , rifle-combat
        , rifle-gauss
        , rifle-hunting
        , rifle-railway
        , shotgun-combat
        , shotgun-double-barrel
        , submachine-gun
        , syringer
        -- Throwing Weapons
        , javelin
        , throwing-knives
        , tomahawk
        }
    }
