-- src/crafting/mods/index.dhall

let data = ./src/mods.dhall
let utils = ./src/utils.dhall
let prelude = ./src/prelude.dhall

let mappedModRecord : Type =
    { mapKey : Text
    , mapValue : data.Mod
    }

let processMod = \(input : mappedModRecord) ->
    let out1 = input.mapValue
    let output = (data.ModFactory out1).entryGeneric
    in output

let processModsList = \(input : List mappedModRecord) ->
    let out1 = utils.Map mappedModRecord Text processMod input
    let output = prelude.Text.concatSep "\n" out1
    in output

let Document =
''
| Name | Type | Prefix | Effect | Complexity | Requirements | Weight | Cost |
|------|------|--------|--------|------------|--------------|--------|------|
${processModsList (toMap data.Mods)}
''

in Document