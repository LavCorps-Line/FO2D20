-- src/crafting/index.dhall

let common =
    ./src/package.dhall

let Document =
''
# Crafting

* [Crafting](#crafting)
  * [Materials](#materials)
    * [Material Items](#material-items)
  * [Repair Costs](#repair-costs)
  * [Crafting Costs (Given Complexity)](#crafting-costs-given-complexity)

## Materials

Materials are consumed to craft and repair items. This chapter contains
information relevant to Materials.

### Material Items

| Name               | Cost | Weight |
|--------------------|------|--------|
| Common Materials   | `1`  | `1`    |
| Uncommon Materials | `3`  | `1`    |
| Rare Materials     | `5`  | `1`    |

## Repair Costs

| Item Rarity | Materials Required to Repair                                     |
|-------------|------------------------------------------------------------------|
| `0`         | `1 Common Materials`                                             |
| `1`         | `2 Common Materials`                                             |
| `2`         | `2 Common Materials`, `1 Uncommon Materials`                     |
| `3`         | `2 Common Materials`, `2 Uncommon Materials`                     |
| `4`         | `2 Common Materials`, `2 Uncommon Materials`, `1 Rare Materials` |
| `5+`        | `3 Common Materials`, `3 Uncommon Materials`, `1 Rare Materials` |

## Crafting Costs (Given Complexity)

Items may also have other material costs, be sure to check!

| Complexity | Materials Required to Repair                                     |
|------------|------------------------------------------------------------------|
| `1`        | `2 Common Materials`                                             |
| `2`        | `3 Common Materials`                                             |
| `3`        | `4 Common Materials`, `2 Uncommon Materials`                     |
| `4`        | `5 Common Materials`, `3 Uncommon Materials`,                    |
| `5`        | `6 Common Materials`, `4 Uncommon Materials`, `2 Rare Materials` |
| `6`        | `7 Common Materials`, `5 Uncommon Materials`, `3 Rare Materials` |
| `7+`       | `8 Common Materials`, `6 Uncommon Materials`, `4 Rare Materials` |
''

in Document