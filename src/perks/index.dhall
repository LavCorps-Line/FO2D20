-- src/perks/index.dhall

let data = ./src/perks.dhall
let utils = ./src/utils.dhall
let prelude = ./src/prelude.dhall

let mappedPerkRecord : Type =
    { mapKey : Text
    , mapValue : Natural -> data.Perk
    }

let processPerkList = \(input : List mappedPerkRecord) ->
    let out1 = utils.Map mappedPerkRecord Text (\(input : mappedPerkRecord) ->(data.PerkFactory (input.mapValue 0)).entryLinked) input
    let output = prelude.Text.concatSep "\n" out1
    in output

let listPerkList = \(input : List mappedPerkRecord) ->
    let out1 = utils.Map mappedPerkRecord Text (\(input : mappedPerkRecord) -> (data.PerkFactory (input.mapValue 0)).description) input
    let output = prelude.Text.concatSep "\n---\n" out1
    in output

let Document =
''
| Name | Max. Ranks | Requirements |
|------|------------|--------------|
${processPerkList (toMap data.Perks)}

${listPerkList (toMap data.Perks)}
''

in Document
