# Summary

[Index](README.md)

---

# Character Creation

* [Equipment Kits](kits/index.md)
* [Origins](origins/index.md)
* [Perks](perks/index.md)

---

# Content

* [Armor](armor/index.md)
    * [Armor Pieces](armor/armor/index.md)
    * [Clothing](armor/clothing/index.md)
    * [Headgear](armor/headgear/index.md)
    * [Outfits](armor/outfit/index.md)
    * [Power Armor](armor/powered/index.md)
    * [Robot Armor](armor/robot/index.md)
* [Bestiary](bestiary/index.md)
* [Items](items/index.md)
    * [Beverages](items/beverages/index.md)
    * [Chems](items/chems/index.md)
    * [Consumables](items/consumables/index.md)
    * [Food](items/food/index.md)
* [Weapons](weapons/index.md)
    * [Big Guns](weapons/big/index.md)
    * [Energy Weapons](weapons/energy/index.md)
    * [Explosives](weapons/explosives/index.md)
    * [Melee Weapons](weapons/melee/index.md)
    * [Small Guns](weapons/small/index.md)
    * [Throwing Weapons](weapons/throwable/index.md)

---

# Crafting

* [Crafting Rules](crafting/index.md)
* [Armor](crafting/armor/index.md)
* [Items](crafting/items/index.md)
    * [Chems](crafting/items/chems/index.md)
    * [Consumables](crafting/items/consumables/index.md)
* [Mods](crafting/mods/index.md)
    * [Big Guns](crafting/mods/big/index.md)
    * [Energy Weapons](crafting/mods/energy/index.md)
    * [Melee Weapons](crafting/mods/melee/index.md)
    * [Small Guns](crafting/mods/small/index.md)
